#!/bin/sh

if [ -d .git ]; then

    if [ -f .git/HEAD ]; then
        IDFILE=".git/"`cat .git/HEAD | cut -d\: -f2 | tr -d ' '`
        HASH=`cat $IDFILE | cut -c1-8`
        echo "#define HOTZENBOT_GIT_VERSION_HASH ${HASH}-dev"
        exit 0
    else
        echo "ERROR : Cannot find HEAD file in .git directory" 1>&2
        exit 1
    fi

else
    echo "NOTE : Cannot find git version" 1>&2
    if [ -f VERSION ]; then
        echo "#define HOTZENBOT_GIT_VERSION_HASH `cat VERSION`"
    else
        echo "#define HOTZENBOT_GIT_VERSION_HASH unknown-dev"
    fi
fi
