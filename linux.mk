#
# NOTE: -D_XOPEN_SOURCE=700 Will NOT work on Solaris 10. So keep that
# in mind. However, EXTENSIONS are enabled, so some things will work,
# despite it being defined to 600. This is just to keep glibc happy.
#
linux_CFLAGS		=	-std=iso9899:2011 -g -O0 -pedantic -I/usr/local/include -I${PWD}/include \
				-fPIC -Wall -Wextra -Werror -Wno-pedantic
linux_CPPFLAGS		=	-D_XOPEN_SOURCE=700
linux_LDFLAGS		=	-rdynamic -lpthread -ldl -lssl -lcrypto
