/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/modules.h>
#include <hotzenbot/db.h>
#include <hotzenbot/channel_state.h>

#include <assert.h>
#include <string.h>

int
trusted_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sv sender = {0};
	if (eval_ctx_lookup_variable(ctx, SV("sender"), &sender))
		return eval_fail_with("Cannot trust without having a nick");

	sv account = {0};

	switch (irc_user_get_account(ctx->channel.network, sender, &account)) {
	case LOOKUP_OK: break;
	case LOOKUP_NOT_REGISTERED:
		return eval_fail_with("Cannot establish trust into an unregistered user. Please register your nick and rejoin the channel.");
	case LOOKUP_FAILED:
		return eval_fail_with("No account info available. Please try again later.");
	}

	if (!db_is_user_trusted(ctx->channel.network, account))
		return eval_fail_with("You have to be trusted in order to invoke this command.");

	return eval_expr_list(list, ctx, arena, out);
}

int
maintainer_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sv sender = {0};
	if (eval_ctx_lookup_variable(ctx, SV("sender"), &sender))
		return eval_fail_with("Cannot check for maintainer status without having a nick");

	sv account = {0};

	switch (irc_user_get_account(ctx->channel.network, sender, &account)) {
	case LOOKUP_OK: break;
	case LOOKUP_NOT_REGISTERED:
		return eval_fail_with("Cannot establish trust into an unregistered user. Please register your nick and rejoin the channel.");
	case LOOKUP_FAILED:
		return eval_fail_with("No account info available. Please try again later.");
	}

	if (!db_is_user_maintainer(ctx->channel.network, account))
		return eval_fail_with("You have to be a maintainer in order to invoke this command.");

	return eval_expr_list(list, ctx, arena, out);
}

int
authority_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sv sender = {0};
	if (eval_ctx_lookup_variable(ctx, SV("sender"), &sender))
		return eval_fail_with("Cannot check for authority status without having a nick");

	sv account = {0};

	switch (irc_user_get_account(ctx->channel.network, sender, &account)) {
	case LOOKUP_OK: break;
	case LOOKUP_NOT_REGISTERED:
		return eval_fail_with("Cannot establish trust into an unregistered user. Please register your nick and rejoin the channel.");
	case LOOKUP_FAILED:
		return eval_fail_with("No account info available. Please try again later.");
	}

	if (!db_is_user_authority(ctx->channel.network, account))
		return eval_fail_with("You have to be a member of the authority-group in order to invoke this command.");

	return eval_expr_list(list, ctx, arena, out);
}

int
assign_group_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	if (list->exprs_size != 2)
		return eval_fail_with("usage: %%assign_group(group, user...)");

	sb group = {0};
	sb user  = {0};

	if (eval_expr(&list->exprs[0], ctx, arena, &group) < 0)
		return -1;

	for (size_t i = 1; i < list->exprs_size; ++i)
		if (eval_expr(&list->exprs[i], ctx, arena, &user) < 0)
			return -1;

	char *nick_str  = sb_to_string(&user, arena);
	char *group_str = sb_to_string(&group, arena);

	if (!nick_str)
		return eval_fail_with("empty nick");

	if (!group_str)
		return eval_fail_with("empty group");

	sv nick     = SV(nick_str);
	sv account  = {0};
	sv group_sv = SV(group_str);

	if (irc_user_get_account(ctx->channel.network, nick, &account) != LOOKUP_OK)
		return eval_fail_with(
			"User with nick '"SV_FMT"' is either not registered or not joined "
			"to any of the channels.",
			SV_ARGS(nick));

	switch (db_assign_group(group_sv, ctx->channel.network, account)) {
	case DB_OK:
		sb_append_sv(out, arena, SV("User has been added to the group."));
		return 0;
	case DB_FAILURE:
		return eval_fail_with("Database failure");
	case DB_NOSUCHGROUP:
		return eval_fail_with(SV_FMT": No such group", SV_ARGS(group_sv));
	default:
		assert(0 && "Unreached");
	}

	return eval_fail_with("Internal bot error");
}

int
MODULE_LOAD_HOOK()
{
	modules_register_function("trusted", trusted_function_impl);
	modules_register_function("authority", authority_function_impl);
	modules_register_function("maintainer", maintainer_function_impl);
	modules_register_function("assign_group", assign_group_function_impl);

	return 0;
}

int
MODULE_UNLOAD_HOOK()
{
	modules_unregister_function("trusted");
	modules_unregister_function("authority");
	modules_unregister_function("maintainer");
	modules_unregister_function("assign_group");

	return 0;
}
