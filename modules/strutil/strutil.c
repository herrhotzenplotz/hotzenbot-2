/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/modules.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>

#include <wchar.h>
#include <ctype.h>

static int
reverse_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb       _text      = {0};
	sv       result     = {0}, text = {0};
	char    *_text_str;
	wchar_t *buffer     = NULL;
	size_t   characters = 0;

	if (eval_expr_list(list, ctx, arena, &_text) < 0)
		return -1;

	_text_str = sb_to_string(&_text, arena);

	if (!_text_str)
		return 0;

	text       = SV(_text_str);
	buffer     = arena_alloc(arena, (text.length + 1) * sizeof(wchar_t));
	characters = mbsrtowcs(buffer, (const char **)&text.data, text.length, NULL);

	for (size_t i = 0; i < characters / 2; ++i) {
		wchar_t tmp                = buffer[i];
		buffer[i]                  = buffer[characters - i - 1];
		buffer[characters - i - 1] = tmp;
	}

	buffer[characters] = L'\0';

	result.data   = arena_alloc(arena, text.length);
	result.length = text.length;

	if (wcsrtombs(result.data, (const wchar_t **)&buffer, result.length, NULL) == (size_t)(-1))
		return eval_fail_with("Wide character encoding failure");

	sb_append_sv(out, arena, result);
	return 0;
}

static int
cycle_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb    _text  = {0};
	sv    result = {0}, text = {0};
	char *_text_str;

	if (eval_expr_list(list, ctx, arena, &_text) < 0)
		return -1;

	_text_str = sb_to_string(&_text, arena);

	if (!_text_str)
		return 0;

	text          = SV(_text_str);
	result.data   = arena_alloc(arena, text.length);
	result.length = text.length;

	for (size_t i = 0; i < text.length; ++i)
		if (i % 2)
			result.data[i] = toupper(text.data[i]);
		else
			result.data[i] = tolower(text.data[i]);

	sb_append_sv(out, arena, result);
	return 0;
}

static const struct { wchar_t a; wchar_t b; } flip_table[] =
{
	{ .a = L'!', .b = L'¡' },
	{ .a = L'"', .b = L'„' },
	{ .a = L'&', .b = L'⅋' },
	{ .a = L'\'', .b = L',' },
	{ .a = L'(', .b = L')' },
	{ .a = L'.', .b = L'˙' },
	{ .a = L'3', .b = L'Ɛ' },
	{ .a = L'4', .b = L'ᔭ' },
	{ .a = L'6', .b = L'9' },
	{ .a = L'7', .b = L'Ɫ' },
	{ .a = L';', .b = L'؛' },
	{ .a = L'<', .b = L'>' },
	{ .a = L'?', .b = L'¿' },
	{ .a = L'A', .b = L'∀' },
	{ .a = L'B', .b = L'𐐒' },
	{ .a = L'C', .b = L'Ↄ' },
	{ .a = L'D', .b = L'◖' },
	{ .a = L'E', .b = L'Ǝ' },
	{ .a = L'F', .b = L'Ⅎ' },
	{ .a = L'G', .b = L'⅁' },
	{ .a = L'J', .b = L'ſ' },
	{ .a = L'K', .b = L'⋊' },
	{ .a = L'L', .b = L'⅂' },
	{ .a = L'M', .b = L'W' },
	{ .a = L'N', .b = L'ᴎ' },
	{ .a = L'P', .b = L'Ԁ' },
	{ .a = L'Q', .b = L'Ό' },
	{ .a = L'R', .b = L'ᴚ' },
	{ .a = L'T', .b = L'⊥' },
	{ .a = L'U', .b = L'∩' },
	{ .a = L'V', .b = L'ᴧ' },
	{ .a = L'Y', .b = L'⅄' },
	{ .a = L'[', .b = L']' },
	{ .a = L'_', .b = L'‾' },
	{ .a = L'a', .b = L'ɐ' },
	{ .a = L'b', .b = L'q' },
	{ .a = L'c', .b = L'ɔ' },
	{ .a = L'd', .b = L'p' },
	{ .a = L'e', .b = L'ǝ' },
	{ .a = L'f', .b = L'ɟ' },
	{ .a = L'g', .b = L'ƃ' },
	{ .a = L'h', .b = L'ɥ' },
	{ .a = L'i', .b = L'ı' },
	{ .a = L'j', .b = L'ɾ' },
	{ .a = L'k', .b = L'ʞ' },
	{ .a = L'l', .b = L'ʃ' },
	{ .a = L'm', .b = L'ɯ' },
	{ .a = L'n', .b = L'u' },
	{ .a = L'r', .b = L'ɹ' },
	{ .a = L't', .b = L'ʇ' },
	{ .a = L'v', .b = L'ʌ' },
	{ .a = L'w', .b = L'ʍ' },
	{ .a = L'y', .b = L'ʎ' },
	{ .a = L'{', .b = L'}' },
	{ .a = L'‿', .b = L'⁀' },
	{ .a = L'⁅', .b = L'⁆' },
	{ .a = L'∴', .b = L'∵' },
};

static wchar_t
flip_wchar(wchar_t x)
{
	for (size_t i = 0; i < ARRAY_SIZE(flip_table); ++i)
		if (flip_table[i].a == x)
			return flip_table[i].b;
		else if (flip_table[i].b == x)
			return flip_table[i].a;

	return x;
}

static int
flip_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb      _text    = {0};
	char   *text;
	size_t  text_len = 0;

	if (eval_expr_list(list, ctx, arena, &_text) < 0)
		return -1;

	text = sb_to_string(&_text, arena);

	/* Do not pass a NULL pointer which might be returned to strlen */
	if (!text)
		return 0;

	text_len = strlen(text);

	wchar_t       *wcs               = arena_alloc(arena, (text_len + 1) * sizeof(wchar_t));
	size_t         wcs_length        = mbstowcs(wcs, text, text_len);
	wchar_t       *target_buffer     = arena_alloc(arena, (wcs_length + 1) * sizeof(wchar_t));

	for (size_t i = 0; i < wcs_length; ++i)
		target_buffer[wcs_length - i - 1] = flip_wchar(wcs[i]);

	target_buffer[wcs_length] = L'\0';

	while (target_buffer) {
		size_t n = 4 * 32;
		char  *tmp = arena_alloc(arena, n * sizeof(char));

		size_t tmp_len = wcsrtombs(tmp, (const wchar_t ** restrict)&target_buffer, n * sizeof(char), NULL);

		if (tmp_len == (size_t)-1)
			return eval_fail_with("Wide character string conversion failure");

		sb_append_sv(out, arena, sv_from_parts(tmp, tmp_len));
	}

	return 0;
}

static int
head_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb      _text    = {0};
	char   *text;
	size_t  text_len = 0;

	if (eval_expr_list(list, ctx, arena, &_text) < 0)
		return -1;

	text = sb_to_string(&_text, arena);
	if (!text)
		return 0;

	text_len = strlen(text);

	size_t i;
	for (i = 0; i < text_len; ++i)
		if (text[i] == ' ')
			break;

	sv tmp = { .data = text, .length = i };
	sb_append_sv(out, arena, tmp);
	return 0;
}

static int
tail_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb      _text    = {0};
	char   *text;
	size_t  text_len = 0;

	if (eval_expr_list(list, ctx, arena, &_text) < 0)
		return -1;

	text = sb_to_string(&_text, arena);
	if (!text)
		return 0;

	text_len = strlen(text);

	size_t i;
	for (i = 0; i < text_len; ++i)
		if (text[i] == ' ')
			break;

	/* Prevent unsigned integer overflow */
	if (i == text_len)
		return 0;

	sv tmp = { .data = text + i + 1, .length = text_len - i - 1 };
	sb_append_sv(out, arena, tmp);
	return 0;
}

static int
cat_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb    _text = {0};
	char *text;

	if (eval_expr_list(list, ctx, arena, &_text) < 0)
		return -1;

	text = sb_to_string(&_text, arena);
	if (!text)
		return 0;

	sb_append_sv(out, arena, SV(text));

	return 0;
}

static int
drop_first_char_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb    _text = {0};
	char *text;

	if (eval_expr_list(list, ctx, arena, &_text) < 0)
		return -1;

	text = sb_to_string(&_text, arena);
	if (!text || strlen(text) == 0)
		return 0;

	sb_append_sv(out, arena, SV(text + 1));

	return 0;
}

int
MODULE_LOAD_HOOK(void)
{
	modules_register_function("reverse"         ,         reverse_function_impl);
	modules_register_function("cycle"           ,           cycle_function_impl);
	modules_register_function("flip"            ,            flip_function_impl);
	modules_register_function("head"            ,            head_function_impl);
	modules_register_function("tail"            ,            tail_function_impl);
	modules_register_function("cat"             ,             cat_function_impl);
	modules_register_function("drop_first_char" , drop_first_char_function_impl);

	return 0;
}

int
MODULE_UNLOAD_HOOK(void)
{
	modules_unregister_function("reverse");
	modules_unregister_function("cycle");
	modules_unregister_function("flip");
	modules_unregister_function("head");
	modules_unregister_function("tail");
	modules_unregister_function("cat");
	modules_unregister_function("drop_first_char");

	return 0;
}
