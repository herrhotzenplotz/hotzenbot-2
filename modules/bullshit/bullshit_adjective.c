static char *bullshit_adjective_cases[] = {
"24/365",
"24/7",
"B2B",
"B2C",
"back-end",
"best-of-breed",
"bleeding-edge",
"bricks-and-clicks",
"clicks-and-mortar",
"collaborative",
"compelling",
"cross-platform",
"cross-media",
"customized",
"cutting-edge",
"distributed",
"dot-com",
"dynamic",
"e-business",
"efficient",
"end-to-end",
"enterprise",
"extensible",
"frictionless",
"front-end",
"global",
"granular",
"holistic",
"impactful",
"innovative",
"integrated",
"interactive",
"intuitive",
"killer",
"leading-edge",
"magnetic",
"mission-critical",
"next-generation",
"one-to-one",
"open-source",
"out-of-the-box",
"plug-and-play",
"proactive",
"real-time",
"revolutionary",
"rich",
"robust",
"scalable",
"seamless",
"sexy",
"sticky",
"strategic",
"synergistic",
"transparent",
"turn-key",
"ubiquitous",
"user-centric",
"value-added",
"vertical",
"viral",
"virtual",
"visionary",
"web-enabled",
"wireless",
"world-class",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
bullshit_adjective_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(bullshit_adjective_cases);
    sb_append_sv(out, arena, SV(bullshit_adjective_cases[idx]));
    return 0;
}
