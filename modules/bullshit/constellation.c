static char *constellation_cases[] = {
"Big Dipper",
"Litte Dipper",
"Orion",
"Leo",
"Gemini",
"Cancer",
"Canis Minor",
"Canis Major",
"Ursa Major",
"Ursa Minor",
"Virgo",
"Libra",
"Scorpius",
"Sagittarius",
"Lyra",
"Capricornus",
"Aquarius",
"Pisces",
"Aries",
"Leo Minor",
"Auriga",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
constellation_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(constellation_cases);
    sb_append_sv(out, arena, SV(constellation_cases[idx]));
    return 0;
}
