static char *company_cases[] = {
"Rocket Lab",
"SpaceX'",
"Blue Origin",
"Virgin Galactic",
"SpaceDev",
"Bigelow Aerospace",
"Orbital Sciences",
"JPL",
"NASA Jet Propulsion Laboratory",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
company_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(company_cases);
    sb_append_sv(out, arena, SV(company_cases[idx]));
    return 0;
}
