static char *galaxy_cases[] = {
"Milky Way",
"Andromeda",
"Triangulum",
"Whirlpool",
"Blackeye",
"Sunflower",
"Pinwheel",
"Hoags Object",
"Centaurus A",
"Messier 83",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
galaxy_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(galaxy_cases);
    sb_append_sv(out, arena, SV(galaxy_cases[idx]));
    return 0;
}
