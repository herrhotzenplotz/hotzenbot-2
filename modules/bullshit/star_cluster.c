static char *star_cluster_cases[] = {
"Wild Duck",
"Hyades",
"Coma",
"Butterfly",
"Messier 7",
"Pleiades",
"Beehive Cluster",
"Pearl Cluster",
"Hodge 301",
"Jewel Box Cluster",
"Wishing Well Cluster",
"Diamond Cluster",
"Trumpler 10",
"Collinder 140",
"Liller 1",
"Koposov II",
"Koposov I",
"Djorgovski 1",
"Arp-Madore 1",
"NGC 6144",
"NGC 2808",
"NGC 1783",
"Messier 107",
"Messier 70",
"Omega Centauri",
"Palomar 12",
"Palomar 4",
"Palomar 6",
"Pyxis Cluster",
"Segue 3",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
star_cluster_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(star_cluster_cases);
    sb_append_sv(out, arena, SV(star_cluster_cases[idx]));
    return 0;
}
