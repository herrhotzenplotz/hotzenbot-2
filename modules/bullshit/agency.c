static char *agency_cases[] = {
"National Aeronautics and Space Administration",
"European Space Agency",
"German Aerospace Center",
"Indian Space Research Organization",
"China National Space Administration",
"UK Space Agency",
"Brazilian Space Agency",
"Mexican Space Agency",
"Israeli Space Agency",
"Italian Space Agency",
"Japan Aerospace Exploration Agency",
"National Space Agency of Ukraine",
"Russian Federal Space Agency",
"Swedish National Space Board",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
agency_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(agency_cases);
    sb_append_sv(out, arena, SV(agency_cases[idx]));
    return 0;
}
