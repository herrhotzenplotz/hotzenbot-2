static char *moon_cases[] = {
"Moon",
"Luna",
"Deimos",
"Phobos",
"Ganymede",
"Callisto",
"Io",
"Europa",
"Titan",
"Rhea",
"Iapetus",
"Dione",
"Tethys",
"Hyperion",
"Ariel",
"Puck",
"Oberon",
"Umbriel",
"Triton",
"Proteus",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
moon_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(moon_cases);
    sb_append_sv(out, arena, SV(moon_cases[idx]));
    return 0;
}
