static char *nebula_cases[] = {
"Lagoon Nebula",
"Eagle Nebula",
"Triffid Nebula",
"Dumbell Nebula",
"Orion Nebula",
"Ring Nebula",
"Bodes Nebula",
"Owl Nebula",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
nebula_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(nebula_cases);
    sb_append_sv(out, arena, SV(nebula_cases[idx]));
    return 0;
}
