static char *bullshit_verb_cases[] = {
"aggregate",
"architect",
"benchmark",
"brand",
"cultivate",
"deliver",
"deploy",
"disintermediate",
"drive",
"e-enable",
"embrace",
"empower",
"enable",
"engage",
"engineer",
"enhance",
"envisioneer",
"evolve",
"expedite",
"exploit",
"extend",
"facilitate",
"generate",
"grow",
"harness",
"implement",
"incentivize",
"incubate",
"innovate",
"integrate",
"iterate",
"leverage",
"matrix",
"maximize",
"mesh",
"monetize",
"morph",
"optimize",
"orchestrate",
"productize",
"recontextualize",
"redefine",
"reintermediate",
"reinvent",
"repurpose",
"revolutionize",
"scale",
"seize",
"strategize",
"streamline",
"syndicate",
"synergize",
"synthesize",
"target",
"transform",
"transition",
"unleash",
"utilize",
"visualize",
"whiteboard",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
bullshit_verb_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(bullshit_verb_cases);
    sb_append_sv(out, arena, SV(bullshit_verb_cases[idx]));
    return 0;
}
