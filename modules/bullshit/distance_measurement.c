static char *distance_measurement_cases[] = {
"light years",
"AU",
"parsecs",
"kiloparsecs",
"megaparsecs",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
distance_measurement_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(distance_measurement_cases);
    sb_append_sv(out, arena, SV(distance_measurement_cases[idx]));
    return 0;
}
