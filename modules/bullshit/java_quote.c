static char *java_quote_cases[] = {
"Using Java for serious jobs is like trying to take the skin off a rice pudding wearing boxing gloves. – Tel Hudson",
"Of all the great programmers I can think of, I know of only one who would voluntarily program in Java. And of all the great programmers I can think of who don’t work for Sun, on Java, I know of zero. – Paul Graham",
"Java is the most distressing thing to happen to computing since MS-DOS. – Alan Kay",
"Java is, in many ways, C++–. – Michael Feldman",
"C++ is history repeated as tragedy. Java is history repeated as farce. – Scott McKay",
"Java, the best argument for Smalltalk since C++. – Frank Winkler",
"Arguing that Java is better than C++ is like arguing that grasshoppers taste better than tree bark. – Thant Tessman",
"Java: the elegant simplicity of C++ and the blazing speed of Smalltalk. – Jan Steinman",
"Like the creators of sitcoms or junk food or package tours, Java’s designers were consciously designing a product for people not as smart as them. – Paul Graham",
"There are undoubtedly a lot of very intelligent people writing Java, better programmers than I will ever be. I just wish I knew why. – Steve Holden",
"The more of an IT flavor the job descriptions had, the less dangerous was the company. The safest kind were the ones that wanted Oracle experience. You never had to worry about those. You were also safe if they said they wanted C++ or Java developers. If they wanted Perl or Python programmers, that would be a bit frightening. If I had ever seen a job posting looking for Lisp hackers, I would have been really worried. – Paul Graham",
"If you learn to program in Java, you’ll never be without a job! – Patricia Seybold in 1998",
"Knowing the syntax of Java does not make someone a software engineer. – John Knight",
"In the best possible scenario Java will end up mostly like Eiffel but with extra warts because of insufficiently thoughtful early design. – Matthew B Kennel",
"Java has been a boon to the publishing industry. – Rob Pike",
"The only thing going for java is that it’s consuming trademark namespace. – Boyd Roberts",
"Java is the SUV of programming tools. A project done in Java will cost 5 times as much, take twice as long, and be harder to maintain than a project done in a scripting language such as PHP or Perl. … But the programmers and managers using Java will feel good about themselves because they are using a tool that, in theory, has a lot of power for handling problems of tremendous complexity. Just like the suburbanite who drives his SUV to the 7-11 on a paved road but feels good because in theory he could climb a 45-degree dirt slope. – Greenspun, Philip",
"JAVA truly is the great equalizing software. It has reduced all computers to mediocrity and buggyness. - NASA’s J-Track web site",
"C and Java are different in the non-excitability department, though. With C, you don’t get excited about it like you don’t get excited about a good vintage wine, but with Java, you don’t get excited about it like you don’t get excited about taking out the garbage. – Lamont Cranston (aka Jorden Mauro)",
"Saying that Java is good because it works on all platforms is like saying anal sex is good because it works on all genders. – Unknown",
"java is about as fun as an evening with 300 hornets in a 5m^2 room – andguent",
"If Java had true garbage collection, most programs would delete themselves upon execution. – Robert Sewell",
"Java: write once, run away! – Brucee",
"Java is a DSL to transform big XML documents into long exception stack traces. – Scott Bellware",
"The definition of Hell is working with dates in Java, JDBC, and Oracle. Every single one of them screw it up. – Dick Wall CommunityOne 2007: Lunch with the Java Posse",
"Java is like a variant of the game of Tetris in which none of the pieces can fill gaps created by the other pieces, so all you can do is pile them up endlessly. – Steve Yegge (2007, Codes Worst Enemy)",
"Every modern Java program I see embeds its logic in an exaggerated choose-your-own-adventure model where the decisions of where to jump next occur in one word increments. – deong",
"Sufficiently advanced Java is indistinguishable from satire. – @eeppa  (Same is true of sufficiently advanced C++)",
"Whenever I write code in Java I feel like I’m filling out endless forms in triplicate. – Joe Marshall (aka jrm)",
"In the JSR-296 “The intended audience for this snapshot is experienced Swing developers with a moderately high tolerance for pain.” Gil Hova Reply: “Wait. There are Swing developers with low tolerances for pain?”",
};
#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
java_quote_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(java_quote_cases);
    sb_append_sv(out, arena, SV(java_quote_cases[idx]));
    return 0;
}
