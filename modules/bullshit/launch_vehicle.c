static char *launch_vehicle_cases[] = {
"Antares",
"Ariane 5",
"Atlas",
"Diamant",
"Dnepr",
"Delta",
"Electron",
"Energia",
"Europa",
"Falcon 9",
"Falcon Heavy",
"GSLV",
"Juno",
"Long March",
"Mercury-Redstone",
"Minotaur",
"Pegasus",
"Proton",
"PSLV",
"Safir",
"Shavit",
"Saturn IV",
"Semiorka",
"Soyouz",
"Titan",
"Vega",
"Veronique",
"Zenit",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
launch_vehicle_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(launch_vehicle_cases);
    sb_append_sv(out, arena, SV(launch_vehicle_cases[idx]));
    return 0;
}
