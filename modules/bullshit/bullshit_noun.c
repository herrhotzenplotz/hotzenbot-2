static char *bullshit_noun_cases[] = {
"action-items",
"applications",
"architectures",
"bandwidth",
"channels",
"communities",
"content",
"convergence",
"deliverables",
"e-business",
"e-commerce",
"e-markets",
"e-services",
"e-tailers",
"experiences",
"eyeballs",
"functionalities",
"infomediaries",
"infrastructures",
"initiatives",
"interfaces",
"markets",
"methodologies",
"metrics",
"mindshare",
"models",
"networks",
"niches",
"paradigms",
"partnerships",
"platforms",
"portals",
"relationships",
"ROI",
"synergies",
"web-readiness",
"schemas",
"solutions",
"supply-chains",
"systems",
"technologies",
"users",
"vortals",
"web services",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
bullshit_noun_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(bullshit_noun_cases);
    sb_append_sv(out, arena, SV(bullshit_noun_cases[idx]));
    return 0;
}
