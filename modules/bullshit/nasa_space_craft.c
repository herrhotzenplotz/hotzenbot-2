static char *nasa_space_craft_cases[] = {
"Orion",
"Mercury",
"Gemini",
"Apollo",
"Enterprise",
"Columbia",
"Challenger",
"Discovery",
"Atlantis",
"Endeavour",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
nasa_space_craft_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(nasa_space_craft_cases);
    sb_append_sv(out, arena, SV(nasa_space_craft_cases[idx]));
    return 0;
}
