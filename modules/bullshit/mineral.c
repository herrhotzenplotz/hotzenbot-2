static char *mineral_cases[] = {
"Agate",
"Amber",
"Amethyst",
"Apatite",
"Aquamarine",
"Aragonite",
"Beryl",
"Cadmium",
"Cobalt",
"Copper",
"Coral",
"Corundum",
"Diamond",
"Emerald",
"Epidote",
"Euclase",
"Garnet",
"Hematite",
"Heliodor",
"Iridium",
"Iron",
"Jade",
"Lapis",
"Lead",
"Mercury",
"Mica",
"Nickel",
"Olivine",
"Onyx",
"Opal",
"Pearl",
"Perlite",
"Platinum",
"Pyrite",
"Quartz",
"Realgar",
"Ruby",
"Rutile",
"Sapphire",
"Silver",
"Sulfur",
"Titanium",
"Topaz",
"Tungsten",
"Turquoise",
"Ultramarine",
"Vulcanite",
"Zircon",
"Zinc",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
mineral_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(mineral_cases);
    sb_append_sv(out, arena, SV(mineral_cases[idx]));
    return 0;
}
