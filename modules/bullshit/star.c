static char *star_cases[] = {
"Sun",
"Proxima Centauri",
"Rigil Kentaurus",
"Barnards Star",
"Wolf 359",
"Luyten 726-8A",
"Luyten 726-8B",
"Sirius A",
"Sirius B",
"Ross 154",
"Ross 248",
"Procyon A",
"Procyon B",
"Vega",
"Rigel",
"Arcturus",
"Betelgeuse",
"Mahasim",
"Polaris",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
star_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(star_cases);
    sb_append_sv(out, arena, SV(star_cases[idx]));
    return 0;
}
