static char *agency_abv_cases[] = {
"NASA",
"AEM",
"AEB",
"UKSA",
"CSA",
"CNSA",
"ESA",
"DLR",
"ISRO",
"JAXA",
"ISA",
"CNES",
"NSAU",
"ROSCOSMOS",
"SNSB",
};

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
agency_abv_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(agency_abv_cases);
    sb_append_sv(out, arena, SV(agency_abv_cases[idx]));
    return 0;
}
