MODULE_CFLAGS	+=	-I${TOPDIR}/include
TARGET		=	lib${MODNAME}.so
SRCS		?=	${TOPDIR}/modules/${MODNAME}/${MODNAME}.c
OBJS		=	${SRCS:.c=.o}

DEPS		=	${SRCS:.c=.d}

.PHONY: all clean depend
all: ${TOPDIR}/modules/${MODNAME}/Makefile ${DEPS} depend

-include ${DEPS}

.SILENT:

${TARGET}: ${OBJS}
	@echo " ==> Linking ${TARGET}"
	${CC} -o ${TARGET} -shared ${OBJS} ${LINK_FLAGS} ${MODULE_CFLAGS} ${MODULE_LDFLAGS}

clean:
	@echo " ==> Cleaning ${TARGET}"
	rm -f ${TARGET} ${OBJS} ${DEPS}

depend: ${DEPS}
	@echo " ==> Starting build of ${TARGET}"
	${MAKE} CC="${CC}" TOPDIR="${PWD}" MODNAME="${MODNAME}" COMPILE_FLAGS="${COMPILE_FLAGS}" \
		LINK_FLAGS="${LINK_FLAGS}" MKDEPS_FLAGS="${MKDEPS_FLAGS}" \
		-s -f ${TOPDIR}/modules/${MODNAME}/Makefile ${TARGET}

.SUFFIXES: .c .o
.c.o:
# This is kinda slow but ... blame noomake.
	case ${CC} in *c99) printf -- '\n\n ERROR : You are probably using GNU make. Please set CC in your environment to something that is not c99 and see linux.mk for more information.\n\n'; exit 69;; esac
	@echo " ==> Compiling "`basename $@`
	${CC} -c -o $@ ${MODULE_CFLAGS} ${COMPILE_FLAGS} $<

.SUFFIXES: .c .d
.c.d:
	@echo " ==> Generating dependencies for "`basename $<`
	${CC} ${MKDEPS_FLAGS} ${MODULE_CFLAGS} $< > $@
