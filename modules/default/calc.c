/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 * Copyright 2022 Aritra Sarkar <aritra1911@yahoo.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ctype.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wctype.h>

#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>

static int
calc_next_token(sv *input, sv *out)
{
	size_t i = 0;

	*input = sv_trim_front(*input);
	if (input->length == 0)
		return -1;

	if (isdigit(input->data[0]))
		goto digit;

	if (isalpha(input->data[0])) {
		if (sv_has_prefix(*input, "random")) {
			i = strlen("random");
			goto end;
		}

		if (sv_has_prefix(*input, "pi")) {
			i = strlen("pi");
			goto end;
		}

		if (sv_has_prefix(*input, "e")) {
			i = strlen("e");
			goto end;
		}

		i = 1;
		goto end;
	}

	if (input->data[0] == '-' && input->length > 1 && isdigit(input->data[1]))
		goto digit;

	i = 1;
	goto end;

digit:
	for (i = 0; i < input->length; ++i) {
		char c = input->data[i];

		if (!(isdigit(c) || c == '.' || c == tolower('e') || c == '-'))
			break;
	}

end:
	*out           = *input;
	out->length    = i;
	input->data   += i;
	input->length -= i;
	return 0;
}

int
calc_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb _dc_expr = {0};

	if (eval_expr_list(list, ctx, arena, &_dc_expr) < 0)
		return -1;

	char *dc_expr_str = sb_to_string(&_dc_expr, arena);
	if (!dc_expr_str)
		return eval_fail_with("no input to dc");

	sv     dc_expr    = SV(dc_expr_str);
	double stack[64]  = { 0.0 };
	size_t stack_size = 0;

	while (dc_expr.length > 0) {
		sv word = {0};

		if (calc_next_token(&dc_expr, &word) < 0)
			return eval_fail_with("unexpected end of input");

		if (isdigit(word.data[0]) || (word.data[0] == '-' && word.length > 1)) {
			char *endptr = NULL;

			double maybe_number = strtod(word.data, &endptr);

			if (endptr != word.data + word.length)
				return eval_fail_with("calc: "SV_FMT" is not a number", SV_ARGS(word));

			if (stack_size == ARRAY_SIZE(stack))
				return eval_fail_with("calc: stack overflow");

			stack[stack_size++] = maybe_number;

		} else if (sv_eq_to_cstr(word, "pi")) {
			if (stack_size == ARRAY_SIZE(stack))
				return eval_fail_with("calc: stack overflow");

			time_t now = time(NULL);
			struct tm *now_tm = gmtime(&now);

			if (now_tm->tm_mon == 3 && now_tm->tm_mday == 1)
				stack[stack_size++] = 10;
			else
				stack[stack_size++] = M_PI;

		} else if (sv_eq_to_cstr(word, "random")) {
			if (stack_size == ARRAY_SIZE(stack))
				return eval_fail_with("calc: stack overflow");

			stack[stack_size++] = (double)(rand()) / (double)(RAND_MAX);
		} else if (sv_eq_to_cstr(word, "e")) {
			if (stack_size == ARRAY_SIZE(stack))
				return eval_fail_with("calc: stack overflow");

			stack[stack_size++] = exp(1.0);
		} else if (sv_eq_to_cstr(word, "v")) {
			if (stack_size < 1)
				return eval_fail_with("calc: stack underflow");

			double v = stack[--stack_size];
			stack[stack_size++] = sqrt(v);
		} else if (sv_eq_to_cstr(word, "b")) {
			if (stack_size < 1)
				return eval_fail_with("calc: stack underflow");

			double v = stack[--stack_size];
			stack[stack_size++] = fabs(v);
		} else if (word.length == 1) {
			if (stack_size < 2)
				return eval_fail_with("calc: stack underflow");

			double v1 = stack[--stack_size];
			double v2 = stack[--stack_size];
			switch (word.data[0]) {
			case '+':
				stack[stack_size++] = v1 + v2;
				break;
			case '-':
				stack[stack_size++] = v2 - v1;
				break;
			case '*':
				stack[stack_size++] = v2 * v1;
				break;
			case '^':
				stack[stack_size++] = pow(v2, v1);
				break;
			case '/': {
				if (fabs(v1) <= DBL_EPSILON)
					return eval_fail_with("calc: Division by zero");
				stack[stack_size++] = v2 / v1;
			} break;
			case '~': {
				if (fabs(v1) <= DBL_EPSILON)
					return eval_fail_with("calc: Division by zero");
				stack[stack_size++] = floor(v2 / v1);
				stack[stack_size++] = fmod(v2, v1);
			} break;
			case '%': {
				if (fabs(v1) <= DBL_EPSILON)
					return eval_fail_with("calc: Division by zero");
				stack[stack_size++] = fmod(v2, v1);
			} break;
			default:
				return eval_fail_with("calc: unrecognized operation "SV_FMT, SV_ARGS(word));
			}

		} else {
			return eval_fail_with("calc: Failed to parse "SV_FMT, SV_ARGS(word));
		}

		dc_expr = sv_trim_front(dc_expr);
		if (dc_expr.length == 0)
			/* Finished parsing the expr */
			break;
	}

	if (stack_size == 0)
		return eval_fail_with("calc: Stack underflow while printing");

	char buffer[128] = {0};
	size_t buffer_len = snprintf(buffer, sizeof(buffer),
								 "%lf", stack[--stack_size]);

	char *result_buffer = arena_alloc(arena, buffer_len);
	memcpy(result_buffer, buffer, buffer_len);

	sb_append_sv(out, arena, sv_from_parts(result_buffer, buffer_len));
	return 0;
}

static int
itobasestr(long input, int obase, char *buffer, size_t buffer_size)
{
	const char digits[] = "0123456789ABCDEF";
	size_t length = 0;

	memset(buffer, 0, buffer_size);

	if (obase == 0)
		return eval_fail_with("convertbase: output base cannot be zero");

	if (input == 0) {
		buffer[0] = '0';
		return 0;
	}

	while (input != 0) {
		if (length == buffer_size)
			return eval_fail_with("convertbase: conversion buffer too small");

		buffer[buffer_size - 1 - (length++)] = digits[input % obase];

		input = (input - (input % obase)) / obase;
	}

	memmove(buffer, buffer + (buffer_size - length), length);
	buffer[length] = '\0';

	return 0;
}

int
convertbase_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb ibase_sb = {0};
	sb obase_sb = {0};
	sb input_sb = {0};

	char *ibase_str = NULL;
	char *obase_str = NULL;
	char *input_str = NULL;
	char *endptr    = NULL;

	unsigned long ibase = 0;
	unsigned long obase = 0;
	long          input = 0;

	if (list->exprs_size != 3)
		return eval_fail_with(
			"convertbase: expected 3 arguments but got %zu instead",
			list->exprs_size);

	if (eval_expr(&list->exprs[0], ctx, arena, &ibase_sb) < 0)
		return -1;
	ibase_str = sb_to_string(&ibase_sb, arena);

	ibase = strtoul(ibase_str, &endptr, 10);
	if (endptr != ibase_str + strlen(ibase_str))
		return eval_fail_with("convertbase: cannot parse input base");

	if (eval_expr(&list->exprs[1], ctx, arena, &obase_sb) < 0)
		return -1;
	obase_str = sb_to_string(&obase_sb, arena);

	obase = strtoul(obase_str, &endptr, 10);
	if (endptr != obase_str + strlen(obase_str))
		return eval_fail_with("convertbase: cannot parse output base");

	if (eval_expr(&list->exprs[2], ctx, arena, &input_sb) < 0)
		return -1;
	input_str = sb_to_string(&input_sb, arena);

	input = strtol(input_str, &endptr, (int)(ibase));
	/* possible decimal point when we parse integers */
	if (endptr != input_str + strlen(input_str) && *endptr != '.')
		return eval_fail_with("convertbase: cannot parse input");

	if (input < 0)
		return eval_fail_with(
			"convertbase: input number must be greater or equal to zero");

	const size_t  buffer_size = 64;
	char         *buffer = arena_alloc(arena, buffer_size);

	if (itobasestr(input, obase, buffer, buffer_size) < 0)
		return -1;

	sb_append_sv(out, arena, sv_from_parts(buffer, buffer_size));

	return 0;
}

static int
base64_encode(char *input, char *buffer, size_t buffer_size)
{
	const char digits[] =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	unsigned char byte     = 0;
	unsigned long octets   = 0;
	size_t        bits_rem = 0;
	size_t        length   = 0;

	memset(buffer, 0, buffer_size);

	while ((byte = *(unsigned char *) input++)) {

		octets = (octets << 8) | byte;
		bits_rem += 8;

		/* 3 bytes (24 bits) of input yields 4 base64 digits */

		if (bits_rem == 24) {

			while (bits_rem > 0) {
				buffer[length + (bits_rem / 6) - 1] = digits[octets & 0x3f];
				octets >>= 6;
				bits_rem -= 6;
			}

			length += 4;
		}
	}

	if (bits_rem > 0) {

		size_t zero_bits = 6 - (bits_rem % 6);
		octets <<= zero_bits;
		bits_rem += zero_bits;

		size_t digit_count = 0;
		while (bits_rem > 0) {
			buffer[length + (bits_rem / 6) - 1] = digits[octets & 0x3f];
			octets >>= 6;
			bits_rem -= 6;
			digit_count++;
		}

		length += digit_count;

		/* Add required number of padding chars */
		while (zero_bits > 0) {
			buffer[length++] = '=';
			zero_bits -= 2;
		}
	}

	return 0;
}

static int
base64_decode(char *input, char *buffer, size_t buffer_size)
{
	const char digits[] =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	char          digit    = 0;
	unsigned long octets   = 0;
	size_t        bits_rem = 0;
	size_t        length   = 0;

	memset(buffer, 0, buffer_size);

	while ((digit = *input++)) {

		if (digit == '=') {
			if (bits_rem % 8 == 0)
				/* Unnecessary padding char */
				return eval_fail_with("base64: invalid base64 input");

			do {
				if (digit != '=')
					return eval_fail_with("base64: invalid base64 input");

				octets >>= 2;
				bits_rem -= 2;

				digit = *input++;

			} while (bits_rem % 8 != 0);

			if (digit)
				return eval_fail_with("base64: invalid base64 input");

			size_t byte_count = 0;
			while (bits_rem > 0) {
				unsigned char octet = octets & 0xff;

				if (octet == '\0')
					return eval_fail_with("base64: null-character encountered"
										  " during decode");

				buffer[length + (bits_rem / 8) - 1] = (char) octet;
				octets >>= 8;
				bits_rem -= 8;
				byte_count++;
			}

			length += byte_count;

			return 0;
		}

		size_t sextet = 0;

		/* Lookup index for a digit.  We shall perform a linear search
		 * for the index of the digit.  Since there are only 64 digits,
		 * this should be done in a jiffy. */
		for ( ; sextet < 64; sextet++)
			if (digits[sextet] == digit)
				break;

		if (sextet == 64)
			/* Oops!  We couldn't lookup the index of `digit` */
			return eval_fail_with("base64: invalid base64 input");

		octets = (octets << 6) | sextet;
		bits_rem += 6;

		/* 4 sextets (24 bits) of base64 input yields 3 bytes */

		if (bits_rem == 24) {
			while (bits_rem > 0) {
				unsigned char octet = octets & 0xff;

				if (octet == '\0')
					return eval_fail_with("base64: null-character encountered"
										  " during decode");

				buffer[length + (bits_rem / 8) - 1] = (char) octet;
				octets >>= 8;
				bits_rem -= 8;
			}

			length += 3;
		}
	}

	if (bits_rem > 0)
		return eval_fail_with("base64: invalid base64 input");

	return 0;
}

int
base64_function_impl(expr_list *list, arena *arenaptr, eval_ctx *ctx, sb *out)
{
	sb enc_sb   = {0};
	sb input_sb = {0};

	char *enc_str = NULL;
	char *input   = NULL;
	char *endptr  = NULL;

	unsigned long enc = 0;

	if (list->exprs_size != 2)
		return eval_fail_with(
			"base64: expected 2 arguments but got %zu instead",
			list->exprs_size);

	if (eval_expr(&list->exprs[0], ctx, arenaptr, &enc_sb))
		return -1;

	enc_str = sb_to_string(&enc_sb, arenaptr);
	enc = strtoul(enc_str, &endptr, 2);

	/* `enc` is supposed to be a boolean flag.
	 *
	 *     enc == 0 => decode
	 *     enc == 1 => encode
	 *
	 */

	if (endptr != enc_str + strlen(enc_str) || enc > 1)
		return eval_fail_with(
			"base64: cannot parse boolean flag `enc`");

	if (eval_expr(&list->exprs[1], ctx, arenaptr, &input_sb))
		return -1;

	input = sb_to_string(&input_sb, arenaptr);

	if (!input)
		return eval_fail_with("base64: no input given");

	const size_t buffer_size = 256;
	char        *buffer = arena_alloc(arenaptr, buffer_size);

	int ret = 0;

	if (enc)
		ret = base64_encode(input, buffer, buffer_size);
	else
		ret = base64_decode(input, buffer, buffer_size);

	if (ret < 0)
		return -1;

	/* Check if decoded base64 is safe to print */
	if (!enc) {
		wchar_t *wbuffer;
		size_t   wchars_converted;

		wbuffer = arena_calloc(arenaptr, buffer_size, sizeof(wchar_t));
		wchars_converted = mbstowcs(wbuffer, buffer, buffer_size);

		if (wchars_converted == (size_t) -1)
			return eval_fail_with("base64: invalid multibyte sequence"
								  " encountered during decode");

		for (size_t i = 0; i < wchars_converted; i++)
			if (iswcntrl(wbuffer[i]))
				return eval_fail_with("base64: non-printable character"
									  " encountered during decode");
	}

	sb_append_sv(out, arenaptr, sv_from_parts(buffer, buffer_size));

	return 0;
}
