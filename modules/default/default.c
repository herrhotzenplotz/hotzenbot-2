/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/channel_state.h>
#include <hotzenbot/db.h>
#include <hotzenbot/helper.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/modules.h>

#include <ctype.h>
#include <stdio.h>
#include <string.h>

extern int base64_function_impl(expr_list *, arena *, eval_ctx *, sb *);
extern int calc_function_impl(expr_list *, arena *, eval_ctx *, sb *);
extern int convertbase_function_impl(expr_list *, arena *, eval_ctx *, sb *);
extern int add_reminder_function_impl(expr_list *, arena *, eval_ctx *, sb *);
extern int markov_function_impl(expr_list *, arena *, eval_ctx *, sb *);
extern int examine_function_impl(expr_list *, arena *, eval_ctx *, sb *);
extern int stalk_function_impl(expr_list *, arena *, eval_ctx *, sb *);

int
addcmd_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	/*
	 * We wanna assume, that the first word in the list is the command
	 * name and the rest is the code. That way we can work around
	 * having to implement an awkward getopt parser. At least I hope
	 * so.
	 */

	sb args = {0};
	if (eval_expr_list(list, ctx, arena, &args) < 0)
		return -1;

	char *args_str = sb_to_string(&args, arena);
	if (!args_str)
		return eval_fail_with("no arguments given to addcmd");

	/* Find the first word */
	size_t i;
	size_t args_len = strlen(args_str);

	for (i = 0; i < args_len; ++i)
		if (isspace(args_str[i]))
			break;

	sv cmd_name = sv_from_parts(args_str, i);
	sv cmd_code = sv_from_parts(args_str + i + 1, args_len - 1 - i);

	if (db_add_command(cmd_name, cmd_code) < 0)
		return eval_fail_with("Unable to insert command definition into database");

	sb_append_sv(out, arena, SV("Command has been created"));

	return 0;
}

int
delcmd_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb _command_name = {0};

	if (eval_expr_list(list, ctx, arena, &_command_name) < 0)
		return -1;

	char *command_name_str = sb_to_string(&_command_name, arena);
	if (!command_name_str)
		return eval_fail_with("missing command name to delcmd");

	sv command_name = SV(command_name_str);

	if (db_del_commandname(command_name) < 0)
		return eval_fail_with("Unable to delete command "SV_FMT, SV_ARGS(command_name));

	sb_append_sv(out, arena, SV("Command has been deleted"));
	return 0;
}

int
getquote_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	(void) list;

	sv quote = {0};
	if (db_get_random_quote(ctx->channel, arena, &quote) < 0)
		return eval_fail_with("No quotes in database");

	sb_append_sv(out, arena, quote);

	return 0;
}

int
addquote_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb quote = {0};

	if (eval_expr_list(list, ctx, arena, &quote) < 0)
		return -1;

	char *quote_str = sb_to_string(&quote, arena);
	if (!quote_str)
		return eval_fail_with("no quote text given");

	if (db_addquote(ctx->channel, SV(quote_str)) < 0)
		return eval_fail_with("Unable to add quote to database.");

	sb_append_sv(out, arena, SV("Quote has been added to the database"));
	return 0;
}

int
or_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb tmp = {0};

	for (size_t i = 0; i < list->exprs_size; ++i) {
		if (eval_expr(&list->exprs[i], ctx, arena, &tmp) < 0)
			continue;

		if (sb_length(&tmp) == 0)
			continue;

		sb_append_sb(out, tmp);
		return 0;
	}

	// TODO: adapt eval_fail_with to produce a more helpful error message
	return eval_fail_with("%%or: no viable result");
}


/* %if(cond, action, else) */
int
if_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	if (list->exprs_size != 3)
		return eval_fail_with("%%if: expected 3 parameters");

	int if_condition = 0;

	/* Condition */
	{
		sb cond_sb = {0};
		if (eval_expr(&list->exprs[0], ctx, arena, &cond_sb) < 0)
			return -1;

		/* True conditions:
		 *  - YES yes true True TRUE ON on 1
		 *
		 * Everything else is treated as false */
		char *cond = sb_to_string(&cond_sb, arena);
		if (!cond) {
			if_condition = 0;
			goto found_condition;
		}

		sv cond_trimmed = sv_trim(SV(cond));
		if (!cond_trimmed.length)
			goto found_condition;

		/* We don't care about any weird character encodings here as
		 * our true cases are ASCII-only. */
		for (size_t i = 0; i < cond_trimmed.length; ++i)
			cond[i] = tolower(cond[i]);

		static const char *const true_cases[] = {"yes", "true", "on", "1"};

		for (size_t i = 0; i < ARRAY_SIZE(true_cases); ++i) {
			if (!strncmp(true_cases[i], cond_trimmed.data, cond_trimmed.length)) {
				if_condition = 1;
				goto found_condition;
			}
		}
	}

found_condition:

	/* in true case we choose the 2nd element (idx 1) otherwise the
	 * 3rd element at idx 2. */
	return eval_expr(&list->exprs[2 - if_condition], ctx, arena, out);
}

static void
reset_tz(char *old_tz)
{
	if (old_tz) {
		setenv("TZ", old_tz, 1);
		free(old_tz);
	} else {
		unsetenv("TZ");
	}

	tzset();
}

int
localtime_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb tz = {0};

	if (eval_expr_list(list, ctx, arena, &tz) < 0)
		return -1;

	char *new_tz = sb_to_string(&tz, arena);
	if (!new_tz)
		new_tz = "Pacific/Adamstown";

	/*
	 * Warning! Ugly code! UNIX Timezone handling is an awful
	 * mess. Don't blame it on me. Even esr recommends this:
	 *
	 *   - http://www.catb.org/esr/time-programming/
	 */
	char *old_tz = getenv("TZ");
	if (old_tz)
		old_tz = strdup(old_tz);

	if (setenv("TZ", new_tz, 1) < 0) {
		if (old_tz)
			free(old_tz);

		return eval_fail_with("Unable to set timezone for %s", new_tz);
	}

	tzset();

	time_t now = time(NULL);
	struct tm result = {0};

	if (localtime_r(&now, &result) == NULL) {
		reset_tz(old_tz);

		return eval_fail_with("Unable to calculate time in timezone %s (localtime_r failed)", new_tz);
	}

	char *buf = arena_alloc(arena, 6);
	buf[5] = '\0';

	snprintf(buf, 6, "%02d:%02d", result.tm_hour, result.tm_min);

	reset_tz(old_tz);

	sb_append_sv(out, arena, SV(buf));

	return 0;
}

int
ordinal_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb the_number = {0};

	if (eval_expr_list(list, ctx, arena, &the_number) < 0)
		return -1;

	char *_number = sb_to_string(&the_number, arena);
	if (!_number)
		return eval_fail_with("no input to ordinal");

	sv number = SV(_number);
	sb_append_sv(out, arena, number);

	if (number.length == 0)
		return 0;

	switch (number.data[number.length - 1]) {
	case '1':
		sb_append_sv(out, arena, SV("st"));
		break;
	case '2':
		sb_append_sv(out, arena, SV("nd"));
		break;
	case '3':
		sb_append_sv(out, arena, SV("rd"));
		break;
	default:
		sb_append_sv(out, arena, SV("th"));
		break;
	}

	return 0;
}

int
get_var_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb _var_name        = {0};
	sv variable_content = {0};

	if (eval_expr_list(list, ctx, arena, &_var_name) < 0)
		return -1;

	char *_var_name_str = sb_to_string(&_var_name, arena);
	if (!_var_name_str)
		return eval_fail_with("empty variable name to getvar");

	sv var_name = SV(_var_name_str);

	if (db_lookup_variable(arena, ctx->channel, var_name, &variable_content) < 0)
		return eval_fail_with(SV_FMT": variable not found in this channel", SV_ARGS(var_name));

	sb_append_sv(out, arena, variable_content);
	return 0;
}

int
set_var_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	if (list->exprs_size != 2)
		return eval_fail_with("set_var expects 2 arguments");

	sb _var_name    = {0};
	sb _var_content = {0};

	if (eval_expr(&list->exprs[0], ctx, arena, &_var_name) < 0)
		return -1;

	if (eval_expr(&list->exprs[1], ctx, arena, &_var_content) < 0)
		return -1;

	char *_var_name_str    = sb_to_string(&_var_name, arena);
	if (!_var_name_str)
		return eval_fail_with("empty variable name to setvar");

	char *_var_content_str = sb_to_string(&_var_content, arena);
	if (!_var_content_str)
		return eval_fail_with("Cannot clear variable by setting to empty string");

	sv var_name    = SV(_var_name_str);
	sv var_content = SV(_var_content_str);

	if (db_set_variable(ctx->channel, var_name, var_content) < 0)
		return eval_fail_with(SV_FMT": unable to set variable",
							  SV_ARGS(var_name));

	log_warn("Variable "SV_FMT" has been altered for channel "CHAN_FMT".",
			 SV_ARGS(var_name), CHAN_ARGS(ctx->channel));

	sb_append_sv(out, arena, SV("Variable has been set"));
	return 0;
}

int
at_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb    _nick    = {0};
	sv    nick     = {0};
	char *nick_str = NULL;

	if (eval_expr_list(list, ctx, arena, &_nick) < 0)
		return -1;

	nick_str = sb_to_string(&_nick, arena);
	if (!nick_str)
		return eval_fail_with(
			"It may come as a surprise to you, but a person"
			" without a name is not someone I know about.");

	nick = SV(nick_str);

	if (user_is_joined_to_channel(ctx->channel, nick)) {
		sb_append_sv(out, arena, nick);
		return 0;
	} else {
		return eval_fail_with("Who the fuck is %s?", nick_str);
	}
}

int
MODULE_LOAD_HOOK(void)
{
	modules_register_function("addcmd", addcmd_function_impl);
	modules_register_function("delcmd", delcmd_function_impl);
	modules_register_function("or", or_function_impl);
	modules_register_function("if", if_function_impl);
	modules_register_function("calc", calc_function_impl);
	modules_register_function("get_quote", getquote_function_impl);
	modules_register_function("add_quote", addquote_function_impl);
	modules_register_function("local_time", localtime_function_impl);
	modules_register_function("add_reminder", add_reminder_function_impl);
	modules_register_function("ordinal", ordinal_function_impl);
	modules_register_function("get_var", get_var_function_impl);
	modules_register_function("set_var", set_var_function_impl);
	modules_register_function("markov", markov_function_impl);
	modules_register_function("examine", examine_function_impl);
	modules_register_function("at", at_function_impl);
	modules_register_function("convertbase", convertbase_function_impl);
	modules_register_function("base64", base64_function_impl);
	modules_register_function("stalk", stalk_function_impl);

	return 0;
}

int
MODULE_UNLOAD_HOOK(void)
{
	modules_unregister_function("addcmd");
	modules_unregister_function("delcmd");
	modules_unregister_function("or");
	modules_unregister_function("if");
	modules_unregister_function("calc");
	modules_unregister_function("get_quote");
	modules_unregister_function("add_quote");
	modules_unregister_function("local_time");
	modules_unregister_function("add_reminder");
	modules_unregister_function("ordinal");
	modules_unregister_function("get_var");
	modules_unregister_function("set_var");
	modules_unregister_function("markov");
	modules_unregister_function("examine");
	modules_unregister_function("at");
	modules_unregister_function("convertbase");
	modules_unregister_function("base64");
	modules_unregister_function("stalk");

	return 0;
}
