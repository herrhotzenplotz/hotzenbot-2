/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/eval.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/sb.h>
#include <hotzenbot/stalk.h>

int
stalk_function_impl(expr_list *exprs, arena *arena, eval_ctx *ctx, sb *out)
{
	stalk_buf sbuf = {0};
	sb _nick = {0};
	sb _message = {0};
	char *nick = NULL;
	char *message = NULL;

	if (exprs->exprs_size != 1 && exprs->exprs_size != 2) {
		return eval_fail_with(
			"%stalk takes either one or two arguments: %stalk(nick[, message])");
	}

	/* Extract the nickname */
	if (eval_expr(&exprs->exprs[0], ctx, arena, &_nick) < 0)
		return -1;

	nick = sb_to_string(&_nick, arena);
	if (!nick) {
		return eval_fail_with("missing nick name");
	}

	/* optional message */
	if (exprs->exprs_size == 2) {
		if (eval_expr(&exprs->exprs[1], ctx, arena, &_message) < 0)
			return -1;

		message = sb_to_string(&_message, arena);
	}

	if (eval_ctx_lookup_variable(ctx, SV("sender"), &sbuf.requested_by) < 0)
		return eval_fail_with(
			"cannot issue stalk request from a non-user context");

	sbuf.request_channel = ctx->channel;
	sbuf.target_user = SV(nick);
	sbuf.message = SV(message);

	log_warn("stalk request by "SV_FMT" in "CHAN_FMT" for %s "SV_FMT" message",
			SV_ARGS(sbuf.requested_by),
			CHAN_ARGS(ctx->channel),
			nick,
			SV_ARGS(sbuf.message));

	if (stalk(&sbuf) < 0)
		return eval_fail_with("stalk request failed");

	sb_append_sv(out, arena, SV("stalk request succeeded"));

	return 0;
}
