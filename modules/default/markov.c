/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/markov.h>
#include <hotzenbot/eval.h>

int
markov_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb    seed_builder = {0};
	char *_seed;
	sv    seed;

	if (eval_expr_list(list, ctx, arena, &seed_builder) < 0)
		return -1;

	_seed = sb_to_string(&seed_builder, arena);
	if (_seed)
		seed = sv_trim_front(SV(_seed));
	else
		seed = (sv) {0};

	if (seed.length > 0) {
		if (markov_generate_sentence_seeded(arena, seed, out) < 0)
			return eval_fail_with("Error generating sentence from the markov chain");
	} else {
		if (markov_generate_sentence(arena, out) < 0)
			return eval_fail_with("Error generating sentence from the markov chain");
	}

	return 0;
}

int
examine_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb    seed_builder = {0};
	char *_seed;
	sv    seed;

	if (eval_expr_list(list, ctx, arena, &seed_builder) < 0)
		return -1;

	_seed = sb_to_string(&seed_builder, arena);
	if (_seed)
		seed = sv_trim_front(SV(_seed));
	else
		seed = (sv) {0};

	if (seed.length > 0) {
		if (markov_examine(arena, seed, out) < 0)
			return eval_fail_with("Examine failed");
		else
			return 0;
	}

	return eval_fail_with("Cannot lookup frequencies for empty seed.");
}
