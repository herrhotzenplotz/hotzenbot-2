/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/modules.h>
#include <hotzenbot/reminder.h>

int
add_reminder_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	/*
	 * A bit of natural language processing:
	 *
	 * The command is going to be invoked as something like that:
	 *  $remind me in 20 minutes to go check the stove
	 *
	 *  so what we'll need to do is to skip the `me`, parse the time,
	 *  skip the `to` and take the rest as the reminder message.
	 */

	sb      _reminder_raw = {0};
	char   *reminder_raw_str;
	sv      reminder_raw, timestamp_sv, text_sv, sender_sv;
	char   *stop_words[]  = { "to", "of", "about" };
	size_t  i;

	if (eval_expr_list(list, ctx, arena, &_reminder_raw) < 0)
		return -1;

	reminder_raw_str = sb_to_string(&_reminder_raw, arena);
	if (!reminder_raw_str)
		return eval_fail_with("usage: remind me in <interval> about/to/of ...");

	reminder_raw = SV(reminder_raw_str);

	reminder_raw = sv_trim_front(reminder_raw);

	if (sv_has_prefix(reminder_raw, "me")) {
		reminder_raw.data   += 2;
		reminder_raw.length -= 2;
		reminder_raw         = sv_trim_front(reminder_raw);
	}

	if (reminder_raw.length < 2)
		return eval_fail_with("unexpected end of input in reminder command");

	for (i = 0; i < reminder_raw.length - 1; ++i) {
		for (size_t stop_word_idx = 0; stop_word_idx < ARRAY_SIZE(stop_words); ++stop_word_idx) {
			if (strncmp(reminder_raw.data + i, stop_words[stop_word_idx], strlen(stop_words[stop_word_idx])) == 0) {
				/* this is the index we need! */

				timestamp_sv = sv_from_parts(reminder_raw.data, i);
				text_sv      = (sv) {
					.data = reminder_raw.data + i + strlen(stop_words[stop_word_idx]) + 1,
					.length = reminder_raw.length - strlen(stop_words[stop_word_idx]) - 1 - i
				};

				goto found_timestamp;
			}
		}
	}

	return eval_fail_with("I don't understand your reminder. Please specifiy when you want to reminded of what");

found_timestamp:
	timestamp_sv = sv_trim_front(timestamp_sv);
	text_sv      = sv_trim_front(text_sv);
	sender_sv    = (sv) {0};

	if (eval_ctx_lookup_variable(ctx, SV("sender"), &sender_sv) < 0)
		return eval_fail_with("Unable to find sender to set reminder for");

	size_t  tmp_buffer_len = sender_sv.length + 2 + text_sv.length;
	char   *tmp_buffer     = arena_alloc(arena, tmp_buffer_len);

	memcpy(tmp_buffer, sender_sv.data, sender_sv.length);
	tmp_buffer[sender_sv.length]     = ':';
	tmp_buffer[sender_sv.length + 1] = ' ';
	memcpy(tmp_buffer + sender_sv.length + 2, text_sv.data, text_sv.length);

	reminder_error err = add_reminder(
		ctx->channel,
		timestamp_sv,
		sv_from_parts(tmp_buffer, tmp_buffer_len));

	switch (err) {
	case RE_OK:
		sb_append_sv(out, arena, SV("Added reminder"));
		return 0;
	case RE_DBFAIL:
		return eval_fail_with("Database failure");
	case RE_TIMEPARSE:
		return eval_fail_with("Unable to parse reminder time");
	}

	log_fatal("unreachable in reminder.c");
	return eval_fail_with("Internal bot error.");
}
