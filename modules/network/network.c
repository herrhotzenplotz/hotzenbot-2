/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/eval.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/modules.h>

#include <ctype.h>
#include <string.h>

#if defined(USE_FETCH)
#include <sys/param.h>
#include <stdio.h>
#include <fetch.h>
#include "fetch.c"
#elif defined(USE_CURL)
#include <curl/curl.h>
#include "curl.c"
#else
#error "No networking library specified. Something is going wrong in the Makefile"
#endif

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

int
urlencode_impl(expr_list *args, arena *arena, eval_ctx *ctx, sb *out)
{
	sb text = {0};
	if (eval_expr_list(args, ctx, arena, &text) < 0)
		return -1;

	char *text_str = sb_to_string(&text, arena);
	if (!text_str)
		return 0;

	size_t  text_str_len = strlen(text_str);
	char   *buf          = arena_alloc(arena, 3 * text_str_len + 1);
	size_t  buf_size     = 0;

	for (size_t i = 0; i < text_str_len; ++i) {
		if (!isalnum(text_str[i])) {
			snprintf(buf + buf_size, 4, "%%%02X", text_str[i]);
			buf_size += 3;
		} else {
			buf[buf_size++] = text_str[i];
		}
	}

	sb_append_sv(out, arena, sv_from_parts(buf, buf_size));
	return 0;
}

int
dnslookup_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	struct addrinfo *addresses = NULL;
	struct addrinfo	 hints	   = {0};
	int				 rc		   = 0;
	sb				 hostname_ = {0};

	if (eval_expr_list(list, ctx, arena, &hostname_) < 0)
		return -1;

	char *hostname = sb_to_string(&hostname_, arena);
	if (!hostname || strlen(hostname) == 0) /* Let's just hope this short-circuits */
		return eval_fail_with("%%dnslookup: hostname must not be empty");

	hints.ai_family = AF_INET;

	if ((rc = getaddrinfo(hostname, NULL, &hints, &addresses)) < 0)
		return eval_fail_with(
			"%%dnslookup: unable to resolve address: %s",
			gai_strerror(rc));

	if (!addresses)
		return eval_fail_with("%%dnslookup: name does not resolve");

	/* Thank you BSD for the wonderful socket mess */
#define len 16
	char *buffer = arena_alloc(arena, len);
	/* Be on the safe side */
	memset(buffer, 0, len);

	struct in_addr addr = ((struct sockaddr_in *)(addresses->ai_addr))->sin_addr;
	inet_ntoa_r(addr, buffer, len);

	sb_append_sv(out, arena, sv_from_parts(buffer, len));
#undef len

	freeaddrinfo(addresses);

	return 0;
}

int
MODULE_LOAD_HOOK(void)
{
	modules_register_function("curl", curl_function_impl);
	modules_register_function("curl_plain", curl_plain_function_impl);
	modules_register_function("urlencode", urlencode_impl);
	modules_register_function("dnslookup", dnslookup_impl);

	return 0;
}

int
MODULE_UNLOAD_HOOK(void)
{
	modules_unregister_function("curl");
	modules_unregister_function("curl_plain");
	modules_unregister_function("urlencode");
	modules_unregister_function("dnslookup");

	return 0;
}
