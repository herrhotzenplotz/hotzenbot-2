#!/bin/sh

die()
{
    echo "error: "$1 1>&2
    exit 1
}

do_curl()
{
    if [ "`uname`" = "SunOS" ]; then
        echo "-I${CURLBASE-/usr/local}/include -DUSE_CURL"
    else
        which pkg-config 2>/dev/null > /dev/null
        [ $? -eq 0 ] || die "pkg-config not found"

        echo "`pkg-config --cflags libcurl` -DUSE_CURL"
    fi
}

if [ "$(uname)" = "FreeBSD" ]; then
    if [ $USE_CURL ]; then
        do_curl
        exit 0
    else
        echo "-DUSE_FETCH"
        exit 0
    fi
else
    do_curl
    exit 0
fi
