/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

static int
fetch_internal(expr_list *args, arena *arena, eval_ctx *ctx, sb *out, int plain)
{
	sb url_sb = {0};

	if (eval_expr_list(args, ctx, arena, &url_sb) < 0)
		return -1;

	char *url = sb_to_string(&url_sb, arena);
	if (!url)
		return eval_fail_with("%%curl_plain: empty url");

	/* Fetch is a little weird but eeh */
	if (plain)
		setenv("HTTP_ACCEPT", "text/plain", 0);

	FILE *response = fetchGetURL(url, "");

	if (plain)
		unsetenv("HTTP_ACCEPT");

	if (!response)
		return eval_fail_with("Unable to perform HTTP request");

	while (!feof(response)) {
		char buf[64] = {0};

		size_t n_bytes = fread(buf, 1, sizeof(buf), response);
		if (n_bytes == 0)
			break;

		char *tmp = arena_alloc(arena, n_bytes);
		memcpy(tmp, buf, n_bytes);
		sb_append_sv(out, arena, sv_from_parts(tmp, n_bytes));
	}

	fclose(response);
	return 0;
}

int
curl_plain_function_impl(expr_list *args, arena *arena, eval_ctx *ctx, sb *out)
{
	return fetch_internal(args, arena, ctx, out, 1);
}

int
curl_function_impl(expr_list *args, arena *arena, eval_ctx *ctx, sb *out)
{
	return fetch_internal(args, arena, ctx, out, 0);
}
