#!/bin/sh

do_curl()
{
    if [ `uname` = "SunOS" ]; then
        echo "-Xlinker -L${CURLBASE-/opt/bw}/lib -Xlinker -R${CURLBASE-/usr/local}/lib -lcurl"
    else
        printf -- "`pkg-config --libs libcurl`"
    fi
}

if [ "$(uname)" = "FreeBSD" ]; then
   if [ $USE_CURL ]; then
       do_curl
       exit 0
   else
       printf -- "-L/usr/lib -lfetch"
       exit 0
   fi
else
    do_curl
    exit 0
fi
