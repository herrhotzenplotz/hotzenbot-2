/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

static ssize_t
write_cb(void *buffer, size_t size, size_t nemb, void *ctx)
{
	sv *out = (sv *)ctx;

	out->data = realloc(out->data, out->length + size * nemb);
	memcpy(out->data + out->length, buffer, size * nemb);
	out->length += size * nemb;
	return size * nemb;
}

static int
curl_internal(expr_list *args, arena *arena, eval_ctx *ctx, sb *out, int plain)
{
	sb url_sb = {0};

	if (eval_expr_list(args, ctx, arena, &url_sb) < 0)
		return -1;

	char *url = sb_to_string(&url_sb, arena);
	if (!url)
		return eval_fail_with("%%curl: empty url");

	CURL *curl = curl_easy_init();
	if (!curl)
		return eval_fail_with("unable to init curl");

	struct curl_slist *headers = NULL;
	if (plain)
		headers = curl_slist_append(headers, "Accept: text/plain");

	sv content = {0};
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_cb);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &content);

	if (plain)
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	CURLcode res = curl_easy_perform(curl);

	if (plain)
		curl_slist_free_all(headers);

	if (res != CURLE_OK) {
		log_error("error from libcurl during GET request to %s: %s", url, curl_easy_strerror(res));

		curl_easy_cleanup(curl);

		if (content.data)
			free(content.data);

		return eval_fail_with("libcurl error. Please see log file.");
	}

	curl_easy_cleanup(curl);

	char *buffer = arena_alloc(arena, content.length);
	memcpy(buffer, content.data, content.length);
	free(content.data);

	sb_append_sv(out, arena, sv_from_parts(buffer, content.length));

	return 0;
}

int
curl_function_impl(expr_list *args, arena *arena, eval_ctx *ctx, sb *out)
{
	return curl_internal(args, arena, ctx, out, 0);
}

int
curl_plain_function_impl(expr_list *args, arena *arena, eval_ctx *ctx, sb *out)
{
	return curl_internal(args, arena, ctx, out, 1);
}
