/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/modules.h>
#include <hotzenbot/logging.h>

#include <signal.h>
#include <errno.h>

/* error strings stolen from the FreeBSD manual */
static const char *
bus_errorstr(int error)
{
	switch (error) {
	case BUS_ADRALN:
		return "invalid address alignment";
	case BUS_ADRERR:
		return "nonexistent physical address";
	case BUS_OBJERR:
		return "object-specific hardware error";
#if defined(BUS_OOMERR)
	case BUS_OOMERR:
		return "cannot alloc a page to map at fault";
#endif
	default:
		return "<invalid bus error code>";
	}
}

static const char *
segv_errorstr(int error)
{
	switch (error) {
	case SEGV_MAPERR:
		return "address not mapped to object";
	case SEGV_ACCERR:
		return "invalid permissions for mapped object";
	default:
		return "<invalid segv error code>";
	}
}

void
debug_signal_handler(int signum, siginfo_t *info, void *data)
{
	(void) data;

	if (signum == SIGSEGV) {
		log_fatal("SIGSEGV : Segmentation violation.");
		log_fatal("        : Fault address : %p"    , info->si_addr);
#if !defined(__linux__)
		log_fatal("        : Trap number   : 0x%04X", info->si_trapno);
#endif
		log_fatal("        : PID           : %zu"   , info->si_pid);
		log_fatal("        : Errno         : %s"    , strerror(info->si_errno));
		log_fatal("        : Code          : %s"    , segv_errorstr(info->si_code));
		log_fatal("Aborting.");
		abort();
	}
#if defined(SIGPIPE)
	else if (signum == SIGPIPE) {
		log_error("SIGPIPE received");
		return;
	}
#endif /* SIGPIPE */
	else if (signum == SIGBUS) {
		log_fatal("SIGBUS  : Bus error.");
		log_fatal("        : Fault address : %p"    , info->si_addr);
#if !defined(__linux__)
		log_fatal("        : Trap number   : 0x%04X", info->si_trapno);
#endif
		log_fatal("        : PID           : %zu"   , info->si_pid);
		log_fatal("        : Errno         : %s"    , strerror(info->si_errno));
		log_fatal("        : Code          : %s"    , bus_errorstr(info->si_code));

		if (info->si_code == BUS_ADRALN)
			log_info("Buggy allocator? memmoving structs?");

		log_fatal("Aborting.");
		abort();
	} else {
		log_error("Unknown signal received");
	}
}

int
setup_debug_signals(void)
{
	struct sigaction sa = {
		.sa_sigaction = debug_signal_handler,
		.sa_flags     = SA_RESTART|SA_SIGINFO,
	};

#if defined(SIGPIPE)
	if (sigaction(SIGPIPE, &sa, NULL) < 0) {
		log_error("Unable to install signal handler for SIGPIPE: %s",
				  strerror(errno));
		return -1;
	}
#endif /* SIGPIPE */

	if (sigaction(SIGSEGV, &sa, NULL) < 0) {
		log_error("Unable to install signal handler for SIGSEGV: %s",
				  strerror(errno));
		return -1;
	}

	if (sigaction(SIGBUS, &sa, NULL) < 0) {
		log_error("Unable to install signal handler for SIGBUS: %s",
				  strerror(errno));
		return -1;
	}

	log_info("Successfully hooked debug signals");

	return 0;
}

int
MODULE_LOAD_HOOK(void)
{
	return setup_debug_signals();
}

int
MODULE_UNLOAD_HOOK(void)
{
	struct sigaction sa = {
		.sa_handler = SIG_DFL,
		.sa_flags   = SA_RESTART,
	};

	if (sigaction(SIGSEGV, &sa, NULL) < 0)
		log_error("Unable to unhook SIGSEGV handler");

	if (sigaction(SIGBUS, &sa, NULL) < 0)
		log_error("Unable to unhook SIGBUS handler");

#if defined(SIGPIPE)
	if (sigaction(SIGPIPE, &sa, NULL) < 0)
		log_error("Unable to unhook SIGPIPE handler");
#endif /* SIGPIPE */

	return 0;
}
