/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/modules.h>

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

/***********************************************************************
 * Comms stuff
 **********************************************************************/

static pid_t       beep_pid;
static char const *listenhost = "0.0.0.0";
static char const *port       = "42069";

static int listenfd = -1;
static int should_exit = 0;
static int writepipe, readpipe;

struct peer {
	char name[64];
	struct sockaddr addr;
	socklen_t addr_len;
	char *event;
	time_t last_pong;
};

static struct peer *peers = NULL;
static size_t peers_size = 0;
#define MAX_PEERS 4

/* Messages are exchanged in pairs of event-name;event-data */
static void
sendtobeeper(char const *eventname, char const *data, size_t const data_size)
{
	size_t buf_size = 0, buf_len = 0;
	char *buf = NULL;
	size_t const evlen = strlen(eventname);

	buf_size += evlen;
	buf_size += 1; /* for the semicolon */
	buf_size += data_size;

	buf = malloc(buf_size);

	memcpy(buf, eventname, evlen);
	buf_len += evlen;

	buf[buf_len++] = ';';
	memcpy(buf + buf_len, data, data_size);

	write(writepipe, buf, buf_size);
	free(buf);
}

static int
recvfrommain(char **event, char **data)
{
	char *buf;
	size_t datasize;

	if (ioctl(readpipe, FIONREAD, &datasize) < 0) {
		log_error("ioctl: %s", strerror(errno));
		return -1;
	}

	buf = malloc(datasize);
	if (buf == NULL) {
		log_error("could not allocate memory. Just download more RAM: %s", strerror(errno));
		return -1;
	}

	if (read(readpipe, buf, datasize) < 0) {
		log_error("Failed to read: %s", strerror(errno));
		return -1;
	}

	/* extract the event name */
	*event = buf;
	buf = strchr(buf, ';');
	*buf++ = '\0';
	*data = buf;

	return 0;
}

static void
sighup_handler(int sig)
{
	(void) sig;

	log_info("Beep process received SIGHUP...");
	should_exit = 1;
}

static int
sockaddr_to_str(struct sockaddr const *const sa, char *buf, size_t buflen)
{
	switch (sa->sa_family) {
	case AF_INET:
		inet_ntop(sa->sa_family,
		          &((struct sockaddr_in const *)(sa))->sin_addr,
		          buf, buflen);
		return 0;
	case AF_INET6:
		inet_ntop(sa->sa_family,
		          &((struct sockaddr_in6 const *)(sa))->sin6_addr,
		          buf, buflen);
		return 0;
	default:
		strncpy(buf, "addrfamily?", buflen);
		return -1;
	}
}

static int
setup_listener(void)
{
	struct addrinfo hints = {0}, *res = NULL;
	int error, one = 1;

	log_info("Starting beep listener");

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_SEQPACKET;
	hints.ai_protocol = IPPROTO_SCTP;

	/* Resolve the address */
	if ((error = getaddrinfo(listenhost, port, &hints, &res))) {
		log_error("getaddrinfo: %s", gai_strerror(error));
		return -1;
	}

	/* Open a socket */
	listenfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (listenfd < 0) {
		log_error("socket: %s", strerror(errno));
		return -1;
	}

	/* make the local address reusable */
	if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) < 0) {
		log_error("Could not set SO_REUSEADDR on listener fd");
		close(listenfd);
		return -1;
	}

	/* Allow catching association state changes on the sockets */
	struct sctp_event ev = {0};
	ev.se_assoc_id = SCTP_FUTURE_ASSOC;
	ev.se_type = SCTP_ASSOC_CHANGE;
	ev.se_on = 1;

	if (setsockopt(listenfd, IPPROTO_SCTP, SCTP_EVENT, &ev, sizeof(ev)) < 0) {
		log_info("setsockopt: %s", strerror(errno));
		return -1;
	}

	/* bind to the local address */
	if (bind(listenfd, res->ai_addr, res->ai_addrlen) < 0) {
		log_error("bind: %s", strerror(errno));
		close(listenfd);
		return -1;
	}

	/* accept incoming connections */
	listen(listenfd, 5);

	freeaddrinfo(res);

	return 0;
}

static void
remove_peer(struct peer *p)
{
	log_info("Deleting beep peer for event %s...", p->event);
	free(p->event);
	p->name[0] = '\0';
	size_t i = (p - peers) / sizeof(*p);
	peers[i] = peers[--peers_size];
}

static struct peer *
findpeer(struct sockaddr const *paddr, socklen_t const paddr_len)
{
	/* Search for a associated peer */
	for (size_t i = 0; i < peers_size; ++i) {
		if (memcmp(paddr, &peers[i].addr, sizeof(*paddr)) == 0)
			return &peers[i];
	}

	/* Otherwise reserve a new one */
	if (peers_size == MAX_PEERS) {
		log_info("Maximum number of peers reached: %d", MAX_PEERS);
		return NULL;
	}

	if (sockaddr_to_str(paddr, peers[peers_size].name, sizeof(peers[peers_size].name)) < 0)
		log_warn("Connection from unknown AF");
	else
		log_info("Connection from %s received!", peers[peers_size].name);

	peers[peers_size].addr = *paddr;
	peers[peers_size].addr_len = paddr_len;
	peers[peers_size].event = NULL;
	peers[peers_size].last_pong = time(NULL);

	return &peers[peers_size++];
}

static void
sendtopeer(struct peer const *peer, char *data)
{
	ssize_t rc;
	size_t const datalen = strlen(data);

	rc = sendto(listenfd, data, datalen, 0, &peer->addr, peer->addr_len);
	if (rc < 0)
		log_error("sendto %s in sendtopeer failed: %s", peer->name, strerror(errno));

	if ((size_t)rc != datalen)
		log_error("Beep event truncated");
}

static void
pipefd_event(void)
{
	char *eventname;
	char *data;

	/* receive the request */
	if (recvfrommain(&eventname, &data) < 0) {
		log_fatal("Could not receive beep event data from bot main loop");
		_Exit(EXIT_FAILURE);
	}

	log_info("Distributing beep event for %s", eventname);

	/* distribute event to peers */
	for (size_t i = 0; i < peers_size; ++i) {
		if (strcmp(peers[i].event, eventname) == 0)
			sendtopeer(&peers[i], data);
	}

	free(eventname);
	free(data);
}

static void
beer_subscription(struct peer *peer, char *const buf, size_t buf_size)
{
	free(peer->event);

	peer->event = calloc(buf_size - 4, 1);
	strcpy(peer->event, buf + 4);
	log_info("Peer %s: subscription for %s", peer->name, buf + 4);
}

static void
peer_statechange(struct peer *peer, union sctp_notification const *n)
{
	assert(n->sn_header.sn_type == SCTP_ASSOC_CHANGE);
	uint8_t t = n->sn_assoc_change.sac_state;

	switch (t) {
	case SCTP_COMM_UP:
		log_info("Associated with new peer %s", peer->name);
		break;
	case SCTP_SHUTDOWN_COMP:
		log_info("Shutdown from peer %s", peer->name);
		remove_peer(peer);
		break;
	case SCTP_COMM_LOST:
		log_info("Lost association with peer %s", peer->name);
		remove_peer(peer);
		break;
	case SCTP_RESTART:
		log_info("Communication restarted with peer %s", peer->name);
		break;
	case SCTP_CANT_STR_ASSOC:
		log_info("Failed to start communication with peer %s", peer->name);
		remove_peer(peer);
		break;
	default:
		log_warn("Unhandled association state change with peer: %s",
		         peer->name);
		break;
	}
}

static void
terminate_assoc(struct sockaddr *paddr, socklen_t const paddrlen)
{
	ssize_t rc;
	char tmp[64] = {0};

	rc = sctp_sendmsg(listenfd, NULL, 0, paddr, paddrlen, 0,
	                  SCTP_ABORT, 0, 0, 0);
	if (rc < 0) {
		sockaddr_to_str(paddr, tmp, sizeof tmp);
		log_error("Failed to terminate association to peer %s", tmp);
	}
}

static void
listenfd_event(void)
{
	struct sockaddr paddr = {0};
	struct peer *p;
	int avail;
	struct msghdr mh = {0};
	struct iovec iov = {0};
	ssize_t actual;

	if (ioctl(listenfd, FIONREAD, &avail) < 0)
		abort();

	iov.iov_base = calloc(avail, 1);
	iov.iov_len = avail;
	mh.msg_iov = &iov;
	mh.msg_iovlen = 1;
	mh.msg_name = &paddr;
	mh.msg_namelen = sizeof paddr;
	mh.msg_flags = MSG_NOTIFICATION;

	actual = recvmsg(listenfd, &mh, MSG_NOTIFICATION);
	if (actual < 0) {
		log_error("recvmsg: %s", strerror(errno));
		abort();
	}

	if (actual != avail)
		log_warn("Received number of bytes differs from expected: "
		         "%ld vs %ld", actual, avail);

	if ((p = findpeer(&paddr, mh.msg_namelen)) == NULL) {
		terminate_assoc(&paddr, mh.msg_namelen);
		return;
	}

	if (mh.msg_flags & MSG_NOTIFICATION) {
		peer_statechange(p, iov.iov_base);
	} else {
		char *buf = mh.msg_iov[0].iov_base;
		size_t const buflen = mh.msg_iov[0].iov_len;

		if (buf[0] == 'S' && buf[1] == 'U' && buf[2] == 'B' && buf[3] == ' ')
			beer_subscription(p, buf, buflen);
		else
			log_warn("Bad event »%s« from peer %s", buf, p->name);
	}

	free(iov.iov_base);
}

static void
beepproc_error(char const *msg)
{
	log_error("beep process: %s", msg);
	_Exit(1);
}

static void
beep_proc(void)
{
	log_info("Beep process starting...");
	/* Catch sighup */
	signal(SIGHUP, sighup_handler);

	if (setup_listener() < 0)
		beepproc_error("Couldn't setup beep listener");

	peers = calloc(sizeof(*peers), MAX_PEERS);

	for (;;) {
		enum { fds_size = 2, listen_idx = 0, pipe_idx = 1 };
		struct pollfd fds[fds_size] = {
			[listen_idx] = { .fd = listenfd, .events = POLLIN|POLLHUP, .revents = 0, },
			[pipe_idx] = { .fd = readpipe, .events = POLLIN|POLLHUP, .revents = 0, },
		};
		int rc;

		rc = poll(fds, fds_size, -1);
		if (rc < 0) {
			if (errno == EINTR) {
				if (should_exit)
					log_info("Beep process terminating...");
				else
					log_fatal("Beep process was interrupted but exit flag wasn't set. Exiting.\n");
			} else {
				log_fatal("poll returned with error: %s. Beep process terminating.", strerror(errno));
			}
			return;
		}

		if (fds[listen_idx].revents & POLLERR)
			beepproc_error("listenfd poll error");
		if (fds[pipe_idx].revents & POLLERR)
			beepproc_error("readpipe poll error");

		if (fds[listen_idx].revents & POLLHUP)
			beepproc_error("listenfd hung up");
		if (fds[pipe_idx].revents & POLLHUP)
			beepproc_error("readpipe hung up");

		if (fds[listen_idx].revents & POLLIN)
			listenfd_event();

		if (fds[pipe_idx].revents & POLLIN)
			pipefd_event();
	}
}

/***********************************************************************
 * Bot callback functions
 **********************************************************************/
static int
beep_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	(void) out;

	if (list->exprs_size != 2)
		return eval_fail_with("usage: %%beep(event, data)");

	sb event = {0};
	sb data  = {0};

	if (eval_expr(&list->exprs[0], ctx, arena, &event) < 0)
		return -1;

	if (eval_expr(&list->exprs[1], ctx, arena, &data) < 0)
		return -1;

	char *event_str = sb_to_string(&event, arena);
	char *data_str  = sb_to_string(&data, arena);

	sendtobeeper(event_str, data_str, strlen(data_str));

	return 0;
}

int
MODULE_LOAD_HOOK(void)
{
	pid_t p;
	int fds[2];

	log_info("Beep module starting up...");

	if (socketpair(PF_UNIX, SOCK_SEQPACKET, 0, fds) < 0) {
		log_info("socketpair failed: %s", strerror(errno));
		return -1;
	}

	readpipe = fds[0];
	writepipe = fds[1];

	p = fork();
	if (p == 0) {
		/* child */
		beep_proc();
		_Exit(EXIT_SUCCESS);
	} else if (p > 0) {
		beep_pid = p;
	} else {
		log_error("Could not start beep listener process");
		return -1;
	}

	modules_register_function("beep", beep_function_impl);

	return 0;
}

int
MODULE_UNLOAD_HOOK(void)
{
	if (kill(beep_pid, SIGHUP) < 0) {
		log_error("Could not deliver SIGHUP to beep child process: %s",
		          strerror(errno));
	} else {
		int status;
		if (waitpid(beep_pid, &status, WEXITED) < 0)
			log_error("Could not wait for child to exit: %s",
			          strerror(errno));
		else
			log_info("Beep child has exited with code %d",
			         WEXITSTATUS(status));
	}

	modules_unregister_function("beep");

	return 0;
}
