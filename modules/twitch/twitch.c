/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/db.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/modules.h>

/* We are going to store the information of shoutout commands in
 * channel-specific variables of the names shoutout_channel
 */
int
shoutout_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
	sb _variable_name = {0};
	sb _channel_name  = {0};

	if (eval_expr_list(list, ctx, arena, &_channel_name) < 0)
		return -1;

	char *_channel_name_str = sb_to_string(&_channel_name, arena);
	if (!_channel_name_str)
		return eval_fail_with("Missing channel");

	sv channel_name = SV(_channel_name_str);

	sb_append_sv(&_variable_name, arena, SV("shoutout_"));
	sb_append_sv(&_variable_name, arena, channel_name);

	char *_variable_name_str = sb_to_string(&_variable_name, arena);
	if (!_variable_name_str)
		return 0;

	sv variable_name = SV(_variable_name_str);
	sv message = {0};
	if (db_lookup_variable(arena, ctx->channel, variable_name, &message) < 0) {
		sb_append_sv(out, arena, SV("Who the fuck is "));
		sb_append_sv(out, arena, channel_name);
		sb_append_sv(out, arena, SV("?"));
	} else {
		sb_append_sv(out, arena, message);
	}

	return 0;
}

int
MODULE_LOAD_HOOK(void)
{
	modules_register_function("shoutout", shoutout_function_impl);

	return 0;
}

int
MODULE_UNLOAD_HOOK(void)
{
	modules_unregister_function("shoutout");

	return 0;
}
