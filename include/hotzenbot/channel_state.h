/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CHANNEL_STATE_H
#define CHANNEL_STATE_H

#include <hotzenbot/channel.h>
#include <hotzenbot/socket.h>

typedef enum lookup_result {
	LOOKUP_OK = 0,
	LOOKUP_FAILED,
	LOOKUP_NOT_REGISTERED
} lookup_result;

void          irc_rejoin_channels_after_reconnect(irc_socket *);
void          irc_user_whois_registered(sv network, sv nick, sv account);
void          irc_user_whois_unregistered(sv network, sv nick);
void          channel_set_topic(channel channel, sv topic);
int           channel_get_topic(channel channel, sv *out);
lookup_result irc_user_get_account(sv network, sv nick, sv *out);
sv            channel_get_prefix(channel channel);
void          channel_set_prefix(channel channel, sv prefix);
int           channel_should_lurk(channel channel);
void          channel_set_lurk_mode(channel channel, int enable);
void          channel_user_joined(channel channel, sv nick);
void          channel_user_parted(channel channel, sv nick);
int           user_is_joined_to_channel(channel channel, sv nick);
void          channel_state_dump(void);

#endif /* CHANNEL_STATE_H */
