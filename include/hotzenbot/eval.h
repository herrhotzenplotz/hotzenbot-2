/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef EVAL_H
#define EVAL_H

#include <hotzenbot/arena.h>
#include <hotzenbot/channel.h>
#include <hotzenbot/pipe.h>
#include <hotzenbot/sb.h>

#define FUNCTION_CALL_MAX_ARGS 8

typedef enum atom_kind {
	ATOM_KIND_FUNCALL,
	ATOM_KIND_VARIABLE,
	ATOM_KIND_TEXT
} atom_kind;

typedef struct expr_list expr_list;
typedef struct expr_atom expr_atom;

struct expr_atom {
	union {
		struct {
			sv function_name;
			expr_list *args;
		} function_call;

		sv variable;
		sv text;
	};

	atom_kind kind;
};

typedef struct eval_ctx {
	channel channel;
	sv      variables_names[64];
	sv      variables_values[64];
	size_t  variables_size;
} eval_ctx;

struct expr_list {
	expr_atom exprs[64];
	size_t    exprs_size;
};

int   parse_expr(sv in, arena *, expr_list *out);
int   eval_expr_list(expr_list *, eval_ctx *, arena *, sb *out);
int   eval_expr(expr_atom *, eval_ctx *, arena *, sb *out);
char *eval_get_error(void);
char *run_pipe(cmd_pipe *p, eval_ctx *, arena *arena);
void  eval_ctx_set_variable(eval_ctx *, char *, sv);
int   eval_ctx_lookup_variable(eval_ctx *, sv, sv *out);
int   eval_fail_with(char *fmt, ...);


#endif /* EVAL_H */
