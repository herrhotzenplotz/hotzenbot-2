/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DB_H
#define DB_H

#include <hotzenbot/arena.h>
#include <hotzenbot/hook.h>
#include <hotzenbot/reminder.h>
#include <hotzenbot/stalk.h>
#include <hotzenbot/sv.h>
#include <hotzenbot/timer.h>

typedef struct db_examine_result db_examine_result;

enum {
	DB_OK = 0,
	DB_FAILURE,
	DB_NOSUCHGROUP,
};

struct db_examine_result {
	sv  word2;
	int frequency;
};

void   init_database(char *db, int perform_initial_setup);
void   close_database(void);
int    db_begin_transaction(void);
int    db_rollback_transaction(void);
int    db_commit_transaction(void);
int    db_get_command_definition(char *command_name, arena *, sv *out);
int    db_is_user_trusted(sv network, sv account);
int    db_is_user_ignored(sv network, sv account);
int    db_is_user_authority(sv network, sv account);
int    db_is_user_maintainer(sv network, sv account);
int    db_add_command(sv name, sv definition);
int    db_del_commandname(sv cmdname);
int    db_assign_group(sv group, sv network, sv account);
int    db_get_random_quote(channel channel, arena *mem, sv *out);
int    db_addquote(channel channel, sv quote);
int    db_read_timers(arena *, timer **);
int    db_bump_timer(int id, time_t next);
int    db_get_channel_prefix(int id, arena *mem, sv *out);
int    db_add_reminder(channel channel, sv text, time_t reminder_trigger_time);
size_t db_get_reminders(arena *, reminder **);
int    db_delete_reminder_by_id(int);
int    db_find_hook(arena *, sv network, sv channel, sv message, hook *);
int    db_bump_times_hook(hook *);
int    db_lookup_variable(arena *, channel channel, sv variable_name, sv *out);
int    db_set_variable(channel channel, sv variable_name, sv variable_content);
int    db_generate_markov_sentence_seeded(arena *arena, sv seed, sb *out);
int    db_add_markov_sentence(sv sentence);
int    db_generate_markov_sentence(arena *arena, sb *out);
int    db_markov_examine(arena *arena, sv seed, db_examine_result **results);

/* See stalk.h description of struct stalk_buf */
int    db_add_stalk(channel channel, sv src_user, sv tgt_user, sv message);
int    db_query_stalk(arena *arena, sv network, sv nick, stalk_buf **out);
int    db_delete_stalk(int id);
#endif /* DB_H */
