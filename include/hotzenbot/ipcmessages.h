/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef IPCMESSAGES_H
#define IPCMESSAGES_H

#include <stdlib.h>

#define ipc_socket_path "/var/tmp/hotzenbotctl.pipe"

typedef enum {
	IPCMSGTYPE_JOIN,
	IPCMSGTYPE_PART,
	IPCMSGTYPE_SHUTDOWN,
	IPCMSGTYPE_PRIVMSG,
	IPCMSGTYPE_LOADMOD,
	IPCMSGTYPE_UNLOADMOD,
	IPCMSGTYPE_SETPREFIX,
	IPCMSGTYPE_LURK,
	IPCMSGTYPE_UNLURK,
} ipcmessage_type;

typedef struct ipcmessage ipcmessage;

struct ipcmessage {
	ipcmessage_type type;
	char            channel[64];
	size_t          channel_size;
	char            params[64];
	size_t          params_size;
};

#endif /* IPCMESSAGES_H */
