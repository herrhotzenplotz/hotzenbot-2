/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOCKET_H
#define SOCKET_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <openssl/ssl.h>

#include <hotzenbot/config.h>

typedef struct irc_socket irc_socket;

struct irc_socket {
	char                read_buf[4096]; /* Input buffer                                     */
	int                 read_buf_size;  /* Input buffer size                                */
	int                 irc_fd;         /* Underlying socket file descriptor                */
	SSL                *ssl;            /* Context for OpenSSL if config->secure_connection */
	config_entry       *config;         /* Config section associated with this socket       */
	struct sockaddr_in  sa;             /* Remote address for reconnecting                  */
};

void  irc_socket_init(irc_socket *socket, config_entry *entry);
void  irc_socket_close(irc_socket *socket);
void  irc_socket_connect(irc_socket *);
void  irc_socket_reconnect(irc_socket *);
int   irc_socket_pending(irc_socket *);
char *irc_socket_read_line(irc_socket *);
int   irc_socket_write(irc_socket *, const char *buf, size_t buf_size);

#endif /* SOCKET_H */
