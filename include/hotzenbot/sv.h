/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SV_H
#define SV_H

#include <hotzenbot/arena.h>

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define SV_ARGS(x) (int)(x.length), x.data
#define SV_FMT "%.*s"
#define SV(x) (sv) { .data = x, .length = (x) ? strlen(x) : 0 }
#define SV_NULL (sv) {0}

typedef struct sv sv;

struct sv {
	char   *data;
	size_t  length;
};

static inline sv
sv_from_parts(char *data, size_t len)
{
	return (sv) { .data = data, .length = len };
}

static inline bool
sv_null(sv it)
{
	return !it.data || !it.length;
}

int sv_eq(sv, sv);
int sv_eq_to_cstr(sv, const char *);
sv  sv_trim_front(sv);
sv  sv_trim_end(sv);
sv  sv_trim(sv);
sv  sv_dup_into_arena(arena *, sv);
int sv_has_prefix(sv, const char *);
sv  sv_chop_by_delim(sv *, const char);
sv  sv_dup(sv);

#endif /* SV_H */
