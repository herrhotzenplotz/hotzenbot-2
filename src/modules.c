/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/modules.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/helper.h>

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static char         *bot_module_names[32]     = {0};
static void         *bot_modules[32]          = {0};
static size_t        bot_modules_size         = 0;
static sv            bot_functions_names[128] = {0};
static eval_function bot_functions[128]       = {0};
static size_t        bot_functions_size       = 0;

static int
module_idx(char *path)
{
	for (size_t i = 0; i < bot_modules_size; ++i) {
		if (strcmp(path, bot_module_names[i]) == 0) {
			return i;
		}
	}

	return -1;
}

void
modules_load_module(char *path)
{
	if (module_idx(path) != -1) {
		log_error("Trying to load module that is already loaded");
		return;
	}

	if (access(path, F_OK) < 0) {
		log_error("Trying to load a non-existing dynamic shared object");
		return;
	}

	void *link = dlopen(path, RTLD_LAZY);
	if (!link) {
		log_fatal("Unable to load module '%s': %s", path, dlerror());
		return;
	}

	module_hook load_hook = (module_hook)dlsym(link, STR(MODULE_LOAD_HOOK));
	if (!load_hook) {
		log_fatal("Unable to load module '%s': %s", path, dlerror());
		abort();
	}

	if (load_hook() < 0) {
		log_fatal("Unable to initialize module");
		abort();
	}

	bot_module_names[bot_modules_size] = strdup(path);
	bot_modules[bot_modules_size] = link;
	bot_modules_size += 1;

	log_warn("loaded module %s", path);
}

void
modules_unload_module(char *path)
{
	int i = module_idx(path);
	if (i < 0) {
		log_error("Request to unload non-loaded module %s", path);
		return;
	}

	module_hook unload_hook = (module_hook)(dlsym(bot_modules[i], STR(MODULE_UNLOAD_HOOK)));
	if (!unload_hook)
		log_warn("Unable to find unload_hook for module. Forcibly unloading without calling unload hook...");
	else
		unload_hook();

	dlclose(bot_modules[i]);

	/* Free the strduped-module path */
	free(bot_module_names[i]);

	memmove(&bot_modules[i],
			&bot_modules[i + 1],
			sizeof(bot_modules[0]) * (bot_modules_size - i - 1));
	memmove(&bot_module_names[i],
			&bot_module_names[i + 1],
			sizeof(bot_module_names[0]) * (bot_modules_size - i - 1));

	bot_modules_size -= 1;

	log_warn("unloaded module %s", path);
}

void
modules_register_function(char *_name, eval_function f)
{
	if (bot_functions_size >= sizeof(bot_functions) / sizeof(bot_functions[0])) {
		log_fatal("Too many eval-functions have been registered in the bot");
		abort();
	}

	sv name = SV(_name);
	bot_functions_names[bot_functions_size] = name;
	bot_functions[bot_functions_size]       = f;
	bot_functions_size += 1;
}

void
modules_unregister_function(char *_name)
{
	if (bot_functions_size == 0) {
		log_fatal("No eval-functions have been registered in the bot");
		return;
	}

	sv name = SV(_name);
	size_t i;
	for (i = 0; i < bot_functions_size; ++i)
		if (sv_eq(name, bot_functions_names[i]))
			break;

	memmove(&bot_functions_names[i],
			&bot_functions_names[i + 1],
			(bot_functions_size - i - 1) * sizeof(bot_functions_names[0]));
	memmove(&bot_functions[i],
			&bot_functions[i + 1],
			(bot_functions_size - i - 1) * sizeof(bot_functions[0]));

	bot_functions_size -= 1;
}

eval_function
modules_lookup_function(sv name)
{
	for (size_t i = 0; i < bot_functions_size; ++i) {
		if (sv_eq(name, bot_functions_names[i]))
			return bot_functions[i];
	}

	return NULL;
}

void
modules_dump(void)
{
	log_info("MODULE DUMP");
	log_info("  There are %zu modules currently loaded.", bot_modules_size);
	for (size_t i = 0; i < bot_modules_size; ++i) {
		log_info("   - %s", bot_module_names[i]);
	}

	log_info("  %zu / %zu functions are currently registered",
			 bot_functions_size, ARRAY_SIZE(bot_functions));
}
