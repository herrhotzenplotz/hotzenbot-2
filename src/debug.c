/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <hotzenbot/arena.h>
#include <hotzenbot/channel.h>
#include <hotzenbot/channel_state.h>
#include <hotzenbot/db.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/modules.h>

static char  *database_file        = NULL;
static int    should_init_database = 0;
static arena  memory               = {0};
static char  *debug_nick           = "debuguser";
static char  *account              = "debug";

static void
usage(void)
{
	fprintf(
		stderr,
		"usage: debug -d <database> "
		"[-i] "
		"[-n <nick>] "
		"[-l <libmodule.so]\n");
	exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
	int ch   = 0;
	int quit = 0;

	const struct option options[] = {
		{ .name    = "database",
		  .has_arg = required_argument,
		  .flag    = NULL,
		  .val     = 'd' },
		{ .name    = "nick",
		  .has_arg = required_argument,
		  .flag    = NULL,
		  .val     = 'n',
		},
		{ .name    = "link",
		  .has_arg = required_argument,
		  .flag    = NULL,
		  .val     = 'l'
		},
		{ .name    = "init",
		  .has_arg = no_argument,
		  .flag    = &should_init_database,
		  .val     = 1
		},
		{0},
	};

	srand(time(NULL));
	init_log_files();

	while ((ch = getopt_long(argc, argv, "d:n:l:i", options, NULL)) != -1) {
		switch (ch) {
		case 'd': {
			if (database_file) {
				fprintf(stderr, "error: -d given twice\n");
				exit(EXIT_FAILURE);
			}
			database_file = optarg;
		} break;
		case 'n': {
			debug_nick = optarg;
		} break;
		case 'l': {
			modules_load_module(optarg);
		} break;
		case 'i': {
			should_init_database = 1;
		} break;
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	init_database(database_file, should_init_database);
	arena_init(&memory, 64 * 1024);

	/* Main repl */
	{
		channel chan = CHAN(SV("libera"), SV("#debug"));
		struct tm tm = {0};
		time_t tn = time(NULL);
		localtime_r(&tn, &tm);

		const size_t buf_len = 5;
		char *yearbuf = arena_alloc(&memory, buf_len);
		snprintf(yearbuf, buf_len, "%0d", tm.tm_year + 1900);

		irc_user_whois_registered(chan.network, SV(debug_nick), SV(account));

		do {
			eval_ctx  ctx          = {0};
			char      buffer[1024] = {0};
			sv        input        = {0};
			cmd_pipe  pipe         = {0};
			char     *response     = NULL;

			ctx.channel = chan;

			printf("%s> ", debug_nick);
			fflush(stdout);

			fgets(buffer, sizeof(buffer), stdin);
			input = SV(buffer);

			eval_ctx_set_variable(&ctx, "ichannel", chan.name);
			eval_ctx_set_variable(&ctx, "sender", SV(debug_nick));
			eval_ctx_set_variable(&ctx, "year", sv_from_parts(yearbuf, buf_len - 1));
			eval_ctx_set_variable(&ctx, "version", SV("hotzenbot-debugrepl"));
			eval_ctx_set_variable(&ctx, "1", SV(NULL));

			if (parse_pipe(input, '!', &pipe)) {
				fprintf(stderr, "error: not a pipe\n");
				continue;
			}

			response = run_pipe(&pipe, &ctx, &memory);

			if (response)
				puts(response);
			else
				puts("no response");

		} while (!feof(stdin) && !quit);
	}

	return EXIT_SUCCESS;
}
