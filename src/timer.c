/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/db.h>
#include <hotzenbot/sb.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/plumb.h>
#include <hotzenbot/timer.h>

#include <string.h>
#include <pthread.h>
#include <unistd.h>

struct timer_ctx {
	int pipe_fd;
};

static pthread_t timer_thread = {0};

static void *
timer_thread_worker(void *ctx)
{
	arena timer_arena = {0};
	arena_init(&timer_arena, 64 * 1024);

	log_info("Timer thread started");
	int pipe_fd = ((struct timer_ctx *)ctx)->pipe_fd;

	timer *timers = NULL;
	for (;;) {
		arena_reset(&timer_arena);

		/* Can free NULL, man-page */
		free(timers);
		timers = NULL;

		if (db_begin_transaction() < 0) {
			log_error("unable to start database transaction in timer thread");
			sleep(60);
			continue;
		}

		int timers_length = db_read_timers(&timer_arena, &timers);
		if (timers_length < 0) {
			log_error("Unable to read timers from Database");
			db_rollback_transaction();

			sleep(60);
			continue;
		}

		if (timers_length == 0) {
			log_info("No timers to trigger");
			db_rollback_transaction();

			sleep(60);
			continue;
		}

		time_t now = time(NULL);
		time_t next_check_time = now + 60; /* Wait a maximum of 60 seconds afterwards */

		for (int i = 0; i < timers_length; ++i) {
			if (timers[i].trigger_time <= now) {

				/*
				 * Here we correct for a possible shift of the trigger
				 * times. When the bot was offline for a period of
				 * time longer than the interval, we correct for that
				 * here and cut off the times that the timer was
				 * missed. That way we don't spam the channels, where
				 * those timers were supposed to get triggered.
				 */
				long long int off_from_actual_trigger = now - timers[i].trigger_time;
				int           missed                  = off_from_actual_trigger / timers[i].interval;
				time_t        next_trigger_time       = timers[i].trigger_time + (missed + 1) * timers[i].interval;

				if (db_bump_timer(timers[i].timer_id, next_trigger_time) < 0) {
					log_error("Cannot bump trigger time of timer no %d", i);
					continue;
				}

				sb        result_msg = {0};
				expr_list list       = {0};

				if (parse_expr(timers[i].code, &timer_arena, &list) < 0) {
					sb_append_sv(&result_msg, &timer_arena, SV("TIMER PARSE FAILURE: "));
					char *err = eval_get_error();
					sb_append_sv(&result_msg, &timer_arena, SV(err));

					char *msg = sb_to_string(&result_msg, &timer_arena);

					plumb(pipe_fd, timers[i].network, "PRIVMSG "SV_FMT" :%s\r\n",
						  SV_ARGS(timers[i].channel), msg);

					continue;
				}

				eval_ctx ctx = {
					.channel = CHAN(timers[i].network, timers[i].channel)
				};

				if (eval_expr_list(&list, &ctx, &timer_arena, &result_msg) < 0) {
					result_msg = (sb) {0};

					sb_append_sv(&result_msg, &timer_arena, SV("TIMER EVAL FAILURE: "));
					char *err = eval_get_error();
					sb_append_sv(&result_msg, &timer_arena, SV(err));

					char *msg = sb_to_string(&result_msg, &timer_arena);

					plumb(pipe_fd, timers[i].network, "PRIVMSG "SV_FMT" :%s\r\n",
						  SV_ARGS(timers[i].channel), msg);

					continue;
				}

				plumb(pipe_fd, timers[i].network, "PRIVMSG "SV_FMT" :%s\r\n",
					  SV_ARGS(timers[i].channel), sb_to_string(&result_msg, &timer_arena));

				if (next_trigger_time < next_check_time)
					next_check_time = next_trigger_time;
			} else {
				/* Determine minimum, if applicable */
				if (timers[i].trigger_time < next_check_time)
					next_check_time = timers[i].trigger_time;
			}
		}

		if (db_commit_transaction() < 0)
			log_fatal("unable to commit transaction in timer thread");

		time_t absolute_wait_time = next_check_time - time(NULL);
		sleep(absolute_wait_time);
	}

	return NULL;
}

void
start_timer_thread(int pipe_fd)
{
	struct timer_ctx *ctx = calloc(sizeof(struct timer_ctx), 1);
	if (!ctx) {
		log_fatal("Unable to calloc()");
		abort();
	}

	ctx->pipe_fd = pipe_fd;

	if (pthread_create(&timer_thread, NULL, &timer_thread_worker, ctx) < 0) {
		log_fatal("Unable to start timer thread");
		abort();
	}
}
