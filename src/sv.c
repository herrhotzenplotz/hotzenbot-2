/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/sv.h>

#include <string.h>

int
sv_eq(sv s1, sv s2)
{
	if (s1.length != s2.length)
		return 0;

	for (size_t i = 0; i < s1.length; ++i)
		if (s1.data[i] != s2.data[i])
			return 0;

	return 1;
}

int
sv_eq_to_cstr(sv s1, const char *s2)
{
	size_t l2 = strlen(s2);

	if (s1.length != l2)
		return 0;

	for (size_t i = 0; i < l2; ++i)
		if (s1.data[i] != s2[i])
			return 0;

	return 1;
}

sv
sv_trim_front(sv in)
{
	while (in.length > 0) {
		if (*in.data == ' ' || *in.data == '\n' || *in.data == '\t' || *in.data == '\r') {
			in.data   += 1;
			in.length -= 1;
		} else {
			break;
		}
	}

	return in;
}

sv
sv_trim_end(sv in)
{
	for (int i = in.length - 1; i >= 0; --i)
		if (in.data[i] == ' ' || in.data[i] == '\n' || in.data[i] == '\t' || in.data[i] == '\r')
			in.length -= 1;
		else
			break;

	return in;
}

sv
sv_dup_into_arena(arena *arena, sv it)
{
	char *buf = arena_alloc(arena, it.length);
	memcpy(buf, it.data, it.length);
	return sv_from_parts(buf, it.length);
}

int
sv_has_prefix(sv it, const char *pref)
{
	size_t pref_length = strlen(pref);

	if (pref_length > it.length)
		return 0;

	for (size_t i = 0; i < pref_length; ++i)
		if (it.data[i] != pref[i])
			return 0;

	return 1;
}

sv
sv_chop_by_delim(sv *it, const char c)
{
	size_t  i;
	char   *front;

	front = it->data;

	for (i = 0; i < it->length; ++i)
		if (it->data[i] == c)
			break;

	it->length -= i;
	it->data   += i;

	return sv_from_parts(front, i);
}

sv
sv_trim(sv in)
{
	return sv_trim_front(sv_trim_end(in));
}

sv
sv_dup(sv it)
{
	char *buffer = malloc(it.length + 1);
	memcpy(buffer, it.data, it.length);
	buffer[it.length] = '\0';
	return (sv) { .data = buffer, .length = it.length };
}
