/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <inttypes.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sysexits.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <sys/socket.h>
#include <sys/types.h>

static char const *spkdev    = "/dev/speaker";
static char const *peername  = "alpha.genunix.com";
static char const *port      = "42069";
static char const *eventname = NULL;

static int peerfd = -1;
static struct sockaddr_in peeraddr = {0};

static void
usage(void)
{
	fprintf(stderr, "usage: beepd [-r hotzenbot-host] [-p port] [-s speakerdevice] [-e event]\n");
}

static void
setup_connection(void)
{
	struct addrinfo hints = {0}, *res = NULL;
	int error;

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_SEQPACKET;
	hints.ai_protocol = IPPROTO_SCTP;

	if ((error = getaddrinfo(peername, port, &hints, &res)))
		err(EX_OSERR, "%s", gai_strerror(error));

	peerfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (peerfd < 0)
		err(EX_OSERR, "socket");

	memcpy(&peeraddr, res->ai_addr, sizeof(peeraddr));

	freeaddrinfo(res);
	res = NULL;

	/* Allow catching association state changes on the socket */
	struct sctp_event ev = {0};
	ev.se_assoc_id = SCTP_FUTURE_ASSOC;
	ev.se_type = SCTP_ASSOC_CHANGE;
	ev.se_on = 1;

	if (setsockopt(peerfd, IPPROTO_SCTP, SCTP_EVENT, &ev, sizeof(ev)) < 0)
		err(EX_OSERR, "setsockopt");

	listen(peerfd, 1);
}

static void
xsend(char const *const buf, size_t const buflen)
{
	if (sendto(peerfd, buf, buflen, 0, (struct sockaddr *)(&peeraddr),
	           sizeof(peeraddr)) != (ssize_t)buflen)
		err(EX_OSERR, "sendto");
}

static void
subscribe(char const *event)
{
	size_t const ev_len = strlen(event);
	char buf[4 + 64 + 1] = {0};

	if (ev_len + 5 > sizeof(buf))
		errx(EX_DATAERR, "event name too long");

	size_t const buf_len = snprintf(buf, sizeof(buf), "SUB %s", event);
	xsend(buf, buf_len);
}

static int
beep(char const *data, ssize_t data_size)
{
	int fd = -1;
	ssize_t rc = 0;

	fd = open(spkdev, O_WRONLY);
	if (fd < 0)
		return -1;

	rc = write(fd, data, data_size);

	close(fd);

	return
		(rc != data_size)
		? -1
		: 0;
}

static void
assocchange(union sctp_notification const *n)
{
	assert(n->sn_header.sn_type == SCTP_ASSOC_CHANGE);
	uint8_t t = n->sn_assoc_change.sac_state;

	switch (t) {
	case SCTP_COMM_UP:
		warnx("Associated with master");
		break;
	case SCTP_SHUTDOWN_COMP:
		errx(0, "Shutdown from master");
	case SCTP_COMM_LOST:
		errx(EX_UNAVAILABLE, "Lost association with master");
	case SCTP_RESTART:
		warnx("Communication with master restarted");
		break;
	case SCTP_CANT_STR_ASSOC:
		errx(EX_OSERR, "Failed to start communication with master");
	default:
		fprintf(stderr, "Unhandled association state change with master\n");
		abort();
	}
}

static int
handle(void)
{
	for (;;) {
		ssize_t rc;
		int len;
		struct msghdr mh = {0};
		struct iovec iov = {0};
		struct pollfd pfd = { .fd = peerfd, .events = POLLIN|POLLHUP, .revents = 0 };

		if (poll(&pfd, 1, -1) != 1)
			err(EX_OSERR, "poll");

		if (ioctl(peerfd, FIONREAD, &len) < 0)
			err(EX_OSERR, "ioctl");

		iov.iov_base = calloc(1, len);
		iov.iov_len = len;
		mh.msg_iov = &iov;
		mh.msg_iovlen = 1;
		mh.msg_flags = MSG_NOTIFICATION;

		rc = recvmsg(peerfd, &mh, MSG_NOTIFICATION);
		if (rc < 0)
			err(EX_OSERR, "recvmsg");

		if (mh.msg_flags & MSG_NOTIFICATION) {
			assocchange(iov.iov_base);
		} else {
			if (beep(iov.iov_base, rc) < 0)
				err(EX_OSERR, "beep failed");
		}

		free(iov.iov_base);
	}
}

static void
loop(void)
{
	for (;;) {
		setup_connection();
		subscribe(eventname);

		if (handle() < 0)
			return;
	}
}

int
main(int argc, char *argv[])
{
	int ch;

	struct option const longopts[] = {
		{ .name = "speaker",
		  .has_arg = required_argument,
		  .flag = NULL,
		  .val = 's'},
		{ .name = "remote",
		  .has_arg = required_argument,
		  .flag = NULL,
		  .val = 'r' },
		{ .name = "port",
		  .has_arg = required_argument,
		  .flag = NULL,
		  .val = 'p' },
		{ .name = "event",
		  .has_arg = required_argument,
		  .flag = NULL,
		  .val = 'e'},
		{0}
	};

	while ((ch = getopt_long(argc, argv, "s:r:e:p:", longopts, NULL)) != -1) {
		switch (ch) {
		case 's':
			spkdev = optarg;
			break;
		case 'r':
			peername = optarg;
			break;
		case 'e':
			eventname = optarg;
			break;
		case 'p':
			port = optarg;
			break;
		default:
			usage();
			exit(EXIT_FAILURE);
		}
	}

	argc -= optind;
	argv += optind;

	if (access(spkdev, W_OK) < 0)
		err(1, "cannot access %s for writing", spkdev);

	loop();

	return EXIT_SUCCESS;
}
