/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/irc_commands.h>
#include <hotzenbot/logging.h>

#include <stdio.h>
#include <unistd.h>

#define check_socket(sock)                                              \
	do {                                                                \
		if (sock->irc_fd < 0) {                                         \
			log_warn("IRC command skipped because IRC socket is not connected. A reconnect process may be in progress."); \
			return;                                                     \
		}                                                               \
	}                                                                   \
	while (0)

void
irc_write(irc_socket *socket, char *buffer, int len)
{
	if (len >= 512) {
		log_warn("Forcefully trimming too long IRC message to 512 bytes");

		buffer[510] = '\r';
		buffer[511] = '\n';
	}

	irc_socket_write(socket, buffer, len);
}

void
irc_plain_auth(irc_socket *socket, sv nick, sv user)
{
	check_socket(socket);
	char line[512] = {0};
	int len = snprintf(line, sizeof(line),
					   "NICK "SV_FMT"\r\n",
					   SV_ARGS(nick));
	irc_write(socket, line, len);

	len = snprintf(line, sizeof(line), "USER "SV_FMT" * * :"SV_FMT"\r\n", SV_ARGS(user), SV_ARGS(user));
	irc_write(socket, line, len);
}

void
irc_privmsg(irc_socket *socket, sv channel, char *msg)
{
	check_socket(socket);
	char line[512] = {0};

	int len = strlen(msg);

	while (len > 0 && (msg[0] == '/' || msg[0] == '.' || msg[0] == ' ' || msg[0] == '\n' || msg[0] == '\t')) {
		len--;
		msg++;
	}

	len = snprintf(line, sizeof(line), "PRIVMSG "SV_FMT" :%s\r\n", SV_ARGS(channel), msg);
	irc_write(socket, line, len);
}

void
irc_join(irc_socket *socket, char *channel)
{
	check_socket(socket);
	char line[512] = {0};
	int len = snprintf(line, sizeof(line), "JOIN %s\r\n", channel);
	irc_write(socket, line, len);
}

void
irc_part(irc_socket *socket, char *channel)
{
	check_socket(socket);
	char line[512] = {0};
	int len = snprintf(line, sizeof(line), "PART %s\r\n", channel);
	irc_write(socket, line, len);
}

void
irc_pong(irc_socket *socket, sv host)
{
	check_socket(socket);
	char line[512] = {0};
	int len = snprintf(line, sizeof(line), "PONG :"SV_FMT"\r\n", SV_ARGS(host));
	irc_write(socket, line, len);
}

void
irc_whois(irc_socket *socket, sv nick)
{
	check_socket(socket);
	char line[512] = {0};
	int len = snprintf(line, sizeof(line), "WHOIS :"SV_FMT"\r\n", SV_ARGS(nick));
	irc_write(socket, line, len);
}

void
irc_pass_auth(irc_socket *socket, sv nick, sv pass)
{
	check_socket(socket);
	char line[512] = {0};
	int len = snprintf(line, sizeof(line), "PASS "SV_FMT"\r\n", SV_ARGS(pass));
	irc_write(socket, line, len);

	len = snprintf(line, sizeof(line), "NICK "SV_FMT"\r\n", SV_ARGS(nick));
	irc_write(socket, line, len);
}

void
irc_cap_req(irc_socket *socket, sv caps)
{
	check_socket(socket);
	char line[512] = {0};
	int len = snprintf(line, sizeof(line), "CAP REQ :"SV_FMT"\r\n", SV_ARGS(caps));
	irc_write(socket, line, len);
}
