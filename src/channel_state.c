/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/channel_state.h>
#include <hotzenbot/irc_commands.h>
#include <hotzenbot/logging.h>

#include <assert.h>

struct userinfo {
	sv  network;
	sv  nick;
	sv  account;
	int is_registered;
};

static struct userlist {
	struct userinfo *it;
	struct userlist *next;
} *users = NULL;

struct chaninfo {
	channel channel;
	sv      topic;
	sv      prefix;
	int     lurk_mode;

	/* Linked list of pointers */
    struct userlist *joined_users;
};

static struct chanlist {
	struct chaninfo *it;
	struct chanlist *next;
} *channels = NULL;

static channel
channel_dup(channel c)
{
	return (channel) { .network = c.network, .name = sv_dup(c.name) };
}

static struct chaninfo *
create_channel(channel channel)
{
	struct chaninfo *it = calloc(sizeof(*it), 1);
	struct chanlist *elem = calloc(sizeof(*elem), 1);

	/* Fill in entries */
	elem->it = it;
	it->channel = channel_dup(channel); /* channel name is in
	                                     * temporary message buffer
	                                     * hence dup needed */

	it->prefix = sv_dup(SV("!")); /* standard call prefix */

	/* Push front */
	elem->next = channels;
	channels = elem;

	return it;
}

struct chaninfo *
find_channel(channel channel)
{
	for (struct chanlist *c = channels; c; c = c->next)
		if (channel_eq(c->it->channel, channel))
			return c->it;

	return create_channel(channel);
}

struct userinfo *
create_user(sv nick, sv network)
{
	struct userlist *elem = calloc(sizeof(*elem), 1);
	struct userinfo *it = calloc(sizeof(*it), 1);

	/* fill in */
	elem->it = it;

	it->nick = sv_dup(nick);    /* from the temporary message buffer,
	                             * dup needed */
	it->network = network;      /* network is in mmap-ed config file,
	                             * thus no dup needed */

	/* push */
	elem->next = users;
	users = elem;

	return it;
}

struct userinfo *
userlist_find(struct userlist *list, sv nick, sv network)
{
	if (!list)
		return NULL;

	for (struct userlist *u = list; u; u = u->next)
		if (sv_eq(u->it->nick, nick) && sv_eq(u->it->network, network))
			return u->it;

	return NULL;
}

struct userinfo *
find_user(sv nick, sv network)
{
	struct userinfo *res;

	if ((res = userlist_find(users, nick, network)))
		return res;
	else
		return create_user(nick, network);
}

static void
cleanup_user(struct userinfo *info)
{
	/* First check if the users is still joined to any of the channels */
	for (struct chanlist *c = channels; c; c = c->next) {
		if (sv_eq(c->it->channel.network, info->network)) {
			if (userlist_find(c->it->joined_users, info->nick, info->network))
				return;
		}
	}

	/* if we haven't returned it means that none of the channels
	 * mentions the user in question. Delete it from the users list
	 * and cleanup all allocated memory. */
	for (struct userlist *u = users; u && u->next; u = u->next) {
		if (u->next->it == info) {
			struct userlist *old = u->next;
			u->next = old->next;

			/* free internal data */
			free(old->it->nick.data);
			memset(old->it, 0, sizeof(*old->it));
			free(old->it);
			memset(old, 0, sizeof(*old));
			free(old);
			old = NULL;

			return;
		}
	}

	log_error("User "SV_FMT" not found in global users list. Leak likely.",
	          SV_ARGS(info->nick));
}

void
channel_user_joined(channel channel, sv nick)
{
	struct chaninfo *c = find_channel(channel);
	struct userinfo *u = find_user(nick, channel.network);

	/* new head */
	struct userlist *hd = calloc(sizeof(*hd), 1);
	hd->it = u;

	/* push */
	hd->next = c->joined_users;
	c->joined_users = hd;
}

int
user_is_joined_to_channel(channel channel, sv nick)
{
	struct chaninfo *c = find_channel(channel);

	return !!userlist_find(c->joined_users, nick, channel.network);
}

void
channel_user_parted(channel channel, sv nick)
{
	struct chaninfo *c = find_channel(channel);

	/* needed so that the next for loop works correctly */
	if (!c->joined_users) {
		log_warn("User "SV_FMT" parted from channel "
		         CHAN_FMT" but its user list is empty.",
		         SV_ARGS(nick),
		         CHAN_ARGS(channel));
		return;
	}

	/* remove it */
	for (struct userlist *u = c->joined_users; (u && u->next); u = u->next) {
		if (sv_eq(u->next->it->nick, nick)) {
			struct userlist *node = u->next;
			u->next = node->next;

			cleanup_user(node->it);
			free(node);

			return;
		}
	}

	/* did not find it */
	log_warn("User "SV_FMT" parted from channel "
	         CHAN_FMT" but channel state did not list it",
	         SV_ARGS(nick),
	         CHAN_ARGS(channel));
}

void
irc_user_whois_unregistered(sv network, sv nick)
{
	struct userinfo *info = find_user(nick, network);
	info->is_registered = 0;
}

void
irc_user_whois_registered(sv network, sv nick, sv account)
{
	struct userinfo *info = find_user(nick, network);
	info->is_registered = 1;
	info->account = sv_dup(account);
}

void
channel_set_topic(channel channel, sv topic)
{
	struct chaninfo *info = find_channel(channel);

	free(info->topic.data); /* null or old topic */
	info->topic = sv_dup(topic);
}

lookup_result
irc_user_get_account(sv network, sv nick, sv *out)
{
	struct userinfo *info = userlist_find(users, nick, network) ;
	if (!info)
		return LOOKUP_FAILED;

	if (!info->is_registered)
		return LOOKUP_NOT_REGISTERED;

	*out = info->account;
	return LOOKUP_OK;
}

int
channel_should_lurk(channel channel)
{
	struct chaninfo *info = find_channel(channel);
	return info->lurk_mode;
}

sv
channel_get_prefix(channel channel)
{
	struct chaninfo *info = find_channel(channel);
	return info->prefix;
}

int
channel_get_topic(channel channel, sv *out)
{
	struct chaninfo *info = find_channel(channel);

	if (!info->topic.length)
		return -1;

	*out = info->topic;
	return 0;
}

void
channel_set_lurk_mode(channel channel, int enable)
{
	struct chaninfo *info = find_channel(channel);
	info->lurk_mode = enable;
}

void
channel_set_prefix(channel channel, sv prefix)
{
	struct chaninfo *info = find_channel(channel);
	free(info->prefix.data);
	info->prefix = sv_dup(prefix);
}

void
irc_rejoin_channels_after_reconnect(irc_socket *sock)
{
	sv network = sock->config->network;

	if (!channels)
		return;

	for (struct chanlist *c = channels; c; c = c->next) {
		if (sv_eq(network, c->it->channel.network))
			irc_join(sock, c->it->channel.name.data);
	}
}

void
channel_state_dump(void)
{
	log_info("Not yet implemented");
}
