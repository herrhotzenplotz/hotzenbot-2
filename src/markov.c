/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/db.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/markov.h>

#include <ctype.h>
#include <stdio.h>

sv
markov_chop_next_word(sv *sentence)
{
	size_t i;
	sv     it;

	*sentence = sv_trim_front(*sentence);

	for (i = 0; i < sentence->length; ++i)
		if (!(isalpha(sentence->data[i]) || sentence->data[i] == '\''))
			break;

	it.data   = sentence->data;
	it.length = i;

	if (sentence->length == i) {
		sentence->length -= i;
		sentence->data   += i;

		return it;
	}

	sentence->length -= i + 1;
	sentence->data   += i + 1;

	/* Trim off all the noise */
	while (sentence->length > 0 && !isalpha(sentence->data[0])) {
		sentence->length -= 1;
		sentence->data   += 1;
	}

	return it;
}

void
markov_add_sentence(sv sentence)
{
	if (db_add_markov_sentence(sentence) < 0)
		log_error("Unable to add sentence to markov model");
}

int
markov_generate_sentence_seeded(arena *arena, sv seed, sb *out)
{
	return db_generate_markov_sentence_seeded(arena, seed, out);
}

int
markov_generate_sentence(arena *arena, sb *out)
{
	return db_generate_markov_sentence(arena, out);
}

#define min(x, y) ((x) < (y) ? (x) : (y))

int
markov_examine(arena *arena, sv seed, sb *out)
{
	db_examine_result *results;
	int                result_size;

	result_size = db_markov_examine(arena, seed, &results);
	if (result_size < 0)
		return -1;

	for (int i = 0; i < result_size; ++i) {
		db_examine_result *it = results + i;
		char tmp[16] = {0};

		sb_append_sv(out, arena, SV(" "));
		sb_append_sv(out, arena, it->word2);
		sb_append_sv(out, arena, SV(": "));

		int len    = snprintf(tmp, sizeof(tmp), "%d", it->frequency);
		sv  tmp_sv = sv_from_parts(tmp, min(len, (int)sizeof(tmp)));

		sb_append_sv(out, arena, sv_dup_into_arena(arena, tmp_sv));
	}

	return 0;
}
