/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/helper.h>
#include <hotzenbot/pipe.h>

#include <ctype.h>

static int
parse_call(sv *in, call *out)
{
	in->data   += 1;
	in->length -= 1;

	if (in->length == 0)
		return -1;

	size_t i;
	for (i = 0; i < in->length; ++i)
		if (isspace(in->data[i]) || in->data[i] == '|')
			break;

	out->command_name = sv_from_parts(in->data, i);

	in->data   += i;
	in->length -= i;

	*in = sv_trim_front(*in);

	if (in->length == 0 || in->data[0] == '|')
		return 0;

	for (i = 0; i < in->length; ++i)
		if (in->data[i] == '|')
			break;

	sv arg = { .data = in->data, .length = i };
	out->command_arg = sv_trim_end(arg);

	in->data   += i;
	in->length -= i;

	return 0;
}

int
parse_pipe(sv in, char command_prefix, cmd_pipe *out)
{
	in = sv_trim_front(in);

	if (in.length == 0)
		return -1;

	if (in.data[0] != command_prefix)
		return -1;

	while (in.data[0] == command_prefix && out->calls_size < ARRAY_SIZE(out->calls)) {
		call it = {0};

		if (parse_call(&in, &it) < 0)
			return -1;

		out->calls[out->calls_size++] = it;

		in = sv_trim_front(in);

		if (in.length > 0) {
			/* Skip the pipe | */
			in.data   += 1;
			in.length -= 1;
			in = sv_trim_front(in);
		} else {
			return 0;
		}
	}

	return -1;
}
