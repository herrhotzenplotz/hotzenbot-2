/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/config.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>

static void
err(int code, const char *fmt, ...)
{
	va_list vp;
	va_start(vp, fmt);

	vfprintf(stderr, fmt, vp);

	fprintf(stderr, ": %s\n", strerror(errno));

	va_end(vp);
	exit(code);
}

static void
parse_error(char *fmt, ...)
{
	va_list vp;
	va_start(vp, fmt);

	fputs("config parse error: ", stderr);
	vfprintf(stderr, fmt, vp);

	fputc('\n', stderr);

	va_end(vp);
	exit(69);
}

static config_entry
read_config_entry(sv *in)
{
	config_entry it = {0};

	*in = sv_trim_front(*in);
	if (in->length == 0)
		parse_error("(bug) trailing whitespace");

	if (in->data[0] != '[')
		parse_error("unexpected %c to begin a network config section", in->data[0]);

	in->data   += 1;
	in->length -= 1;

	it.network = sv_chop_by_delim(in, ']');

	in->data   += 1;
	in->length -= 1;

	while (in->length > 0) {
		*in = sv_trim_front(*in);

		if (in->length == 0)
			break;

		if (in->data[0] == '[')
			break;

		sv field_name    = {0};
		sv field_content = {0};

		field_name = sv_chop_by_delim(in, '=');
		field_name = sv_trim_end(field_name);

		if (in->length == 0)
			parse_error("unexpected end of input in declaration of field "SV_FMT, SV_ARGS(field_name));

		in->length -= 1;
		in->data   += 1;

		field_content = sv_chop_by_delim(in, '\n');
		field_content = sv_trim(field_content);

		if (field_content.length == 0)
			parse_error("unallowed empty content of field "SV_FMT, SV_ARGS(field_name));

		if (sv_eq_to_cstr(field_name, "user"))
			it.user = field_content;
		else if (sv_eq_to_cstr(field_name, "nick"))
			it.nick = field_content;
		else if (sv_eq_to_cstr(field_name, "pass"))
			it.pass = field_content;
		else if (sv_eq_to_cstr(field_name, "host"))
			it.host = field_content;
		else if (sv_eq_to_cstr(field_name, "caps"))
			it.caps = field_content;
		else if (sv_eq_to_cstr(field_name, "secure_connection")) {
			if (sv_eq_to_cstr(field_content, "yes")) {
				it.secure_connection = 1;
			} else if (sv_eq_to_cstr(field_content, "no")) {
				it.secure_connection = 0;
			} else {
				parse_error("unrecognized value for field secure_connection. "
							"Expected either 'yes' or 'no'");
			}
		} else if (sv_eq_to_cstr(field_name, "port")) {
			char *endptr;
			it.port = strtol(field_content.data, &endptr, 10);
			if (endptr != (field_content.data + field_content.length))
				parse_error("unexpected non-numeric port");
		} else
			parse_error("unknown config field "SV_FMT, SV_ARGS(field_name));
	}

	return it;
}

void
config_read_from_file(const char *file_path, config *out)
{
	int          fd;
	char        *data;
	struct stat  stat_buf;

	if ((fd = open(file_path, O_RDONLY)) < 0)
		err(1, "Unable to open config file");

	if (fstat(fd, &stat_buf) < 0)
		err(1, "Unable to stat config file");

	data = mmap(NULL, stat_buf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data == MAP_FAILED)
		err(1, "Unable to mmap config file");

	sv config_file_content = sv_from_parts(data, stat_buf.st_size);

	while (config_file_content.length > 0) {
		out->entries[out->entries_size++] = read_config_entry(&config_file_content);

		/* I hate this but okay. This is twitch's fault */
		if (out->entries[out->entries_size - 1].caps.data
			&& sv_has_prefix(out->entries[out->entries_size - 1].network, "twitch"))
			out->entries[out->entries_size - 1].is_twitch = 1;
	}

	/* Note: we don't unmap the config file, as we still need to
	 * access the buffer */
}
