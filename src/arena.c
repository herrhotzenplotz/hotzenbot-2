/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/arena.h>
#include <hotzenbot/logging.h>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

void
arena_init(arena *it, size_t size)
{
	it->arena = malloc(size);

	if (!it->arena) {
		log_fatal("Unable to malloc. Please download more RAM!");
		abort();
	}

	it->next_free = it->arena;
	it->used      = 0;
	it->capacity  = size;
}

void *
arena_alloc_aligned(arena *it, uintptr_t alignment, size_t size)
{
	if ((it->capacity - it->used) < size)
		return NULL;

	if (size == 0)
		return it->next_free;

	char *chunk = it->next_free;

	it->next_free = (char *)((uintptr_t)(chunk + size + (alignment - 1)) & -alignment);
	it->used += it->next_free - chunk;

	return chunk;
}

void *
arena_alloc(arena *it, size_t size)
{
	return arena_alloc_aligned(it, 16, size);
}

void *
arena_calloc(arena *it, size_t number, size_t size)
{
	size_t total_size = number * size;

	void *chunk = arena_alloc(it, total_size);
	memset(chunk, 0, total_size);

	return chunk;
}

void
arena_free(arena *it)
{
	free(it->arena);
	it->arena = NULL;
}

void
arena_reset(arena *it)
{
	it->next_free = it->arena;
	it->used      = 0;
}
