/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/un.h>
#include <sys/socket.h>

#include <hotzenbot/ipcmessages.h>

static int
usage(FILE *f, char *binary_name)
{
	fprintf(f,
			"usage: %s [subcommand]\n"
			"\n"
			"    available subcommands:\n"
			"       join #channel\n"
			"       privmsg #channel message\n"
			"       part #channel\n"
			"       loadmod /path/to/libmodule.so\n"
			"       unloadmod /path/to/libmodule.so\n"
			"       lurk #channel\n"
			"       unlurk #channel\n"
			"       shutdown\n"
			"       status\n", binary_name);
	return EXIT_FAILURE;
}

static char *
shift(int *argc, char ***argv)
{
	assert(*argc > 0);
	char *result = **argv;
	*argv += 1;
	*argc -= 1;
	return result;
}

int
send_command(ipcmessage *msg)
{
	int fd = 0;

	struct sockaddr_un addr = {0};
	memcpy(addr.sun_path, ipc_socket_path, strlen(ipc_socket_path) + 1);
	addr.sun_family = AF_UNIX;

	fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (fd < 0) {
		fprintf(stderr, "Unable to create a UNIX-domain socket\n");
		exit(EXIT_FAILURE);
	}

	if (connect(fd, (struct sockaddr *)(&addr), sizeof(addr)) < 0) {
		fprintf(stderr, "Unable to connect to the hotzenbot daemon. Is it running?\n");
		exit(EXIT_FAILURE);
	}

	char *buffer = (char *)msg;

	long sent_bytes = 0;

	while (sent_bytes < (long)sizeof(ipcmessage))
		sent_bytes += write(fd, buffer + sent_bytes, sizeof(ipcmessage) - sent_bytes);

	return fd;
}

static void
await_ack(int fd)
{
	char buf[3];

	if (read(fd, buf, sizeof(buf)) != sizeof(buf)) {
		fprintf(stderr, "The daemon responded with an error code. Execution may have failed.\n");
		exit(1);
	}

	close(fd);
}

int
main(int argc, char **argv)
{
	char *binary_name = shift(&argc, &argv);

	if (argc == 0) {
		fprintf(stderr, "%s: expected subcommand\n", binary_name);
		return usage(stderr, binary_name);
	}

	char *subcommand_name = shift(&argc, &argv);
	if (strcmp(subcommand_name, "shutdown") == 0) {
		ipcmessage msg = { .type = IPCMSGTYPE_SHUTDOWN };
		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	if (strcmp(subcommand_name, "privmsg") == 0) {
		if (argc != 2) {
			fprintf(stderr, "%s: need channel and message\n", binary_name);
			return usage(stderr, binary_name);
		}

		char *channel = shift(&argc, &argv);
		char *message = shift(&argc, &argv);

		ipcmessage msg = {
			.type = IPCMSGTYPE_PRIVMSG,
			.channel_size = strlen(channel),
			.params_size = strlen(message),
		};

		if (msg.channel_size > 64 || msg.params_size > 64) {
			fprintf(stderr, "%s: Message or channel too long\n", binary_name);
			return EXIT_FAILURE;
		}

		memcpy(msg.channel, channel, msg.channel_size);
		memcpy(msg.params, message, msg.params_size);

		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	if (strcmp(subcommand_name, "join") == 0) {
		if (argc != 1) {
			fprintf(stderr, "%s: need channel\n", binary_name);
			return usage(stderr, binary_name);
		}

		char *channel = shift(&argc, &argv);
		ipcmessage msg = {
			.type = IPCMSGTYPE_JOIN,
			.channel_size = strlen(channel)
		};

		if (msg.channel_size > 64) {
			fprintf(stderr, "%s: channel name too long\n", binary_name);
			return EXIT_FAILURE;
		}

		memcpy(msg.channel, channel, msg.channel_size);

		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	if (strcmp(subcommand_name, "part") == 0) {
		if (argc != 1) {
			fprintf(stderr, "%s: need channel\n", binary_name);
			return usage(stderr, binary_name);
		}

		char *channel = shift(&argc, &argv);
		ipcmessage msg = {
			.type = IPCMSGTYPE_PART,
			.channel_size = strlen(channel)
		};

		if (msg.channel_size > 64) {
			fprintf(stderr, "%s: channel name too long\n", binary_name);
			return EXIT_FAILURE;
		}

		memcpy(msg.channel, channel, msg.channel_size);

		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	if (strcmp(subcommand_name, "lurk") == 0) {
		if (argc != 1) {
			fprintf(stderr, "%s: need channel\n", binary_name);
			return usage(stderr, binary_name);
		}

		char *channel = shift(&argc, &argv);
		ipcmessage msg = {
			.type = IPCMSGTYPE_LURK,
			.channel_size = strlen(channel)
		};

		if (msg.channel_size > 64) {
			fprintf(stderr, "%s: channel name too long\n", binary_name);
			return EXIT_FAILURE;
		}

		memcpy(msg.channel, channel, msg.channel_size);

		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	if (strcmp(subcommand_name, "unlurk") == 0) {
		if (argc != 1) {
			fprintf(stderr, "%s: need channel\n", binary_name);
			return usage(stderr, binary_name);
		}

		char *channel = shift(&argc, &argv);
		ipcmessage msg = {
			.type = IPCMSGTYPE_UNLURK,
			.channel_size = strlen(channel)
		};

		if (msg.channel_size > 64) {
			fprintf(stderr, "%s: channel name too long\n", binary_name);
			return EXIT_FAILURE;
		}

		memcpy(msg.channel, channel, msg.channel_size);

		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	if (strcmp(subcommand_name, "loadmod") == 0) {
		if (argc != 1) {
			fprintf(stderr, "%s: need the path of the module to load\n", binary_name);
			return usage(stderr, binary_name);
		}

		char *path = shift(&argc, &argv);
		ipcmessage msg = {
			.type = IPCMSGTYPE_LOADMOD,
			.params_size = strlen(path)
		};

		if (msg.params_size > 64) {
			fprintf(stderr, "%s: path name too long\n", binary_name);
			return EXIT_FAILURE;
		}

		memcpy(msg.params, path, msg.params_size);

		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	if (strcmp(subcommand_name, "unloadmod") == 0) {
		if (argc != 1) {
			fprintf(stderr, "%s: need the path of the module to unload\n", binary_name);
			return usage(stderr, binary_name);
		}

		char *path = shift(&argc, &argv);
		ipcmessage msg = {
			.type = IPCMSGTYPE_UNLOADMOD,
			.params_size = strlen(path)
		};

		if (msg.params_size > 64) {
			fprintf(stderr, "%s: path name too long\n", binary_name);
			return EXIT_FAILURE;
		}

		memcpy(msg.params, path, msg.params_size);

		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	if (strcmp(subcommand_name, "setprefix") == 0) {
		if (argc != 2) {
			fprintf(stderr, "%s: need channel and prefix\n", binary_name);
			return usage(stderr, binary_name);
		}

		char *channel = shift(&argc, &argv);
		char *prefix = shift(&argc, &argv);

		ipcmessage msg = {
			.type = IPCMSGTYPE_SETPREFIX,
			.channel_size = strlen(channel),
			.params_size = strlen(prefix),
		};

		if (msg.channel_size > 64 || msg.params_size > 1) {
			fprintf(stderr, "%s: Prefix or channel too long\n", binary_name);
			return EXIT_FAILURE;
		}

		memcpy(msg.channel, channel, msg.channel_size);
		memcpy(msg.params, prefix, msg.params_size);

		await_ack(send_command(&msg));
		return EXIT_SUCCESS;
	}

	fprintf(stderr, "%s: unrecognized subcommand\n", binary_name);
	return usage(stderr, binary_name);
}
