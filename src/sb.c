/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/sb.h>

#include <string.h>

void
sb_append_sv(sb *sb, arena *arena, sv sv)
{
	if (!sb->buckets) {
		sb->buckets = arena_alloc(arena, sizeof(struct sb_bucket));
		sb->buckets->val = sv;
		sb->buckets->next = NULL;
		return;
	}

	struct sb_bucket *b;
	for (b = sb->buckets; b->next != NULL; b = b->next);

	b->next = arena_alloc(arena, sizeof(struct sb_bucket));
	b->next->next = NULL;
	b->next->val = sv;
}

void
sb_append_sb(sb *out, sb in)
{
	if (!out->buckets) {
		out->buckets = in.buckets;
		return;
	}

	struct sb_bucket *b;
	for (b = out->buckets; b->next != NULL; b = b->next);

	b->next = in.buckets;
}

void
sb_prepend_sv(sb *sb, arena *arena, sv sv)
{
	struct sb_bucket *old_head = sb->buckets;
	struct sb_bucket *new_head = arena_alloc(arena, sizeof(struct sb_bucket));
	new_head->next = old_head;
	sb->buckets = new_head;
	new_head->val = sv;
}

size_t
sb_length(sb *sb)
{
	size_t len = 0;

	for (struct sb_bucket *b = sb->buckets; b != NULL; b = b->next)
		len += b->val.length;

	return len;
}

char *
sb_to_string(sb *sb, arena *arena)
{
	size_t len = sb_length(sb);

	if (len == 0)
		return NULL;

	char *buffer = arena_alloc(arena, len + 1);
	char *end = buffer;

	for (struct sb_bucket *b = sb->buckets; b != NULL; b = b->next) {
		memcpy(end, b->val.data, b->val.length);
		end += b->val.length;
	}

	buffer[len] = '\0';

	return buffer;
}
