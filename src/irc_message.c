/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include <hotzenbot/irc_message.h>
#include <hotzenbot/logging.h>

#define fail_with(x) (log_error("%s", x), 1)

static int
parse_nick_or_servername(irc_message *out, sv *input)
{
	out->nick = *input;

	for (size_t i = 0; i < input->length; ++i) {
		switch (input->data[i]) {
		case ' ':
		case '!':
		case '@': {
			input->length -= i;
			out->nick.length = i;
			input->data += i;
			return 0;
		}
		}
	}

	return fail_with("Invalid nick or server name");
}

static int
parse_user(irc_message *out, sv *input)
{
	out->user = *input;

	for (size_t i = 0; i < input->length; ++i) {
		switch (input->data[i]) {
		case ' ':
		case '@': {
			input->length -= i;
			out->user.length = i;
			input->data += i;
			return 0;
		}
		}
	}

	return fail_with("invalid user name");
}

static int
parse_host(irc_message *out, sv *input)
{
	out->host = *input;

	for (size_t i = 0; i < input->length; ++i) {
		switch (input->data[i]) {
		case ' ':
		case '@': {
			input->length -= i;
			out->host.length = i;
			input->data += i;
			return 0;
		}
		}
	}

	return fail_with("invalid hostname");
}

static int
parse_message_prefix(irc_message *out, sv *input)
{
	int err;

	input->data += 1;
	input->length -= 1;

	err = parse_nick_or_servername(out, input);
	if (err)
		return err;

	if (input->length <= 0)
		return fail_with("Unexpected end of input");

	if (input->data[0] == '!') {
		input->length -= 1;
		input->data += 1;

		err = parse_user(out, input);
		if (err)
			return err;
	}

	if (input->data[0] == '@') {
		input->length -= 1;
		input->data += 1;

		err = parse_host(out, input);
		if (err)
			return err;
	}

	return 0;
}

static int
parse_message_command(irc_message *out, sv *input)
{
	if (isdigit(input->data[0])) {
		if (input->length < 3)
			return fail_with("less than 3 characters in a numeric IRC command");

		for (int i = 1; i < 3; ++i)
			if (!isdigit(input->data[i]))
				return fail_with("non-digit character in numeric IRC command");

		out->command = *input;
		out->command.length = 3;
		input->data += 3;
		input->length -= 3;
		return 0;
	} else if (isalpha(input->data[0])) {
		for (size_t i = 1; i < input->length; ++i) {
			if (!isalpha(input->data[i])) {
				out->command = *input;
				out->command.length = i;
				input->data += i;
				input->length -= i;
				return 0;
			}
		}

		return fail_with("Unexpected end of input");
	} else {
		return fail_with("Invalid IRC command");
	}
}

static int
parse_message_params(irc_message *out, sv *input)
{
	for (int n = 0; n < IRC_MESSAGE_MAX_PARAMETERS; ++n) {
		if (input->length == 2 && input->data[0] == '\r')
			return 0;

		if (input->length <= 0 || input->data[0] != ' ')
			return fail_with("Missing space to begin an IRC param");

		input->length -= 1;
		input->data += 1;

		if (input->length <= 0)
			return fail_with("Unexpected end of input");

		/*
		 * Trailing
		 */
		if (input->data[0] == ':') {
			for (size_t i = 1; i < input->length; ++i) {
				if (input->data[i] == '\r'
					|| input->data[i] == '\n'
					|| input->data[i] == '\0') {
					out->trailing = *input;
					out->trailing.data += 1;
					out->trailing.length = i - 1;

					input->data += i + 1;
					input->length -= i + 1;

					out->parameters_size = (size_t)(n);

					return 0;
				}
			}

			return fail_with("Unexpected end of input");
		}
		/*
		 * Middle
		 */
		else {
			for (size_t i = 0; i < input->length; ++i) {
				if (input->data[i] == '\r'
					|| input->data[i] == '\n'
					|| input->data[i] == '\0'
					|| input->data[i] == ' ') {
					out->parameters[n] = *input;
					out->parameters[n].length = i;

					input->data += i;
					input->length -= i;

					break; /* inner loop */
				}

			}
		}
	}

	return fail_with("Too many params in message");
}

static int
parse_message_tag(irc_message *msg, sv *input)
{
	size_t i = 0;
	sv key = *input;

	for (i = 0; i < input->length; ++i) {
		switch (input->data[i]) {
		case ' ':
		case ';':
		case '=':
			goto end_of_loop;
		}
	}

end_of_loop:
	if (input->length == i)
		return fail_with("Unexpected end of input");

	key.length = i;

	input->data   += i;
	input->length -= i;

	msg->tags_keys[msg->tags_size] = key;

	if (input->data[0] == '=') {
		input->data   += 1;
		input->length -= 1;

		// Parse the value corresponding to the key
		for (i = 0; i < input->length; ++i) {
			if (isspace(input->data[i]) || input->data[i] == ';') {
				msg->tags_values[msg->tags_size] = *input;
				msg->tags_values[msg->tags_size].length = i;

				msg->tags_size += 1;

				input->data   += i;
				input->length -= i;

				return 0;
			}
		}

		return fail_with("Unexpected end of input in message tag");
	} else {
		msg->tags_size += 1;
		return 0;
	}
}

static int
parse_message_tags(irc_message *msg, sv *input)
{
	int err;

	if (input->data[0] != '@')
		return fail_with("Missing `@` to start tags.");

	input->data   += 1;
	input->length -= 1;

	do {
		if (input->data[0] == ';') {
			input->data   += 1;
			input->length -= 1;
		}

		err = parse_message_tag(msg, input);
		if (err)
			return err;
	} while (input->data[0] == ';');

	return 0;
}

int
irc_message_parse(irc_message *msg, sv raw)
{
	int err;

	if (!(raw.length && raw.data))
		return fail_with("Empty IRC message");

	msg->raw = raw;

	sv input = raw;

	if (input.data[0] == '@') {
		err = parse_message_tags(msg, &input);
		if (err)
			return err;

		if (input.data[0] != ' ')
			return fail_with("Missing space after message tags");

		input.data   += 1;
		input.length -= 1;
	}

	if (input.data[0] == ':') {
		err = parse_message_prefix(msg, &input);
		if (err)
			return err;

		if (input.data[0] != ' ')
			return fail_with("Invalid IRC message prefix");

		input.data += 1;
		input.length -= 1;
	}

	if (input.length <= 0)
		return fail_with("Unexpected end of input");

	err = parse_message_command(msg, &input);
	if (err)
		return err;

	err = parse_message_params(msg, &input);
	if (err)
		return err;

	return 0;
}

sv
irc_message_tags_get_value(irc_message *msg, const char *_tag_name)
{
	sv tag_name = SV((char *)_tag_name);

	for (size_t i = 0; i < msg->tags_size; ++i)
		if (sv_eq(tag_name, msg->tags_keys[i]))
			return msg->tags_values[i];

	return (sv) {0};
}
