/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/channel_state.h>
#include <hotzenbot/ipcsocket.h>
#include <hotzenbot/ipcmessages.h>
#include <hotzenbot/irc_commands.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/modules.h>
#include <hotzenbot/plumb.h>

#include <sys/types.h>
#include <sys/un.h>
#include <sys/socket.h>

#include <string.h>
#include <pthread.h>
#include <unistd.h>

static pthread_t control_thread;

void *
ipc_thread_worker(void *params)
{
	/* because clang is a pos */
	int irc_pipe = (int)((long)params);

	if (access(ipc_socket_path, F_OK) == 0) {
		if (unlink(ipc_socket_path) < 0) {
			log_error("Cannot unlink stray socket at %s.", ipc_socket_path);
			exit(1);
		}
	}

	int ctl_socket = socket(PF_UNIX, SOCK_STREAM, 0);
	if (ctl_socket < 0) {
		log_fatal("Unable to create control socket");
		abort();
	}

	struct sockaddr_un addr = {0};
	memcpy(addr.sun_path, ipc_socket_path, strlen(ipc_socket_path) + 1);
	addr.sun_family = AF_UNIX;

	if (bind(ctl_socket, (struct sockaddr *)(&addr), sizeof(addr)) < 0) {
		log_fatal("Unable to bind control socket");
		abort();
	}

	if (listen(ctl_socket, 1) < 0) {
		log_fatal("Unable to listen on control socket");
		abort();
	}

	char ack_reply[] = "ACK";

accept_again:
	for (;;) {
		struct sockaddr client_addr = {0};
		socklen_t       client_addr_len = 0;

		int client_fd = accept(ctl_socket, &client_addr, &client_addr_len);
		if (client_fd < 0) {
			log_error("accept returned invalid file descriptor");
			continue;
		}

		ipcmessage message = {0};
		size_t received_bytes = 0;

		while (received_bytes < sizeof(message)) {
			int n = read(client_fd, &message + received_bytes, sizeof(message) - received_bytes);
			if (n < 0) {
				log_error("unexpected read failure from client control socket");
				close(client_fd);
				goto accept_again;
			}

			received_bytes += n;
		}

		if (message.type == IPCMSGTYPE_SHUTDOWN) {
			/*
			 * Start shutdown before acknowledging the shutdown
			 */
			write(client_fd, ack_reply, sizeof(ack_reply));
			close(client_fd);

			close(ctl_socket);

			exit(0);
		}


		// TODO: Remove code duplication
		if (message.type == IPCMSGTYPE_PRIVMSG) {
			channel chan;

			if (channel_from_specifier(sv_from_parts(message.channel, message.channel_size), &chan) < 0) {
				log_error("unable to parse channel specifier");
				close(client_fd);
				goto accept_again;
			}

			plumb(irc_pipe, chan.network, "PRIVMSG "SV_FMT" :%.*s\r\n",
				  SV_ARGS(chan.name), message.params_size, message.params);
		} else if (message.type == IPCMSGTYPE_JOIN) {
			channel chan;

			if (channel_from_specifier(sv_from_parts(message.channel, message.channel_size), &chan) < 0) {
				log_error("unable to parse channel specifier");
				close(client_fd);
				goto accept_again;
			}

			plumb(irc_pipe, chan.network, "JOIN "SV_FMT"\r\n", SV_ARGS(chan.name));
		} else  if (message.type == IPCMSGTYPE_PART) {
			channel chan;

			if (channel_from_specifier(sv_from_parts(message.channel, message.channel_size), &chan) < 0) {
				log_error("unable to parse channel specifier");
				close(client_fd);
				goto accept_again;
			}

			plumb(irc_pipe, chan.network, "PART "SV_FMT"\r\n", SV_ARGS(chan.name));;
		} else if (message.type == IPCMSGTYPE_UNLOADMOD) {
			modules_unload_module(message.params);
		} else if (message.type == IPCMSGTYPE_LOADMOD) {
			modules_load_module(message.params);
		} else if (message.type == IPCMSGTYPE_LURK) {
			channel chan;

			if (channel_from_specifier(sv_from_parts(message.channel, message.channel_size), &chan) < 0) {
				log_error("unable to parse channel specifier");
				close(client_fd);
				goto accept_again;
			}

			channel_set_lurk_mode(chan, 1);
		} else if (message.type == IPCMSGTYPE_UNLURK) {
			channel chan;

			if (channel_from_specifier(sv_from_parts(message.channel, message.channel_size), &chan) < 0) {
				log_error("unable to parse channel specifier");
				close(client_fd);
				goto accept_again;
			}

			channel_set_lurk_mode(chan, 0);
		} else if (message.type == IPCMSGTYPE_SETPREFIX) {
			channel chan;

			if (channel_from_specifier(sv_from_parts(message.channel, message.channel_size), &chan) < 0) {
				log_error("unable to parse channel specifier");
				close(client_fd);
				goto accept_again;
			}

			channel_set_prefix(chan, sv_from_parts(message.params, message.params_size));
		}

		write(client_fd, ack_reply, sizeof(ack_reply));
		close(client_fd);
	}
}

void
start_ipc_thread(int fd)
{
	/* SHUT UP CLANG YOU MISERABLE ... */
	if (pthread_create(&control_thread, NULL, ipc_thread_worker, (void *)(long)fd) < 0) {
		log_fatal("Unable to start control thread");
		abort();
	}
}
