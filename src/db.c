/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/channel.h>
#include <hotzenbot/db.h>
#include <hotzenbot/sb.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/markov.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <pthread.h>
#include <time.h>

sqlite3         *db      = NULL;
pthread_mutex_t  db_lock = {0};

const char *sql_prep_commands[] = {
	"CREATE TABLE IF NOT EXISTS Command (id INTEGER PRIMARY KEY,"
	"                                    code TEXT NOT NULL,"
	"                                    times INT NOT NULL DEFAULT 0);",
	"CREATE TABLE IF NOT EXISTS CommandName (name TEXT NOT NULL,"
	"                                        commandId INTEGER NOT NULL REFERENCES Command(id) ON DELETE CASCADE,"
	"                                        UNIQUE(name) ON CONFLICT REPLACE);",
	"CREATE TABLE IF NOT EXISTS Groups (gid INTEGER PRIMARY KEY,"
	"                                   name TEXT NOT NULL);",
	"CREATE TABLE IF NOT EXISTS IrcUserGroup (account TEXT NOT NULL, "
	"                                         network TEXT NOT NULL DEFAULT 'libera',"
	"                                         gid INTEGER NOT NULL REFERENCES Groups(gid) ON DELETE CASCADE);",
	"CREATE TABLE IF NOT EXISTS Quotes (network TEXT NOT NULL, channel TEXT NOT NULL, date TEXT NOT NULL, message TEXT NOT NULL);",
	"CREATE TABLE IF NOT EXISTS PeriodicTimers (id INTEGER PRIMARY KEY, code TEXT NOT NULL, network TEXT NOT NULL, "
	"                                           channel TEXT NOT NULL, nextTriggerTime INTEGER NOT NULL, "
	"                                           interval INTEGER NOT NULL);",
	"CREATE TABLE IF NOT EXISTS Reminders      (id INTEGER PRIMARY KEY, network TEXT NOT NULL, channel TEXT NOT NULL, "
	"                                           text TEXT NOT NULL, triggerTime INTEGER NOT NULL);",
	"CREATE TABLE IF NOT EXISTS Hooks          (id INTEGER PRIMARY KEY, network TEXT NOT NULL, channel TEXT NOT NULL, "
	"                                           pattern TEXT NOT NULL, code TEXT NOT NULL, times INTEGER NOT NULL DEFAULT 0);",
	"CREATE TABLE IF NOT EXISTS Variables      (network TEXT NOT NULL, channel TEXT NOT NULL, variableName TEXT NOT NULL,"
	"                                           variableContent TEXT NOT NULL, UNIQUE(network, channel, variableName) ON CONFLICT REPLACE);",
	"CREATE TABLE IF NOT EXISTS Markov         (eventType INTEGER NOT NULL DEFAULT 0, word1 TEXT NOT NULL DEFAULT '-', "
	"                                           word2 TEXT NOT NULL DEFAULT '-', frequency INTEGER NOT NULL DEFAULT 0, UNIQUE(eventType, word1, word2) ON CONFLICT REPLACE);",
	"CREATE INDEX IF NOT EXISTS MarkovIndex ON Markov(frequency, word1);",
	"CREATE TABLE IF NOT EXISTS Stalk (id          INTEGER PRIMARY KEY,"
	"                                  network     TEXT NOT NULL,"
	"                                  channel     TEXT NOT NULL,"
	"                                  requestUser TEXT NOT NULL,"
	"                                  targetUser  TEXT NOT NULL,"
	"                                  message     TEXT DEFAULT '',"
	"                                  dateTime    INTEGER NOT NULL DEFAULT 0);",
};
const size_t sql_prep_commands_size = sizeof(sql_prep_commands) / sizeof(sql_prep_commands[0]);

int
db_begin_transaction(void)
{
	int error;
	if ((error = pthread_mutex_lock(&db_lock))) {
		log_error("Unable to lock database for transaction: %d", error);
		return -1;
	}

	if (sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
		log_error("Unable to start transaction");
		pthread_mutex_unlock(&db_lock);
		return -1;
	}

	return 0;
}

int
db_commit_transaction(void)
{
	int return_code = 0, error;

	if (sqlite3_exec(db, "COMMIT TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
		log_fatal("Unable to commit database transaction: %s", sqlite3_errmsg(db));
		return_code = -1;
	}

	if ((error = pthread_mutex_unlock(&db_lock))) {
		log_fatal("Unable to unlock database: %d", error);
		return_code = -1;
	}

	return return_code;
}

int
db_rollback_transaction(void)
{
	int return_code = 0, error;

	if (sqlite3_exec(db, "ROLLBACK TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
		log_fatal("Unable to rollback database transaction: %s", sqlite3_errmsg(db));
		return_code = -1;
	}

	if ((error = pthread_mutex_unlock(&db_lock))) {
		log_fatal("Unable to unlock database: %d", error);
		return_code = -1;
	}

	return return_code;
}

void
init_database(char *filepath, int perform_initial_setup)
{
	if ( sqlite3_open(filepath, &db) != SQLITE_OK ) {
		log_fatal("Cannot open database");
		abort();
	}

	if (pthread_mutex_init(&db_lock, NULL) < 0) {
		log_fatal("Cannot initialize database mutex lock");
		abort();
	}

	if (db_begin_transaction() < 0)
		abort();

	char *errormessage;
	for (size_t i = 0; i < sql_prep_commands_size; ++i) {
		if ( sqlite3_exec(db, sql_prep_commands[i], NULL, NULL, &errormessage) != SQLITE_OK ) {
			log_error("Running SQL Statement failed: %s", errormessage);
			abort();
		}
	}

	if (db_commit_transaction() < 0)
		abort();

	if (perform_initial_setup) {
		puts("=== INITIAL SETUP -- PLEASE READ CAREFULLY ===");
		puts("I will add the following code as the addcmd command: %maintainer(%addcmd(%1)).");
		puts("==> Hit any key to continue.");

		getchar();

		puts("Adding command def ...");
		if (sqlite3_exec(db, "INSERT INTO Command (code, id) VALUES ('%maintainer(%addcmd(%1))', 1);", NULL, NULL, &errormessage) != SQLITE_OK) {
			log_error("Cannot add command definition: %s", errormessage);
			exit(1);
		}

		puts("Adding command name ...");
		if (sqlite3_exec(db, "INSERT INTO CommandName (name, commandId) VALUES ('addcmd', 1);", NULL, NULL, &errormessage) != SQLITE_OK) {
			log_error("Cannot add command name: %s", errormessage);
			exit(1);
		}

		puts("The command has been added. You will have to configure a maintainer later.");
		puts("You can use the maintainerctl script in the tools directory of the distribution.");
		puts("I will add a group called 'maintainer' to the database now.");
		puts("==> Hit any key to continue.");

		getchar();

		puts("Adding maintainer group ...");
		if (sqlite3_exec(db, "INSERT INTO Groups (name) VALUES ('maintainer');", NULL, NULL, &errormessage) != SQLITE_OK) {
			log_error("Cannot add command name: %s", errormessage);
			exit(1);
		}

		puts("All done. You can now add a 'trusted' group to the database by inserting\n"
			 "a name into the Groups table. Remember to add yourself as a maintainer to\n"
			 "start adding commands (See above). Please also refer to hotzenbotctl(8) for\n"
			 "information on how to join the bot to channels.\n"
			 "You may want to write a startup script for the bot that launches the daemon\n"
			 "and runs the hotzenbotctl tool to join channels. The default command invokation\n"
			 "prefix is a dollar-sign '$'.");

		exit(0);
	}
}

void
close_database(void)
{
	/* We'll lock the database such that no other thread can access it
	 * anymore. That way they won't interfere here and cause any
	 * faults. We also won't unlock the mutex. The exit handlers of
	 * the OS should kill all the LWPs and call the exit syscall.
	 */

	int error;
	if ((error = pthread_mutex_lock(&db_lock)))
		log_error("Unable to lock database for closing: %d", error);

	if (sqlite3_close_v2(db) != SQLITE_OK)
		log_error("Failed to close the database.");
}

int
db_get_command_definition(char *command_name, arena *arena, sv *out)
{
	sqlite3_stmt *stmt = NULL;

	char sql[] =
		"SELECT cd.id, cd.code, cd.times FROM Command cd "
		"INNER JOIN CommandName cn ON cn.commandId = cd.id "
		"WHERE cn.name = :1;";

	if ( sqlite3_prepare_v2(db, sql, sizeof(sql), &stmt, NULL) != SQLITE_OK ) {
		log_error("Cannot preprare database statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if ( sqlite3_bind_text(stmt, 1, command_name, -1, NULL) != SQLITE_OK ) {
		log_error("Cannot bind to statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if ( sqlite3_step(stmt) == SQLITE_ROW ) {
		int   command_id = sqlite3_column_int(stmt, 0);
		char *data       = (char *)(sqlite3_column_text(stmt, 1));
		int   times      = sqlite3_column_int(stmt, 2);
		*out             = sv_dup_into_arena(arena, SV(data));

		sqlite3_finalize(stmt);

		char sql_bump[] = "UPDATE Command SET times = times + 1 WHERE id = :1;";
		if ( sqlite3_prepare_v2(db, sql_bump, sizeof(sql_bump), &stmt, NULL) != SQLITE_OK ) {
			log_error("Cannot preprare database statement: %s", sqlite3_errmsg(db));
			return -1;
		}

		if (sqlite3_bind_int(stmt, 1, command_id) != SQLITE_OK) {
			log_error("Cannot preprare bind to statement: %s", sqlite3_errmsg(db));
			goto bail;
		}

		if (sqlite3_step(stmt) != SQLITE_DONE) {
			log_error("Unable to step statement");
			goto bail;
		}

		sqlite3_finalize(stmt);

		return times + 1;
	}

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char trusted_query[] =
	"SELECT gp.gid FROM Groups gp INNER JOIN IrcUserGroup ig ON ig.gid = gp.gid "
	"WHERE (gp.name = 'trusted' OR gp.name = 'authority' OR gp.name = 'maintainer') AND ig.account = :1 AND ig.network = :2;";

int
db_is_user_trusted(sv network, sv account)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, trusted_query, sizeof(trusted_query), &stmt, NULL) != SQLITE_OK) {
		log_warn("Cannot prepare SQLite statement");
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, account.data, (int)account.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, network.data, (int)network.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	int result = 0;
	if (sqlite3_step(stmt) == SQLITE_ROW)
		result = 1;

	sqlite3_finalize(stmt);
	return result;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char ignored_query[] =
	"SELECT gp.gid FROM Groups gp INNER JOIN IrcUserGroup ig ON ig.gid = gp.gid "
	"WHERE (gp.name = 'ignored') AND ig.account = :1 AND ig.network = :2;";

int
db_is_user_ignored(sv network, sv account)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, ignored_query, sizeof(ignored_query), &stmt, NULL) != SQLITE_OK) {
		log_warn("Cannot prepare SQLite statement");
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, account.data, (int)account.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, network.data, (int)network.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	int result = 0;
	if (sqlite3_step(stmt) == SQLITE_ROW)
		result = 1;

	sqlite3_finalize(stmt);
	return result;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char authority_query[] =
	"SELECT gp.gid FROM Groups gp INNER JOIN IrcUserGroup ig ON ig.gid = gp.gid "
	"WHERE (gp.name = 'authority' OR gp.name = 'maintainer') AND ig.account = :1 AND ig.network = :2;";

int
db_is_user_authority(sv network, sv account)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, authority_query, sizeof(authority_query), &stmt, NULL) != SQLITE_OK) {
		log_warn("Cannot prepare SQLite statement");
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, account.data, (int)account.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, network.data, (int)network.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	int result = 0;
	if (sqlite3_step(stmt) == SQLITE_ROW)
		result = 1;

	sqlite3_finalize(stmt);
	return result;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char maintainer_query[] =
	"SELECT gp.gid FROM Groups gp INNER JOIN IrcUserGroup ig ON ig.gid = gp.gid "
	"WHERE gp.name = 'maintainer' AND ig.account = :1 AND ig.network = :2;";

int
db_is_user_maintainer(sv network, sv account)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, maintainer_query, sizeof(maintainer_query), &stmt, NULL) != SQLITE_OK) {
		log_warn("Cannot prepare SQLite statement");
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, account.data, (int)account.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, network.data, (int)network.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	int result = 0;
	if (sqlite3_step(stmt) == SQLITE_ROW)
		result = 1;

	sqlite3_finalize(stmt);
	return result;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char sql_add_cmd_query[] =
	"INSERT INTO Command (code) VALUES (:1);";
const char sql_add_name_query[] =
	"INSERT INTO CommandName (commandId, name) VALUES (:1, :2);";

int
db_add_command(sv name, sv definition)
{
	sqlite3_stmt *stmt;
	if (sqlite3_prepare_v2(db, sql_add_cmd_query, sizeof(sql_add_cmd_query), &stmt, NULL) != SQLITE_OK) {
		log_error("Cannot prepare SQLite statement for adding command");
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, definition.data, (int)definition.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_int64 cmd_id = sqlite3_last_insert_rowid(db);
	sqlite3_finalize(stmt);

	if (sqlite3_prepare_v2(db, sql_add_name_query, sizeof(sql_add_name_query), &stmt, NULL) != SQLITE_OK) {
		log_error("Cannot prepare SQLite statement for adding cmd-name");
		return -1;
	}

	if (sqlite3_bind_int64(stmt, 1, cmd_id) != SQLITE_OK) {
		log_error("Unable to bind to SQLite statment: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, name.data, (int)name.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind to SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step SQLite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_finalize(stmt);
	return 0;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char
sql_delete_commandname[] = "DELETE FROM CommandName WHERE name = :1;";

int
db_del_commandname(sv cmdname)
{
	sqlite3_stmt *stmt = NULL;

	if (sqlite3_prepare_v2(db, sql_delete_commandname,
						   sizeof(sql_delete_commandname),
						   &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare SQL statement");
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, cmdname.data, (int)cmdname.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind command name to delete query: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step delete query", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_finalize(stmt);
	log_warn("Command "SV_FMT" has been deleted.", SV_ARGS(cmdname));

	return 0;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char
sql_assign_group_get_group_id[] = "SELECT gid FROM Groups WHERE name = :1;";
const char
sql_assign_group_insert[] = "INSERT INTO IrcUserGroup (gid, network, account) VALUES (:1, :2, :3);";

int
db_assign_group(sv group, sv network, sv account)
{
	sqlite3_stmt *stmt = NULL;

	if (sqlite3_prepare_v2(db, sql_assign_group_get_group_id,
						   sizeof(sql_assign_group_get_group_id),
						   &stmt, NULL) != SQLITE_OK) {
		log_error("Cannot prepare SQLite statement for reading group id: %s", sqlite3_errmsg(db));
		return DB_FAILURE;
	}

	if (sqlite3_bind_text(stmt, 1, group.data, (int)group.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind group name to SQLite statement for reading group id: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return DB_FAILURE;
	}

	if (sqlite3_step(stmt) != SQLITE_ROW) {
		sqlite3_finalize(stmt);
		return DB_NOSUCHGROUP;
	}

	sqlite3_int64 gid = sqlite3_column_int64(stmt, 0);
	sqlite3_finalize(stmt);

	if (sqlite3_prepare_v2(db, sql_assign_group_insert, sizeof(sql_assign_group_insert), &stmt, NULL) != SQLITE_OK) {
		log_error("Cannot prepare statment for assigning group to user: %s", sqlite3_errmsg(db));
		return DB_FAILURE;
	}

	if (sqlite3_bind_int(stmt, 1, gid) != SQLITE_OK) {
		log_error("Cannot bind gid to statement: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return DB_FAILURE;
	}

	if (sqlite3_bind_text(stmt, 2, network.data, (int)network.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind account to statement: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return DB_FAILURE;
	}

	if (sqlite3_bind_text(stmt, 3, account.data, (int)account.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind account to statement: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return DB_FAILURE;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Stepping insertion statement failed: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return DB_FAILURE;
	}

	sqlite3_finalize(stmt);
	return DB_OK;
}

const char sql_get_quote[] =
	"SELECT message, date FROM Quotes WHERE channel = :1 AND network = :2;";
const char sql_count_quotes[] =
	"SELECT COUNT(*) FROM Quotes WHERE channel = :1 AND network = :2;";

int
db_get_random_quote(channel channel, arena *arena, sv *out)
{
	sqlite3_stmt *stmt = NULL;

	if (sqlite3_prepare_v2(db, sql_count_quotes, sizeof(sql_count_quotes), &stmt, NULL) != SQLITE_OK) {
		log_warn("Cannot prepare sqlite statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, channel.name.data, (int)channel.name.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, channel.network.data, (int)channel.network.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_ROW) {
		log_error("Cannot determine amount of quotes for channel: %s", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_int64 number_of_quotes = sqlite3_column_int64(stmt, 0);
	sqlite3_finalize(stmt);

	if (number_of_quotes == 0) {
		log_warn("No quotes in channel");
		return -1;
	}

	if (sqlite3_prepare_v2(db, sql_get_quote, sizeof(sql_get_quote), &stmt, NULL) != SQLITE_OK) {
		log_warn("Cannot prepare sqlite statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, channel.name.data, (int)channel.name.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, channel.network.data, (int)channel.network.length, NULL) != SQLITE_OK) {
		log_warn("Cannot bind to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_int64 quote_idx = rand() % number_of_quotes;

	for (sqlite3_int64 i = 0; i <= quote_idx; ++i) {
		if (sqlite3_step(stmt) != SQLITE_ROW) {
			log_error("Failed to step to selected quote: %s", sqlite3_errmsg(db));
			goto bail;
		}
	}

	const unsigned char *date = sqlite3_column_text(stmt, 1);
	const unsigned char *text = sqlite3_column_text(stmt, 0);

	if (date == NULL || text == NULL) {
		log_error("Unable to fetch quote data from result row: %s", sqlite3_errmsg(db));
		goto bail;
	}

	sb quote_text = {0};
	sb_append_sv(&quote_text, arena, SV((char *)date));
	sb_append_sv(&quote_text, arena, SV(": "));
	sb_append_sv(&quote_text, arena, SV((char *)text));

	char *result = sb_to_string(&quote_text, arena);
	*out = SV(result);

	sqlite3_finalize(stmt);
	return 0;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char sql_addquote[] = "INSERT INTO Quotes (network, channel, message, date) VALUES (:1, :2, :3, :4);";

int
db_addquote(channel channel, sv quote)
{
	char timestamp_buffer[64] = {0};
	time_t     current_time   = time(NULL);
	struct tm *td             = localtime(&current_time);
	size_t     timestamp_len  = strftime(timestamp_buffer, sizeof(timestamp_buffer), "%Y-%b-%d", td);

	sqlite3_stmt *stmt = NULL;
	if (sqlite3_prepare_v2(db, sql_addquote, sizeof(sql_addquote), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare sqlite statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, channel.network.data, (int)channel.network.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind channel to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, channel.name.data, (int)channel.name.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind channel to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 3, quote.data, (int)quote.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind message to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 4, timestamp_buffer, (int)timestamp_len, NULL) != SQLITE_OK) {
		log_error("Unable to bind timestamp to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_finalize(stmt);
	return 0;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

int
db_read_timers(arena *arena, timer **out)
{
	const char sql_timer_query[] = "SELECT id, code, channel, nextTriggerTime, interval, network FROM PeriodicTimers;";

	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, sql_timer_query, sizeof(sql_timer_query), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare a sqlite statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	int no_of_timers = 0;

	// FIXME: This will fail silently in an error condition
	for (int query_result = sqlite3_step(stmt); query_result == SQLITE_ROW; query_result = sqlite3_step(stmt)) {
		*out = realloc(*out, sizeof(timer) * (no_of_timers + 1));

		int     id                = sqlite3_column_int(stmt, 0);
		char   *code              = (char *)sqlite3_column_text(stmt, 1);
		char   *channel           = (char *)sqlite3_column_text(stmt, 2);
		time_t  next_trigger_time = (time_t)sqlite3_column_int(stmt, 3);
		int     interval          = sqlite3_column_int(stmt, 4);
		char   *network           = (char *)sqlite3_column_text(stmt, 5);

		(*out)[no_of_timers++] = (timer) {
			.code         = sv_dup_into_arena(arena, SV(code)),
			.channel      = sv_dup_into_arena(arena, SV(channel)),
			.interval     = (size_t)interval,
			.trigger_time = next_trigger_time,
			.timer_id     = id,
			.network      = sv_dup_into_arena(arena, SV(network)),
		};
	}

	sqlite3_finalize(stmt);

	return no_of_timers;
}

int
db_bump_timer(int id, time_t next)
{
	const char sql_bump_timer[] = "UPDATE PeriodicTimers SET nextTriggerTime = :1 WHERE id = :2;";

	sqlite3_stmt *stmt = NULL;

	if (sqlite3_prepare_v2(db, sql_bump_timer, sizeof(sql_bump_timer), &stmt, NULL) != SQLITE_OK) {
		log_error("Cannot prepare sqlite statement");
		return -1;
	}

	if (sqlite3_bind_int(stmt, 1, next) != SQLITE_OK) {
		log_error("Cannot bind trigger time to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_int(stmt, 2, id) != SQLITE_OK) {
		log_error("Cannot bind trigger id to sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step sqlite statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_finalize(stmt);
	return 0;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char
sql_add_reminder[] =
	"INSERT INTO Reminders (network, channel, text, triggerTime) VALUES (:1, :2, :3, :4);";

int
db_add_reminder(channel channel, sv text, time_t reminder_trigger_time)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, sql_add_reminder, sizeof(sql_add_reminder), &stmt, NULL) != SQLITE_OK) {
		log_error("Cannot prepare database statement to insert reminder: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, channel.network.data, (int)channel.network.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind text to sqlite3 statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, channel.name.data, (int)channel.name.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind text to sqlite3 statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 3, text.data, (int)text.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind text to sqlite3 statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_int64(stmt, 4, reminder_trigger_time) != SQLITE_OK) {
		log_error("Cannot bind timestamp to sqlite3 statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step sqlite3 statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_finalize(stmt);

	return 0;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

const char sql_get_reminders[] =
	"SELECT id, network, channel, text, triggerTime FROM Reminders;";

size_t
db_get_reminders(arena *arena, reminder **out)
{
	sqlite3_stmt *stmt;
	size_t        no_of_reminders = 0;

	if (sqlite3_prepare_v2(db, sql_get_reminders, sizeof(sql_get_reminders), &stmt, NULL) != SQLITE_OK) {
		log_error("Cannot prepare statement to fetch reminders from database: %s", sqlite3_errmsg(db));
		return 0;
	}

	if (*out) {
		free(*out);
		*out = NULL;
	}

	// FIXME; this will fail silently in case of an error condition
	while (sqlite3_step(stmt) == SQLITE_ROW) {
		*out = realloc(*out, sizeof(reminder) * (no_of_reminders + 1));

		int     id           = sqlite3_column_int(stmt, 0);
		channel channel      = CHAN(sv_dup_into_arena(arena, SV((char *)(sqlite3_column_text(stmt, 1)))),
									sv_dup_into_arena(arena, SV((char *)(sqlite3_column_text(stmt, 2)))));
		sv      text         = sv_dup_into_arena(arena, SV((char *)(sqlite3_column_text(stmt, 3))));
		time_t  trigger_time = sqlite3_column_int64(stmt, 4);

		(*out)[no_of_reminders++] = (reminder) {
			.id           = id,
			.channel      = channel,
			.text         = text,
			.trigger_time = trigger_time,
		};
	}

	sqlite3_finalize(stmt);
	return no_of_reminders;
}

const char sql_delete_reminder[] =
	"DELETE FROM Reminders WHERE id = :1;";

int
db_delete_reminder_by_id(int reminder_id)
{
	sqlite3_stmt *stmt;
	if (sqlite3_prepare_v2(db, sql_delete_reminder, sizeof(sql_delete_reminder), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare a database statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_int(stmt, 1, reminder_id) != SQLITE_OK) {
		log_error("Cannot bind reminder id to delete statement: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return -1;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step delete statement: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return -1;
	}

	sqlite3_finalize(stmt);
	return 0;
}

static const char
sql_find_hook[] = "SELECT id, pattern, code, times FROM Hooks WHERE instr(upper(:1), upper(pattern)) != 0 AND channel = :2 AND network = :3;";

int
db_find_hook(arena *arena, sv network, sv channel, sv message, hook *out)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, sql_find_hook, sizeof(sql_find_hook), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare a database statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, message.data, (int)message.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind message to sqlite3 statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, channel.data, (int)channel.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind channel to sqlite3 statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 3, network.data, (int)network.length, NULL) != SQLITE_OK) {
		log_error("Cannot bind network to sqlite3 statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	// FIXME: this will fail silently in case of an error condition
	if (sqlite3_step(stmt) == SQLITE_ROW) {
		int id      = sqlite3_column_int(stmt, 0);
		sv  pattern = sv_dup_into_arena(arena, SV((char *)(sqlite3_column_text(stmt, 1))));
		sv  code    = sv_dup_into_arena(arena, SV((char *)(sqlite3_column_text(stmt, 2))));
		int times   = sqlite3_column_int(stmt, 3);

		(*out) = (hook) {
			.pattern = pattern,
			.code    = code,
			.channel = channel,
			.times   = times,
			.id      = id,
			.network = network,
		};

		sqlite3_finalize(stmt);
		return 0;
	}

bail:
	sqlite3_finalize(stmt);
	return -1;
}

static const char
sql_bump_times_hook[] = "UPDATE Hooks SET times = times + 1 WHERE id = :1;";

int
db_bump_times_hook(hook *hook)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, sql_bump_times_hook, sizeof(sql_bump_times_hook),
						   &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare SQL statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_int(stmt, 1, hook->id) != SQLITE_OK) {
		log_error("Unable to bind id to bump-hook-times SQL statment: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return -1;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Failed to step bump-hook-times SQL statment: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return -1;
	}

	sqlite3_finalize(stmt);

	return 0;
}

static const char
sql_lookup_variable[] = "SELECT variableContent FROM Variables WHERE channel = :1 AND network = :2 AND variableName = :3;";

int
db_lookup_variable(arena *arena, channel channel, sv variable_name, sv *out)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, sql_lookup_variable, sizeof(sql_lookup_variable),
						   &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare query for variable lookup: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, channel.name.data, (int)channel.name.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind channel name to sqlite statement for variable lookup: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, channel.network.data, (int)channel.network.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind network name to sqlite statement for variable lookup: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 3, variable_name.data, (int)variable_name.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind variable name to query: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) == SQLITE_ROW) {
		char *_content = (char *)sqlite3_column_text(stmt, 0);
		sv    content  = sv_dup_into_arena(arena, SV(_content));

		sqlite3_finalize(stmt);

		*out = content;
		return 0;
	}

	log_error(SV_FMT": no such variable", SV_ARGS(variable_name));

bail:

	sqlite3_finalize(stmt);
	return -1;
}

static const char
sql_set_variable[] = "INSERT INTO Variables (network, channel, variableName, variableContent) VALUES (:1, :2, :3, :4);";

int
db_set_variable(channel channel, sv variable_name, sv variable_content)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, sql_set_variable, sizeof(sql_set_variable), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare query for variable lookup: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, channel.network.data, (int)channel.network.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind network name to sql statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, channel.name.data, (int)channel.name.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind channel name to sql statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 3, variable_name.data, (int)variable_name.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind variable name to sql statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 4, variable_content.data, (int)variable_content.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind variable content to sql statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("unable to insert variable into database (sqlite3_step failed): %s", sqlite3_errmsg(db));
		goto bail;
	}

	sqlite3_finalize(stmt);
	return 0;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

static const char
sql_query_markov_event_frequency[] = "SELECT frequency FROM Markov WHERE eventType = :1 AND word1 = :2 AND word2 = :3;";

static const char
sql_update_markov_event_frequency[] = "INSERT INTO Markov (eventType, word1, word2, frequency) VALUES (:1, :2, :3, :4);";

struct markov_event {
	enum markov_event_type type;
	sv word1;
	sv word2;
};

static int
db_add_markov_event(sqlite3_stmt *frequency_stmt, sqlite3_stmt *update_stmt, struct markov_event *event)
{
	sqlite3_reset(frequency_stmt);
	sqlite3_clear_bindings(frequency_stmt);

	if (sqlite3_bind_int(frequency_stmt, 1, event->type) != SQLITE_OK) {
		log_error("Cannot bind event type to database statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(
			frequency_stmt, 2,
			event->type == ME_BEGIN ? "-" : event->word1.data,
			event->type == ME_BEGIN ? 1 : (int)event->word1.length,
			NULL) != SQLITE_OK) {
		log_error("Cannot bind word 1 to database statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(
			frequency_stmt, 3,
			event->type == ME_END ? "-" : event->word2.data,
			event->type == ME_END ? 1 : (int)event->word2.length,
			NULL) != SQLITE_OK) {
		log_error("Cannot bind word 2 to database statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	int frequency = 0;

	if (sqlite3_step(frequency_stmt) == SQLITE_ROW)
		frequency = sqlite3_column_int(frequency_stmt, 0);


	sqlite3_reset(update_stmt);
	sqlite3_clear_bindings(update_stmt);

	if (sqlite3_bind_int(update_stmt, 1, event->type) != SQLITE_OK) {
		log_error("Cannot bind event type to database statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(
			update_stmt, 2,
			event->type == ME_BEGIN ? "-" : event->word1.data,
			event->type == ME_BEGIN ? 1 : (int)event->word1.length,
			NULL) != SQLITE_OK) {
		log_error("Cannot bind word 1 to database statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(
			update_stmt, 3,
			event->type == ME_END ? "-" : event->word2.data,
			event->type == ME_END ? 1 : (int)event->word2.length,
			NULL) != SQLITE_OK) {
		log_error("Cannot bind word 2 to database statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_int(update_stmt, 4, frequency + 1) != SQLITE_OK) {
		log_error("Cannot bind event frequency to database statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_step(update_stmt) != SQLITE_DONE) {
		log_error("Couldn't step muh statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	return 0;
}

int
db_add_markov_sentence(sv sentence)
{
	struct markov_event  event = {0};
	sqlite3_stmt        *frequency_stmt, *update_stmt;

	if (sqlite3_prepare_v2(db, sql_query_markov_event_frequency,
						   sizeof(sql_query_markov_event_frequency),
						   &frequency_stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare database statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_prepare_v2(db, sql_update_markov_event_frequency,
						   sizeof(sql_update_markov_event_frequency),
						   &update_stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare database statement: %s", sqlite3_errmsg(db));
		sqlite3_finalize(frequency_stmt);
		return -1;
	}

	while (sentence.length > 0) {
		event.word2 = markov_chop_next_word(&sentence);

		if (db_add_markov_event(frequency_stmt, update_stmt, &event) < 0)
			goto bail;

		event.word1 = event.word2;
		event.type  = ME_MIDDLE;
	}

	event.type = ME_END;
	event.word1 = event.word2;
	event.word2 = (sv) {0};
	if (db_add_markov_event(frequency_stmt, update_stmt, &event) < 0)
		goto bail;

	sqlite3_finalize(frequency_stmt);
	sqlite3_finalize(update_stmt);

	return 0;

bail:
	sqlite3_finalize(frequency_stmt);
	sqlite3_finalize(update_stmt);

	return -1;
}

/*
 * With some help from some nice coding guy from the Twitch chat ^^
 */
static const char
sql_next_random_event[] =
	"SELECT "
	"  m.eventType, m.word2 "
	"FROM Markov m "
	"  JOIN ("
	"    SELECT (abs(random()) % max(frequency)) AS frequency FROM Markov WHERE word1 = :1 "
	"  ) s ON s.frequency <= m.frequency "
	"WHERE "
	"  m.word1 = :1 "
	"ORDER BY m.frequency ASC "
	"LIMIT 1";

int
db_generate_markov_sentence_seeded(arena *arena, sv seed, sb *out)
{
	sqlite3_stmt *stmt;
	sv            current_word     = seed;
	sv            space            = SV(" ");
	int           generated_words  = 0;
	int           event_type       = 0;

	if (sqlite3_prepare_v2(db, sql_next_random_event,
						   sizeof(sql_next_random_event),
						   &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare statment: %s", sqlite3_errmsg(db));
		return -1;
	}

	do {
		sb_append_sv(out, arena, current_word);
		sb_append_sv(out, arena, space);

		sqlite3_reset(stmt);
		sqlite3_clear_bindings(stmt);

		if (sqlite3_bind_text(stmt, 1, current_word.data, (int)current_word.length, NULL) != SQLITE_OK) {
			log_error("Cannot bind word to statement: %s", sqlite3_errmsg(db));
			goto bail;
		}

		if (sqlite3_step(stmt) == SQLITE_ROW) {
			event_type                 = sqlite3_column_int(stmt, 0);
			const unsigned char *_word = sqlite3_column_text(stmt, 1);
			current_word               = sv_dup_into_arena(arena, SV((char *)(_word)));
		} else {
			log_fatal("Markov model is inconsistent.");
			break;
		}
	} while (event_type == ME_MIDDLE && ++generated_words < 30);

	sqlite3_finalize(stmt);

	return 0;

bail:
	sqlite3_finalize(stmt);
	return -1;
}

static const char
sql_get_random_start_word[] =
	"SELECT "
	"  m.word2 "
	"FROM Markov m "
	"  JOIN ("
	"    SELECT (abs(random()) % max(frequency)) AS frequency FROM Markov WHERE eventType = 0"
	"  ) s ON s.frequency <= m.frequency "
	"WHERE "
	"  m.eventType = 0 "
	"ORDER BY m.frequency ASC "
	"LIMIT 1";

int
db_generate_markov_sentence(arena *arena, sb *out)
{
	sqlite3_stmt *stmt;
	if (sqlite3_prepare_v2(db, sql_get_random_start_word, sizeof(sql_get_random_start_word), &stmt, NULL) < 0) {
		log_error("Unable to prepare statement for markov start word: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_step(stmt) != SQLITE_ROW) {
		log_error("Didn't get a start word from the database: %s", sqlite3_errmsg(db));
		sqlite3_finalize(stmt);
		return -1;
	}

	const unsigned char *_seed = sqlite3_column_text(stmt, 0);
	sv                   seed  = sv_dup_into_arena(arena, SV((char *)_seed));

	sqlite3_finalize(stmt);

	if (db_generate_markov_sentence_seeded(arena, seed, out) < 0)
		return -1;
	else
		return 0;
}

static const char
sql_markov_examine[] =
	"SELECT word2, frequency FROM MARKOV WHERE word1 = :1 ORDER BY frequency DESC LIMIT 5;";

int
db_markov_examine(arena *arena, sv seed, db_examine_result **results)
{
	sqlite3_stmt      *stmt;
	db_examine_result *buffer;

	if (sqlite3_prepare_v2(db, sql_markov_examine, sizeof(sql_markov_examine), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare database statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, seed.data, seed.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind to database statment: %s", sqlite3_errmsg(db));
		goto bail;
	}

	buffer = arena_alloc(arena, sizeof(db_examine_result) * 5);
	*results = buffer;

	int result_count = 0;
	while (sqlite3_step(stmt) == SQLITE_ROW) {
		char *_word2    = (char *)sqlite3_column_text(stmt, 0);
		sv    word2     = sv_dup_into_arena(arena, SV(_word2));
		int   frequency = sqlite3_column_int(stmt, 1);

		buffer[result_count++] = (db_examine_result) {
			.word2     = word2,
			.frequency = frequency,
		};
	}

	sqlite3_finalize(stmt);
	return result_count;

bail:
	sqlite3_finalize(stmt);

	return -1;
}

static const char
sql_add_stalk[] =
	"INSERT INTO Stalk (network, channel, requestUser, targetUser, message, dateTime)"
	"           VALUES (:1, :2, :3, :4, :5, :6);";
int
db_add_stalk(channel channel, sv src_user, sv tgt_user, sv message)
{
	time_t now = time(NULL);
	sqlite3_stmt *stmt = NULL;

	if (sqlite3_prepare_v2(db, sql_add_stalk, sizeof(sql_add_stalk), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare database statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, channel.network.data, channel.network.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind to database statment: %s", sqlite3_errmsg(db));
		goto bail;
	}
	if (sqlite3_bind_text(stmt, 2, channel.name.data, channel.name.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind to database statment: %s", sqlite3_errmsg(db));
		goto bail;
	}
	if (sqlite3_bind_text(stmt, 3, src_user.data, src_user.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind to database statment: %s", sqlite3_errmsg(db));
		goto bail;
	}
	if (sqlite3_bind_text(stmt, 4, tgt_user.data, tgt_user.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind to database statment: %s", sqlite3_errmsg(db));
		goto bail;
	}
	if (sqlite3_bind_text(stmt, 5, message.data, message.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind to database statment: %s", sqlite3_errmsg(db));
		goto bail;
	}
	if (sqlite3_bind_int(stmt, 6, now) != SQLITE_OK) {
		log_error("Unable to bind to database statment: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	log_info("Inserted stalk request into database");
	sqlite3_finalize(stmt);
	return 0;

bail:
	log_error("Unable to add stalk");
	sqlite3_finalize(stmt);
	return -1;
}

static const char sql_query_stalk[] =
	"SELECT "
	"  ID, channel, "
	"  requestUser, message, "
	"  dateTime "
	"FROM Stalk "
	"WHERE targetUser = :1 AND network = :2;";
int
db_query_stalk(arena *arena, sv network, sv nick, stalk_buf **out)
{
	sqlite3_stmt *stmt;
	int count = 0;
	int rc = SQLITE_OK;

	stalk_buf *buffer = NULL;

	if (sqlite3_prepare_v2(db, sql_query_stalk, sizeof(sql_query_stalk), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare sql statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_text(stmt, 1, nick.data, nick.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind text to sql statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_bind_text(stmt, 2, network.data, network.length, NULL) != SQLITE_OK) {
		log_error("Unable to bind text to sql statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
		buffer = realloc(buffer, (count + 1) * sizeof(*buffer));

		stalk_buf *it = &buffer[count++];
		*it = (stalk_buf) {0};

		const char *channel     = (const char *)sqlite3_column_text(stmt, 1);
		const char *requestUser = (const char *)sqlite3_column_text(stmt, 2);
		const char *message     = (const char *)sqlite3_column_text(stmt, 3);

		it->id = sqlite3_column_int(stmt, 0);
		it->when = (time_t)(sqlite3_column_int64(stmt, 4));

		it->requested_by = sv_dup_into_arena(arena, SV((char *)requestUser));
		it->target_user = nick;
		it->message = sv_dup_into_arena(arena, SV((char *)message));
		it->request_channel.name = sv_dup_into_arena(arena, SV((char *)channel));
		it->request_channel.network = network;
	}

	if (rc != SQLITE_DONE)
		goto bail;

	sqlite3_finalize(stmt);

	*out = buffer;
	return count;

bail:
	log_error("Unable to query stalkers");
	sqlite3_finalize(stmt);
	return -1;
}

static const char sql_delete_stalk[] =
	"DELETE FROM Stalk WHERE ID = :1;";
int
db_delete_stalk(int id)
{
	sqlite3_stmt *stmt;

	if (sqlite3_prepare_v2(db, sql_delete_stalk, sizeof(sql_delete_stalk), &stmt, NULL) != SQLITE_OK) {
		log_error("Unable to prepare sql statement: %s", sqlite3_errmsg(db));
		return -1;
	}

	if (sqlite3_bind_int(stmt, 1, id) != SQLITE_OK) {
		log_error("Unable to bind id to sql statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	if (sqlite3_step(stmt) != SQLITE_DONE) {
		log_error("Unable to step statement: %s", sqlite3_errmsg(db));
		goto bail;
	}

	log_warn("Deleted stalk request %d", id);

	sqlite3_finalize(stmt);
	return 0;

bail:
	log_error("Unable to delete stalk request");
	sqlite3_finalize(stmt);
	return -1;
}
