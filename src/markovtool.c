/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <hotzenbot/db.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/markov.h>
#include <hotzenbot/sv.h>

static char *
shift(int *argc, char ***argv)
{
	assert(*argc > 0);
	char *result = **argv;
	*argv += 1;
	*argc -= 1;
	return result;
}

int
main(int argc, char *argv[])
{
	char *binary_name = shift(&argc, &argv);
	char *database_path;

	if (argc < 1) {
		fprintf(stderr, "%s: Expected the database name\n", binary_name);
		return EXIT_FAILURE;
	}
	database_path = shift(&argc, &argv);

	init_log_files();
	init_database(database_path, 0);

	arena arena = {0};
	arena_init(&arena, 8 * 1024);

	if (argc) {
		sb output = {0};

		char *seed = shift(&argc, &argv);

		db_begin_transaction();
		if (markov_generate_sentence_seeded(&arena, SV(seed), &output) < 0) {
			fprintf(stderr, "%s: error generating sentence\n", binary_name);
			return EXIT_FAILURE;
		}
		db_commit_transaction();

		fprintf(stdout, "%s\n", sb_to_string(&output, &arena));

	} else {
		int i = 0;

		char line[1024] = {0};

		db_begin_transaction();

		int foo = 0;
		while (fgets(line, sizeof(line), stdin)) {

			if ((i = (i + 1) % 10) == 0) {
				fprintf(stderr, "\r%9d sentences processed", (++foo) * 10);
				fflush(stderr);
				db_commit_transaction();
				db_begin_transaction();
			}

			sv it = SV(line);

			markov_add_sentence(it);
		}
	}

	arena_free(&arena);

	return EXIT_SUCCESS;
}
