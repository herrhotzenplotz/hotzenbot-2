/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/eval.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/sb.h>
#include <hotzenbot/modules.h>
#include <hotzenbot/db.h>

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

char __eval_error_string[128];

int
eval_fail_with(char *fmt, ...)
{
	va_list vp;
	va_start(vp, fmt);
	vsnprintf(__eval_error_string, sizeof(__eval_error_string), fmt, vp);
	va_end(vp);
	return -1;
}

char *
eval_get_error(void)
{
	return __eval_error_string;
}

char *
run_pipe(cmd_pipe *pipe, eval_ctx *ctx, arena *arena)
{
	sb result = {0};

	for (size_t i = 0; i < pipe->calls_size; ++i) {
		sv   command_code     = (sv){0};
		char command_name[64] = {0};
		char *arg1            = NULL;

		if (pipe->calls[i].command_name.length > sizeof(command_name))
			return
				"Dear script kiddies, I just detected that you want to "
				"smash my stack. Please try again later.";

		memcpy(command_name,
			   pipe->calls[i].command_name.data,
			   pipe->calls[i].command_name.length);

		int times = 0;
		if ((times = db_get_command_definition(command_name, arena, &command_code)) < 0) {
			log_info("%s: no such command", command_name);
			return "";
		}

		sb_prepend_sv(&result, arena, pipe->calls[i].command_arg);
		arg1 = sb_to_string(&result, arena);

		if (arg1)
			eval_ctx_set_variable(ctx, "1", SV(arg1));

		char times_buf[64] = {0};
		snprintf(times_buf, sizeof(times_buf), "%d", times);
		eval_ctx_set_variable(ctx, "times", SV(times_buf));

		result = (sb) {0};

		expr_list exprs = {0};
		if (parse_expr(command_code, arena, &exprs) < 0)
			return eval_get_error();

		if (eval_expr_list(&exprs, ctx, arena, &result) < 0)
			return eval_get_error();
	}

	return sb_to_string(&result, arena);
}

int
eval_expr_list(expr_list *list, eval_ctx *ctx, arena *arena, sb *out)
{
	for (size_t i = 0; i < list->exprs_size; ++i) {
		if (eval_expr(&list->exprs[i], ctx, arena, out) < 0)
			return -1;
	}

	return 0;
}

int
eval_expr(expr_atom *expr, eval_ctx *ctx, arena *arena, sb *out)
{
	switch (expr->kind)
	{
	case ATOM_KIND_TEXT: {
		sb_append_sv(out, arena, expr->text);
	} break;
	case ATOM_KIND_VARIABLE: {
		sv value = {0};
		if (eval_ctx_lookup_variable(ctx, expr->variable, &value) < 0)
			return eval_fail_with("Variable '%%"SV_FMT"' is undefined",
								  SV_ARGS(expr->variable));

		sb_append_sv(out, arena, value);
	} break;
	case ATOM_KIND_FUNCALL: {
		eval_function f = modules_lookup_function(expr->function_call.function_name);
		if (!f)
			return eval_fail_with("No such function: %%"SV_FMT,
								  SV_ARGS(expr->function_call.function_name));

		if (f(expr->function_call.args, arena, ctx, out) < 0)
			return -1;
	} break;
	}

	return 0;
}

static int
parse_variable_or_function(sv *in, arena *arena, expr_atom *out);

static int
parse_string_literal(sv *in, arena *arena, expr_atom *out)
{
	(void) arena;

	in->data   += 1;
	in->length -= 1;

	size_t i;
	for (i = 0; i < in->length; ++i) {
		if (in->data[i] == '"')
			break;
	}

	if (i == in->length)
		return eval_fail_with("unexpected end of input in string literal");

	out->kind = ATOM_KIND_TEXT;
	out->text = sv_from_parts(in->data, i);

	in->data   += i + 1;
	in->length -= i + 1;

	return 0;
}

static int
parse_functioncall_arglist(sv *in, arena *arena, expr_atom *out)
{
	in->data   += 1;
	in->length -= 1;

	*in = sv_trim_front(*in);

	if (in->length == 0)
		return eval_fail_with("Unexpected end of input");

	out->function_call.args = arena_alloc(arena, sizeof(expr_list));
	memset(out->function_call.args, 0, sizeof(expr_list));

	for (;;) {
		switch (in->data[0]) {
		case ')': {
			in->data += 1;
			in->length -= 1;
			return 0;
		}
		case '"': {
			expr_atom *lit = &out->function_call.args->exprs[out->function_call.args->exprs_size++];

			if (parse_string_literal(in, arena, lit) < 0)
				return -1;

		} break;
		case '%': {
			expr_atom *var_or_funcall = &out->function_call.args->exprs[out->function_call.args->exprs_size++];

			if (parse_variable_or_function(in, arena, var_or_funcall) < 0)
				return -1;

		} break;
		default: {
			return eval_fail_with("unexpected '%c' in argument list to function call", in->data[0]);
		}
		}

		*in = sv_trim_front(*in);

		if (in->length == 0)
			return eval_fail_with("unexpected end of input");

		if (in->data[0] == ')') {
			in->data += 1;
			in->length -= 1;
			return 0;
		} else if (in->data[0] == ',') {
			in->data += 1;
			in->length -= 1;

			*in = sv_trim_front(*in);
			continue;
		} else {
			return eval_fail_with("unexpected '%c' in argument list to function call. Maybe you forgot a comma?", in->data[0]);
		}
	}
}

static int
parse_variable_or_function(sv *in, arena *arena, expr_atom *out)
{
	in->data   += 1;
	in->length -= 1;

	size_t i;
	for (i = 0; i < in->length; ++i) {
		if (!(isalnum(in->data[i]) || in->data[i] == '_'))
			break;
	}

	sv name = sv_from_parts(in->data, i);

	in->data   += i;
	in->length -= i;

	if (in->length > 0 && in->data[0] == '(') {
		out->kind = ATOM_KIND_FUNCALL;
		out->function_call.function_name = name;
		return parse_functioncall_arglist(in, arena, out);
	} else {
		out->kind = ATOM_KIND_VARIABLE;
		out->variable = name;
		return 0;
	}
}

int
parse_expr(sv in, arena *arena, expr_list *out)
{
	for (;;) {
		if (in.length == 0)
			return 0;

		// TODO: Escaping % is not yet implemented
		if (in.data[0] == '%') {
			// var or function
			if (parse_variable_or_function(&in, arena, &out->exprs[out->exprs_size]) < 0)
				return -1;

			out->exprs_size += 1;

		} else {
			size_t i;
			for (i = 0; i < in.length; ++i) {
				if (in.data[i] == '%')
					break;
			}

			out->exprs[out->exprs_size].text.data    = in.data;
			out->exprs[out->exprs_size].text.length  = i;
			out->exprs[out->exprs_size].kind         = ATOM_KIND_TEXT;
			out->exprs_size                         += 1;
			in.data                                 += i;
			in.length                               -= i;
		}
	}
}

void
eval_ctx_set_variable(eval_ctx *ctx,  char *_name, sv value)
{
	if (ctx->variables_size >= 64) {
		log_error("Variable storage exceeded when trying to set variable");
		return;
	}

	sv name = SV(_name);
	for (size_t i = 0; i < ctx->variables_size; ++i) {
		if (sv_eq(name, ctx->variables_names[i])) {
			ctx->variables_values[i] = value;
			return;
		}
	}

	ctx->variables_names [ctx->variables_size] = name;
	ctx->variables_values[ctx->variables_size] = value;
	ctx->variables_size += 1;
}

int
eval_ctx_lookup_variable(eval_ctx *ctx, sv name, sv *out)
{
	for (size_t i = 0; i < ctx->variables_size; ++i) {
		if (sv_eq(name, ctx->variables_names[i])) {
			*out = ctx->variables_values[i];
			return 0;
		}
	}

	return -1;
}
