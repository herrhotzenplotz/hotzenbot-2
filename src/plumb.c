/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/plumb.h>
#include <hotzenbot/irc_commands.h>
#include <hotzenbot/logging.h>

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>

/*
 * This is only internal. The outside world does not need to know
 * about it.
 */

struct plumber_message
{
	char   network[64];
	size_t network_size;
	char   message[512];
	size_t message_size;
};

void
plumb(int write_pipe, sv network, const char *fmt, ...)
{
	struct plumber_message msg           = {0};
	size_t                 written_bytes = 0;
	va_list                vp;

	va_start(vp, fmt);
	msg.message_size = vsnprintf(msg.message, sizeof(msg.message), fmt, vp);
	va_end(vp);

	msg.message_size =
		(msg.message_size > sizeof(msg.message))
		? sizeof(msg.message)
		: msg.message_size;

	msg.network_size = snprintf(msg.network, sizeof(msg.network), SV_FMT, SV_ARGS(network));
	msg.network_size =
		(msg.network_size > sizeof(msg.network))
		? sizeof(msg.network)
		: msg.network_size;

	while (written_bytes < sizeof(msg)) {
		ssize_t n = write(write_pipe, ((char *)(&msg)) + written_bytes, sizeof(msg) - written_bytes);
		if (n < 0) {
			log_error("Plumber couldn't write message");
			return;
		}

		written_bytes += n;
	}
}

/* TODO: Change plumb_out to use sendfile */
void
plumb_out(int read_pipe, irc_socket *list, size_t list_size)
{
	struct plumber_message msg        = {0};
	size_t                 read_bytes = 0;

	while (read_bytes < sizeof(msg)) {
		ssize_t n = read(read_pipe, ((char *)(&msg)) + read_bytes, sizeof(msg) - read_bytes);
		if (n < 0) {
			log_error("Plumber couldn't read message");
			return;
		}

		read_bytes += n;
	}

	for (size_t i = 0; i < list_size; ++i) {
		if (sv_eq(list[i].config->network, sv_from_parts(msg.network, msg.network_size))) {
			irc_write(&list[i], msg.message, msg.message_size);
			return;
		}
	}

	log_warn("Plumber received message for non-existant target network '%.*s'",
			 (int)msg.network_size, msg.network);
}
