/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/channel_state.h>
#include <hotzenbot/helper.h>
#include <hotzenbot/irc_commands.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/socket.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <openssl/err.h>

/* These are global as they are gonna be reused for multiple
 * connections. Also, we don't need to store them inside the struct
 * irc_socket then. */
static SSL_CTX    *ssl_context;
static const SSL_METHOD *ssl_method;

static int
irc_socket_ssl_log(const char *str, size_t str_len, void *_user_data)
{
	(void) _user_data;

	log_error("SSL error : %.*s", (int) str_len, str);

	return 0;
}

static void
irc_socket_ensure_ssl(void)
{
	if (!ssl_context) {
		ssl_method = TLS_client_method();

		if (!ssl_method) {
			log_error("TLS_client_method failed");
			ERR_print_errors_cb(irc_socket_ssl_log, NULL);
			exit(1);
		}

		ssl_context = SSL_CTX_new(ssl_method);
		if (!ssl_context) {
			log_error("SSL_CTX_new failed");
			ERR_print_errors_cb(irc_socket_ssl_log, NULL);
			exit(1);
		}
	}
}

static int
socket_connect(irc_socket *sock, struct addrinfo *it)
{
	int rc;

	/* grab a socket fd */
	sock->irc_fd = socket(
		it->ai_family,
		it->ai_socktype,
		it->ai_protocol);
	if (sock->irc_fd < 0) {
		log_error("socket() failed: %s", strerror(errno));
		syslog(LOG_CRIT, "Cannot create a socket. System Call failed. Exiting...");
		exit(1);
	}

	rc = connect(sock->irc_fd, it->ai_addr, it->ai_addrlen);
	if (rc < 0) {
		close(sock->irc_fd);
		sock->irc_fd = -1;
		log_warn("connect() failed: %s", strerror(errno));
		return -1;
	}

	return 0;
}

static int
socket_setup(irc_socket *sock)
{
	int				 rc;
	struct addrinfo	 hints		  = {0};
	struct addrinfo *results	  = NULL, *it = NULL;
	char			 hostname[64] = {0};
	char             port[64]     = {0};

	if (sock->config->host.length >= sizeof(hostname)) {
		log_error("host name is too long");
		exit(1);
	}

	/* Make NUL terminated strings for the hostname and service */
	memcpy(hostname, sock->config->host.data, sock->config->host.length);
	snprintf(port, sizeof(port), "%d", sock->config->port);

	hints.ai_family   = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	/* hostname lookup */
	rc = getaddrinfo(hostname, port, &hints, &results);
	if (rc) {
		log_error("getaddrinfo failed. Could not resolve hostname '%s': %s",
				  hostname, gai_strerror(rc));
		syslog(LOG_CRIT, "Host lookup failed. Exiting...");
		exit(1);
	}

	/* check that we got at least one record */
	if (!results) {
		log_error("getaddrinfo returned no address records for host '%s'",
				  hostname);
		syslog(LOG_CRIT, "Host lookup failed. Exiting...");
		exit(1);
	}

	it = results;

	/* Try to connect to the resolved addresses */
	do {
		rc = socket_connect(sock, it);
	} while (rc && (it = results->ai_next));

	freeaddrinfo(results);
	it = results = NULL;

	return rc;
}

static int
irc_socket_perform_connect(irc_socket *sock)
{
	/* look up address, create socket and connect */
	if (socket_setup(sock))
		return -1;

	/* Now establish a TLS encrypted connection if requested */
	if (sock->config->secure_connection) {
		log_info("Establishing secure TLS connection");

		/* Ensure that we have initialized the OpenSSL stuff */
		irc_socket_ensure_ssl();

		for (;;) {

			/* Cleanup old connection if needed */
			if (sock->ssl) {
				log_info("Cleaning up old SSL connection");
				SSL_free(sock->ssl);
			}

			/* Create new SSL */
			sock->ssl = SSL_new(ssl_context);
			if (!sock->ssl) {
				log_error("SSL_new failed");
				ERR_print_errors_cb(irc_socket_ssl_log, NULL);
				syslog(LOG_CRIT, "Cannot create SSL structure for secure connection");
				exit(1);
			}

			/* Wire up the created socket */
			SSL_set_fd(sock->ssl, sock->irc_fd);

			/* Actually perform the SSL connection */
			if (SSL_connect(sock->ssl) == 1)
				break; /* connected! */

			log_error("SSL_connect failed, gonna retry");
			ERR_print_errors_cb(irc_socket_ssl_log, NULL);

			/* TODO: Max tries */
		}
	}

	sock->read_buf_size = 0;

	if (sock->config->pass.data == NULL)
		irc_plain_auth(sock, sock->config->nick, sock->config->user);
	else
		irc_pass_auth(sock, sock->config->nick, sock->config->pass);

	if (sock->config->caps.data)
		irc_cap_req(sock, sock->config->caps);

	return 0;
}

void
irc_socket_connect(irc_socket *sock)
{
	irc_socket_perform_connect(sock);
}

void
irc_socket_reconnect(irc_socket *sock)
{
	/* We should be checking if SSL_shutdown actually
	 * finished. However, I won't bother. */
	if (sock->ssl)
		SSL_shutdown(sock->ssl);

	close(sock->irc_fd);
	sock->irc_fd = -1;

	if (irc_socket_perform_connect(sock) < 0) {
		log_error("Reconnect to %.*s failed",
				  sock->config->host.data,
				  (int)sock->config->host.length);
		return;
	}

	irc_rejoin_channels_after_reconnect(sock);
}

char *
irc_socket_read_line(irc_socket *sock)
{
	int i = 0;
	while ((i = irc_socket_pending(sock)) < 0) {
		if (sizeof(sock->read_buf) == sock->read_buf_size) {
			log_fatal("Read buffer of the irc socket is too small");
			abort();
		}

		int read_bytes;
		if (sock->ssl)
			read_bytes = SSL_read(sock->ssl, sock->read_buf + sock->read_buf_size, 1);
		else
			read_bytes = read(sock->irc_fd, sock->read_buf + sock->read_buf_size, 1);

		if (read_bytes <= 0) {
			log_fatal("read() returned error code");
			syslog(LOG_CRIT, "hotzenbotd lost irc connection. Attempting to reconnect...");

			irc_socket_reconnect(sock);
			continue;
		}

		sock->read_buf_size += read_bytes;
	}

	char *it = calloc(i + 3, 1);
	memcpy(it, sock->read_buf, i + 2);
	it[i + 2] = '\0';

	memmove(sock->read_buf, sock->read_buf + i + 2, sock->read_buf_size - i - 2);

	sock->read_buf_size -= i + 2;
	return it;
}

int
irc_socket_pending(irc_socket *sock)
{
	for (int i = 0; i < sock->read_buf_size; ++i) {
		if (sock->read_buf[i] == '\r') {
			if (i == sock->read_buf_size - 1)
				continue;

			if (sock->read_buf[i+1] == '\n')
				return i;
		}
	}

	return -1;
}

void
irc_socket_init(irc_socket *socket, config_entry *entry)
{
	socket->config = entry;
}

void
irc_socket_close(irc_socket *socket)
{
	if (socket->ssl) {
		SSL_shutdown(socket->ssl);
		SSL_free(socket->ssl);
		socket->ssl = NULL;
	}

	fsync(socket->irc_fd);
	close(socket->irc_fd);
}

int
irc_socket_write(irc_socket *socket, const char *buf, size_t buf_size)
{
	if (socket->ssl)
		return SSL_write(socket->ssl, buf, buf_size);
	else
		return write(socket->irc_fd, buf, buf_size); /* TODO: types are different */
}
