/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/db.h>
#include <hotzenbot/helper.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/reminder.h>
#include <hotzenbot/plumb.h>

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>

#include <pthread.h>

static char * const
interval_names_table[] = {
	"s", "secs", "seconds", "second", "sec",
	"m", "mins", "minutes", "minute", "min",
	"h", "hrs",  "hours", "hour", "hr",
	"d", "days", "day",
	"weeks", "week",
	"months", "month",
	"a", "yrs",  "years",
};

static const time_t
interval_factor_table[] = {
	1, 1, 1, 1, 1,
	60, 60, 60, 60, 60,
	3600, 3600, 3600, 3600, 3600,
	3600 * 24, 3600 * 24, 3600 * 24,
	3600 * 24 * 7, 3600 * 24 * 7,
	3600 * 24 * 7 * 30, 3600 * 24 * 7 * 30, /* This is somewhat stupid, but whatever. Feel free to fix */
	3600 * 24 * 365,
	3600 * 24 * 365,
	3600 * 24 * 365,
};

_Static_assert(ARRAY_SIZE(interval_names_table) == ARRAY_SIZE(interval_factor_table),
			   "The interval name lookup table for unit of time conversion is inconsistent.");

/*
 * Parse a time interval like '2 days', '3 seconds' etc. into a
 * time_t.
 */
static int
reminder_parse_relative(sv *input, time_t *out)
{
	char   *endptr = NULL;
	long    number = 0;
	size_t  i      = 0;

	number = strtol(input->data, &endptr, 10);
	for (i = 0; i < input->length; ++i)
		if (input->data[i] == ' ')
			break;

	if (input->data + i != endptr)
		return -1;

	input->data   += i;
	input->length -= i;

	*input = sv_trim_front(*input);
	if (input->length == 0)
		return -1;

	for (i = 0; i < input->length; ++i)
		if (input->data[i] == ' ' || input->data[i] == ',')
			break;


	// XXX: Maybe factor this out into a seperate function
	sv unit_string = sv_from_parts(input->data, i);
	for (size_t unit_idx = 0; unit_idx < ARRAY_SIZE(interval_names_table); ++unit_idx) {
		if (sv_eq(unit_string, SV(interval_names_table[unit_idx]))) {
			*out           = number * interval_factor_table[unit_idx];
			input->data   += i;
			input->length -= i;
			return 0;
		}
	}

	/* Unknown unit */
	return -1;

}

/*
 * This function parses a string provided by the user into a
 * time_t. The input string can be of different formats:
 *
 *  - in 3 weeks, 2 days, 7 hours and 5 minutes
 *  - on 6 September 2069 at 14:06
 */
int
reminder_parse_time_string(sv input, time_t *out)
{
	input = sv_trim_front(input);
	if (input.length == 0)
		return -1;

	if (sv_has_prefix(input, "in")) {

		time_t result = time(NULL);

		for (input.length -= 2, input.data += 2, input = sv_trim_front(input);
			 input.length > 0;
			 input = sv_trim_front(input)) {

			time_t interval = 0;
			if (reminder_parse_relative(&input, &interval) < 0)
				return -1;

			result += interval;

			input = sv_trim_front(input);

			if (input.length == 0)
				break;

			if (input.data[0] == ',') {
				input.data   += 1;
				input.length -= 1;
			}

			if (sv_has_prefix(input, "and")) {
				input.data   += 3;
				input.length -= 3;
			}
		}

		*out = result;
		return 0;
	}

	// TODO: Implement parsing absolute timestamp
	return -1;
}

reminder_error
add_reminder(channel channel, sv trigger_time, sv text)
{
	time_t reminder_trigger_time;

	if (reminder_parse_time_string(trigger_time, &reminder_trigger_time) < 0)
		return RE_TIMEPARSE;

	if (db_add_reminder(channel, text, reminder_trigger_time) < 0)
		return RE_DBFAIL;

	return 0;
}

static pthread_t reminder_thread;

static void *
reminder_thread_worker(void *foo)
{
	int       mx_fd              = (int)(long)(foo);
	arena     reminder_arena     = {0};
	reminder *reminder_list      = NULL;
	size_t    reminder_list_size = 0;

	arena_init(&reminder_arena, 64 * 1024);


	for (;;) {
		int sleep_time = 10;

		arena_reset(&reminder_arena);

		if (db_begin_transaction() < 0) {
			log_fatal("unable to begin transaction in reminder thread");
			sleep(sleep_time);
			continue;
		}

		reminder_list_size = db_get_reminders(&reminder_arena, &reminder_list);
		time_t now = time(NULL);

		for (size_t i = 0; i < reminder_list_size; ++i) {
			if (reminder_list[i].trigger_time <= now) {

				log_info("Triggering timer in channel "CHAN_FMT, CHAN_ARGS(reminder_list[i].channel));
				plumb(mx_fd, reminder_list[i].channel.network, "PRIVMSG "SV_FMT" :"SV_FMT"\r\n",
					  SV_ARGS(reminder_list[i].channel.name), SV_ARGS(reminder_list[i].text));

				if (db_delete_reminder_by_id(reminder_list[i].id) < 0) {
					log_fatal("Reminder thread was unable to remove a reminder from the database. To reduce trouble, I'm gonna exit here.");
					return NULL;
				}

			} else {
				time_t remaining = reminder_list[i].trigger_time - now;
				if (remaining < sleep_time)
					sleep_time = (int)remaining;
			}
		}

		if (db_commit_transaction() < 0)
			log_fatal("Unable to commit transaction in reminder thread");

		sleep(sleep_time);
	}
}

int
start_reminder_thread(int fd)
{
	log_info("Attempting to spawn reminder thread");

	if (pthread_create(&reminder_thread, NULL, reminder_thread_worker, (void *)(long)(fd)) != 0) {
		log_fatal("Unable to spawn reminder thread");
		return -1;
	}

	return 0;
}
