/*
 * Copyright 2021, 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/logging.h>
#include <hotzenbot/channel.h>

#include <errno.h>
#include <semaphore.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/types.h>

static FILE *chat_log_file = NULL;
static FILE *gp_log_file   = NULL;

/* Lock for the logger system */
static sem_t *lock = NULL;

void
init_log_files(void)
{
	chat_log_file = fopen("chat.log", "a");
	gp_log_file   = fopen("hotzenbot.log", "a");

	fprintf(stderr, "INFO : Initializing logging semaphores...\n");

	/* Initialize the logger locking semaphores */
	lock = mmap(NULL, 4096, PROT_READ|PROT_WRITE,
	            MAP_SHARED|MAP_ANONYMOUS, -1, 0);
	if (lock == MAP_FAILED) {
		fprintf(stderr,
		        "Could not mmap a shared page for the logger"
		        " locking semaphore: %s",
		        strerror(errno));
		abort();
	}

	/* clear it out */
	memset(lock, 0, sizeof (*lock));

	/* initialize the semaphore to 1 (unlocked) */
	if (sem_init(lock, 1, 1) < 0) {
		fprintf(stderr,
		        "Could not initialize logger locking semaphore: %s",
		        strerror(errno));
		abort();
	}

	fprintf(stderr, "INFO : Logger has been successfully initialized\n");
}

static void
lock_logger(void)
{
	if (sem_wait(lock) < 0) {
		fprintf(stderr, "error: failed on sem_wait: %s",
		        strerror(errno));
		abort();
	}
}

static void
unlock_logger(void)
{
	if (sem_post(lock) < 0) {
		fprintf(stderr, "error: failed on sem_post: %s",
		        strerror(errno));
		abort();
	}
}

void
close_log_files(void)
{
	lock_logger();

	fclose(chat_log_file);
	fclose(gp_log_file);

	chat_log_file = NULL;
	gp_log_file = NULL;

	/* Clean up the semaphore */
	unlock_logger(); /* XXX: do we need this ? */

	if (sem_destroy(lock) < 0)
		fprintf(stderr, "Could not destroy semaphore");

	munmap(lock, 4096);

	lock = NULL;
}

static void
print_timestamp(FILE *stream)
{
	char buf[64];
	time_t now = time(NULL);
	struct tm *now_r = gmtime(&now);

	size_t len = strftime(buf, sizeof(buf), "[%F %T] : ", now_r);
	fwrite(buf, 1, len, stream);
}

static void
gp_log(const char *prefix, const char *fmt, va_list vp)
{
	lock_logger();

	print_timestamp(gp_log_file);
	print_timestamp(stdout);

	fprintf(gp_log_file, "%s : ", prefix);
	fprintf(stdout,      "%s : ", prefix);

	char *ret = calloc(1024, 1);
	vsnprintf(ret, 1024, fmt, vp);
	ret[1023] = '\0';

	fputs(ret, gp_log_file);
	fputs(ret, stdout);

	free(ret);

	fputc('\n', gp_log_file);
	fputc('\n', stdout);
	fflush(gp_log_file);

	unlock_logger();
}

void
log_info(const char *fmt, ...)
{
	va_list vp;
	va_start(vp, fmt);
	gp_log("INFO", fmt, vp);
	va_end(vp);
}

void
log_error(const char *fmt, ...)
{
	va_list vp;
	va_start(vp, fmt);
	gp_log("ERR ", fmt, vp);
	va_end(vp);
}

void
log_fatal(const char *fmt, ...)
{
	va_list vp;
	va_start(vp, fmt);
	gp_log("FATAL", fmt, vp);
	va_end(vp);
}

void
log_warn(const char *fmt, ...)
{
	va_list vp;
	va_start(vp, fmt);
	gp_log("WARN", fmt, vp);
	va_end(vp);
}

void
chat_log(channel channel, sv nick, sv msg)
{
	print_timestamp(chat_log_file);
	print_timestamp(stdout);

	fprintf(chat_log_file, CHAN_FMT"\t"SV_FMT"\t"SV_FMT"\n", CHAN_ARGS(channel), SV_ARGS(nick), SV_ARGS(msg));
	fprintf(stdout, "IRC  : "CHAN_FMT"/"SV_FMT" : "SV_FMT"\n", CHAN_ARGS(channel), SV_ARGS(nick), SV_ARGS(msg));

	fflush(chat_log_file);
}
