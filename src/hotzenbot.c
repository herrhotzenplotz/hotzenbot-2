/*
 * Copyright 2021 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <poll.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>

#include <hotzenbot/channel_state.h>
#include <hotzenbot/config.h>
#include <hotzenbot/db.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <hotzenbot/ipcsocket.h>
#include <hotzenbot/irc_commands.h>
#include <hotzenbot/irc_message.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/markov.h>
#include <hotzenbot/modules.h>
#include <hotzenbot/pipe.h>
#include <hotzenbot/plumb.h>
#include <hotzenbot/sb.h>
#include <hotzenbot/socket.h>
#include <hotzenbot/stalk.h>
#include <hotzenbot/timer.h>
#include <hotzenbot/version.h>

enum {
	IDX_TMR = 0,
	IDX_CTL = 1,
	IDX_RMD = 2,
	N_SYSTEM_PIPES,
};

struct pollfd *pollfds      = NULL;
size_t         pollfds_size = N_SYSTEM_PIPES;
irc_socket     irc_sockets[CONFIG_MAX_ENTRIES] = {0};

/* This is kind of a hack. Whenever we call exit(), we should also
   disconnect from all the trasnports. */

static void
atexit_handler(void)
{
	log_info("atexit handler has been called. Shutting down transports...");

	char quit_msg[] = "QUIT hotzenbot-2 shutting down.\r\n";
	for (size_t i = 0; i < pollfds_size - N_SYSTEM_PIPES; ++i) {
		irc_write(&irc_sockets[i], quit_msg, sizeof(quit_msg));
		irc_socket_close(&irc_sockets[i]);
	}

	log_info("Closing database");
	close_database();

	log_info("Closing log files");
	close_log_files();

	fprintf(stderr, "[FILES SYNCED] Good bye.\n");
}

static void
handle_irc_event(irc_socket *sock, char *line, arena *memory)
{
	irc_message msg = {0};
	if (irc_message_parse(&msg, SV(line))) {
		log_error("Cannot parse irc message");
		return;
	}

	if (sv_eq_to_cstr(msg.command, "PING")) {
		irc_pong(sock, msg.trailing);
		log_info("Replied to ping from "SV_FMT, SV_ARGS(sock->config->host));
		return;
	}

	/* MY GOD. FUCK YOU TWITCH. YOU MISARABLE C<beeep> */
	if (!sock->config->is_twitch) {
		/* channel user list */
		if (sv_eq_to_cstr(msg.command, "353")) {
			log_info("Received channel user list, issuing WHOIS for each nick");

			sv user_list = msg.trailing;
			channel channel = {
				.name = msg.parameters[2],
				.network = sock->config->network,
			};

			while (user_list.length > 0) {
				if (user_list.data[0] == '@' || user_list.data[0] == ' ') {
					user_list.data += 1;
					user_list.length -= 1;
					continue;
				}

				size_t i;
				for (i = 0; i < user_list.length; ++i)
					if (user_list.data[i] == ' ')
						break;

				sv user = user_list;
				user.length = i;

				hotzenbot_usleep(200000); /* Hack to circumvent Excess floods */

				irc_whois(sock, user);
				channel_user_joined(channel, user);

				user_list.data += i;
				user_list.length -= i;
			}

			return;
		} else {
			if (sv_eq_to_cstr(msg.command, "RECONNECT")) {
				irc_socket_reconnect(sock);
				return;
			}
		}

		/* WHOIS account info */
		if (sv_eq_to_cstr(msg.command, "330")) {
			log_info("User "SV_FMT" is registered as "SV_FMT, SV_ARGS(msg.parameters[1]), SV_ARGS(msg.parameters[2]));
			irc_user_whois_registered(sock->config->network, msg.parameters[1], msg.parameters[2]);
			return;
		}

	}

	channel channel = CHAN(sock->config->network, msg.parameters[0]);

	/* channel topic */
	if (sv_eq_to_cstr(msg.command, "332")) {
		log_info("Received channel topic");
		channel_set_topic(CHAN(sock->config->network, msg.parameters[1]),
						  msg.trailing);
		return;
	}

	/* TODO: Handle QUIT properly */

	/* PART (from a user) */
	if (sv_eq_to_cstr(msg.command, "PART")) {
		log_info("User "SV_FMT" parted "CHAN_FMT, SV_ARGS(msg.nick), CHAN_ARGS(channel));
		channel_user_parted(channel, msg.nick);
		return;
	}

	/* Reset the arena for all following operations */
	arena_reset(memory);

	/* Handle stalking users */
	dostalk(memory, channel, msg.nick, sock);

	/* JOIN */
	if (sv_eq_to_cstr(msg.command, "JOIN")) {
		log_info("User "SV_FMT" joined "CHAN_FMT, SV_ARGS(msg.nick), CHAN_ARGS(channel));

		channel_user_joined(channel, msg.nick);

		if (!sock->config->is_twitch)
			irc_whois(sock, msg.nick);
		return;
	}

	if (sv_eq_to_cstr(msg.command, "PRIVMSG")) {
		cmd_pipe  p           = {0};
		eval_ctx  ctx         = {0};
		char     *response;
		hook      maybe_hook  = {0};
		sv        maybe_topic = {0};

		chat_log(channel, msg.nick, msg.trailing);

		if (sock->config->is_twitch) {
			sv twitch_id = irc_message_tags_get_value(&msg, "user-id");
			if (twitch_id.data)
				irc_user_whois_registered(channel.network, msg.nick, twitch_id);
			else
				log_warn("Cannot extract twitch user-id from privmsg. You probably didn't request the tags capability.");
		}

		/*
		 * Right now there are three cases where we wanna ignore a
		 * message.
		 *
		 * 1) The sender has been specifically marked as `ignored'
		 * 2) We are in a channel that has been set to lurk-mode
		 * 3) The Privmsg was a /me command.
		 */
		sv account = {0};
		if (irc_user_get_account(channel.network, msg.nick, &account) == LOOKUP_OK) {
			if (db_is_user_ignored(channel.network, account)) {
				log_warn("Ignoring message from user "SV_FMT" on "CHAN_FMT, SV_ARGS(account), CHAN_ARGS(channel));
				return;
			}
		}

		if (channel_should_lurk(channel)) {
			log_info("Lurk mode enabled in "SV_FMT". Ignoring message.", SV_ARGS(msg.parameters[0]));
			return;
		}

		/* Ignore /me */
		if (sv_has_prefix(msg.trailing, "\001ACTION")) {
			log_info("Ignoring ACTION");
			return;
		}

		sv prefix = channel_get_prefix(channel);

		if (channel_get_topic(channel, &maybe_topic) == 0)
			eval_ctx_set_variable(&ctx, "topic", maybe_topic);

		/* set the %year macro */
		struct tm tm = {0};
		time_t tn = time(NULL);
		localtime_r(&tn, &tm);

		const size_t buf_len = 5;
		char *yearbuf = arena_alloc(memory, buf_len);
		snprintf(yearbuf, buf_len, "%0d", tm.tm_year + 1900);

		eval_ctx_set_variable(&ctx, "ichannel", msg.parameters[0]);
		eval_ctx_set_variable(&ctx, "sender", msg.nick);
		eval_ctx_set_variable(&ctx, "year", sv_from_parts(yearbuf, buf_len - 1));
		eval_ctx_set_variable(&ctx, "version", SV(STR(HOTZENBOT_GIT_VERSION_HASH)));
		eval_ctx_set_variable(&ctx, "1", SV(NULL));

		ctx.channel = channel;

		/*
		 * Once we have a message to process, we try to parse it as a
		 * command pipe. If that fails, we attempt to match a hook
		 * against it. If that fails, we do nothing.
		 */

		if (parse_pipe(msg.trailing, *prefix.data, &p) == 0) {

			/*
			 * We don't want to add command invokations to the markov
			 * model here,
			 */

			response = run_pipe(&p, &ctx, memory);

			if (response)
				irc_privmsg(sock, msg.parameters[0], response);
			else
				log_warn("Eval produced no result. Skipping reply to user");

			return;

		} else if (hook_try_match(memory, sock->config->network, msg.parameters[0], msg.trailing, &maybe_hook) == 0) {
			char times_buffer[64] = {0};

			size_t times_length = snprintf(times_buffer, sizeof(times_buffer), "%d", maybe_hook.times);
			eval_ctx_set_variable(&ctx, "times", sv_from_parts(times_buffer, times_length));
			eval_ctx_set_variable(&ctx, "1", msg.trailing);

			response = hook_run(memory, &ctx, &maybe_hook);
			irc_privmsg(sock, msg.parameters[0], response);

			return;

		} else {
			markov_add_sentence(msg.trailing);

			return;
		}
	}

	log_info(SV_FMT" : "SV_FMT, SV_ARGS(msg.command), SV_ARGS(msg.trailing));
}

static void
signal_handler(int signum)
{
	switch (signum) {
		/* See note below in setup_signals(void) */
#if defined(SIGINFO)
	case SIGINFO:
#endif /* SIGINFO */
	case SIGUSR1: {
		channel_state_dump();
		modules_dump();
	} break;
	default: {
		log_warn("Received an unhooked signal");
	} break;
	}
}

void
setup_signals(void)
{
	struct sigaction sa = {
		.sa_handler = signal_handler,
		.sa_flags   = SA_RESTART,
	};

	if (sigaction(SIGUSR1, &sa, NULL) < 0) {
		log_error("Unable to install signal handler for SIGUSR1: %s",
				  strerror(errno));
		exit(1);
	}

	/* On many systems, siginfo exists and is a perfectly suitable
	 * signal to handle the info dump request with. However, since it
	 * is not defined by POSIX, we cannot depend on it being there. I
	 * expect it to be #defined. If it is not and instead defined by
	 * an enum of some sort, then you may be out of luck
	 * here. However, it should not break anything, as we just omit
	 * this chunk of code and don't hook the signal. */
#if defined(SIGINFO)
	if (sigaction(SIGINFO, &sa, NULL) < 0) {
		log_error("Unable to install signal handler for SIGINFO: %s",
				  strerror(errno));
		exit(1);
	}
#endif /* SIGINFO */

	log_info("Successfully hooked info signals");

#if defined(SIGPIPE)
	/* We also want to ignore SIGPIPE, as it will crash the bot
	 * without us having the opportunity to reconnect to the
	 * service. This is bad, but whatever. If any system provides
	 * SIGPIPE but doesn't define it, you're out of luck here. We have
	 * to change the build system a bit, to make it work. */
	sa.sa_handler = SIG_IGN;
	if (sigaction(SIGPIPE, &sa, NULL) < 0) {
		log_error("Unable to ignore SIGPIPE: %s",
				  strerror(errno));
		exit(1);
	}

	log_info("Ignoring SIGPIPE");
#endif /* SIGPIPE */
}

void
daemonize(void)
{
	syslog(LOG_NOTICE, "Daemonizing hotzenbot.");

	switch (fork()) {
	case 0:
		/* Child */
		break;
	case -1:
		log_fatal("first fork() failed.");
		goto bail;
	default:
		log_info("Detaching to to run as a daemon. The parent will be terminating now.");
		exit(0);
	}

	if (setsid() == -1) {
		log_fatal("setsid() failed.");
		goto bail;
	}

	switch (fork()) {
	case 0:
		/* Child */
		break;
	case -1:
		log_fatal("second fork() failed.");
		goto bail;
	default:
		log_info("Exiting after second fork");
		exit(0);
	}

	int devnull = open("/dev/null", O_RDWR);
	if (devnull < 0) {
		log_fatal("Unable to open /dev/null");
		goto bail;
	}

	if (dup2(devnull, STDOUT_FILENO) == -1) {
		log_fatal("Unable to redirect stdout");
		goto bail;
	}

	if (dup2(devnull, STDIN_FILENO) == -1) {
		log_fatal("Unable to redirect stdout");
		goto bail;
	}

	if (dup2(devnull, STDERR_FILENO) == -1) {
		log_fatal("Unable to redirect stdout");
		goto bail;
	}

	if (chdir("/") < 0) {
		log_fatal("Unable to chdir to /");
		goto bail;
	}

	return;

bail:
	syslog(LOG_WARNING, "Unable to daemonize the hotzenbot.");
	log_fatal("Unable to damonize. Exiting...");
	exit(1);
}

/* Stolen from
 * https://github.com/tsoding/bm/blob/master/nobuild.h#L1007 */
static char *
shift(int *argc, char ***argv)
{
	assert(*argc > 0);
	char *result = **argv;
	*argv += 1;
	*argc -= 1;
	return result;
}

int
main(int argc, char *argv[])
{
	int            should_daemonize                = 1;
	int            should_initialize_database      = 0;
	int            have_config_provided            = 0;
	char          *database_file                   = NULL;
	char          *autoload_modules[16]            = {0};
	size_t         autoload_modules_size           = 0;
	config         config                          = {0};
	int            timer_read_fd                   = -1;
	int            reminders_read_fd               = -1;
	int            control_fd                      = -1;

	setlocale(LC_ALL, "en_US.UTF-8"); // Needed for the wchar_t stuff in some modules

	char *binary_name = shift(&argc, &argv);

	while (argc > 0) {
		char *optname = shift(&argc, &argv);

		if (strcmp(optname, "-f") == 0 || strcmp(optname, "--foreground") == 0) {
			should_daemonize = 0;
			continue;
		}

		if (strcmp(optname, "-i") == 0) {
			should_initialize_database = 1;
			continue;
		}

		if (strcmp(optname, "-d") == 0 || strcmp(optname, "--database") == 0) {
			if (argc == 0) {
				fprintf(stderr, "%s: %s requires database file to be specified\n", binary_name, optname);
				exit(1);
			}

			database_file = shift(&argc, &argv);
			continue;
		}

		if (strcmp(optname, "-l") == 0 || strcmp(optname, "--link-plugin") == 0) {
			if (argc == 0) {
				fprintf(stderr, "%s: %s requires the shared object file to link against\n", binary_name, optname);
				exit(1);
			}

			if (autoload_modules_size == ARRAY_SIZE(autoload_modules)) {
				fprintf(stderr, "%s: too many modules to link\n", binary_name);
				exit(1);
			}

			autoload_modules[autoload_modules_size++] = shift(&argc, &argv);
			continue;
		}

		if (strcmp(optname, "-c") == 0 || strcmp(optname, "--config") == 0) {
			have_config_provided = 1;
			config_read_from_file(shift(&argc, &argv), &config);
			continue;
		}

		fprintf(stderr, "%s: %s: unrecognized option\n", binary_name, optname);
		exit(1);
	}

	if (!database_file) {
		fprintf(stderr, "%s: No database specified. Please specify one with -d or --database\n", binary_name);
		exit(1);
	}

	if (!have_config_provided) {
		fprintf(stderr, "%s: No config file specified. Please specify one with -c or --config\n", binary_name);
		exit(1);
	}

	srand(time(NULL));

	init_log_files();
	init_database(database_file, should_initialize_database);

	for (size_t i = 0; i < autoload_modules_size; ++i) {
		log_info("Attempting to link %s as specified on cli", autoload_modules[i]);
		modules_load_module(autoload_modules[i]);
	}

	if (should_daemonize)
		daemonize();

	setup_signals();

	if (atexit(atexit_handler) < 0)
		log_warn("Could not register atexit handler. This may cause issues when shutting down the bot");

	/* Setup connection */
	for (size_t i = 0; i < config.entries_size; ++i) {
		irc_socket_init(&irc_sockets[i], &config.entries[i]);
		irc_socket_connect(&irc_sockets[i]);
	}

	/* Communication channel */
	{
		int timer_chan_fds[2] = {0};
		if (pipe(timer_chan_fds) < 0) {
			log_fatal("pipe() failed: Cannot create communication channel for timer threads");
			abort();
		}

		timer_read_fd = timer_chan_fds[0];
		start_timer_thread(timer_chan_fds[1]);
	}

	/* Reminder channel */
	{
		int reminder_chan_fds[2] = {0};
		if (pipe(reminder_chan_fds) < 0) {
			log_fatal("pipe() failed: Cannot create communication channel for timer threads");
			abort();
		}

		reminders_read_fd = reminder_chan_fds[0];
		start_reminder_thread(reminder_chan_fds[1]);
	}

	/* Control channel */
	{
		int control_chan_fds[2] = {0};
		if (pipe(control_chan_fds) < 0) {
			log_fatal("pipe() failed: Cannot create communication channel for timer threads");
			abort();
		}

		control_fd = control_chan_fds[0];
		start_ipc_thread(control_chan_fds[1]);
	}

	arena memory;
	arena_init(&memory, 64 * 1024); // 64k memory for the evaluation

	/* Here we prepare the list of file descriptors to poll. We have a
	 * couple of system pipes and also irc sockets. */
	pollfds = calloc(N_SYSTEM_PIPES + config.entries_size, sizeof(struct pollfd));
	if (!pollfds) {
		log_fatal("Unable to calloc pollfd list");
		abort();
	}

	pollfds[IDX_TMR] = (struct pollfd) { .fd = timer_read_fd     , .events = POLLIN, .revents = 0 };
	pollfds[IDX_RMD] = (struct pollfd) { .fd = reminders_read_fd , .events = POLLIN, .revents = 0 };
	pollfds[IDX_CTL] = (struct pollfd) { .fd = control_fd        , .events = POLLIN, .revents = 0 };

	/* make room for the IRC socket fds */
	pollfds_size += config.entries_size;

	/*
	 * Main Event Loop
	 */
	for (;;) {

		/* prepare for polling the file descriptors */
		for (size_t i = 0; i < config.entries_size; ++i) {
			struct pollfd *pfd = &pollfds[N_SYSTEM_PIPES + i];

			pfd->fd = irc_sockets[i].irc_fd;
			pfd->events = POLLIN;
			pfd->revents = 0;
		}

		/* wait for events */
		switch (poll(pollfds, pollfds_size, -1)) {
		case -1:
			if (errno == EINTR)
				continue;

			log_error("poll() returned -1");
			abort();
		case 0:
			continue;
		default:
			break;
		}

		for (size_t i = 0; i < config.entries_size; ++i) {
			if ((pollfds[N_SYSTEM_PIPES + i].revents & POLLHUP) ||
				(pollfds[N_SYSTEM_PIPES + i].revents & POLLNVAL))
			{
				/* We were either disconnected or the FD was messed with */
				irc_socket_reconnect(&irc_sockets[i]);
				continue;
			}

			if (pollfds[N_SYSTEM_PIPES + i].revents & POLLIN) {
				do {
					char *line = irc_socket_read_line(&irc_sockets[i]);
					if (db_begin_transaction() < 0) {
						log_fatal("Unable to start transaction for handling an irc message");
						free(line);
						continue;
					}

					handle_irc_event(&irc_sockets[i], line, &memory);

					if (db_commit_transaction() < 0) {
						log_fatal("Unable to commit transaction after handling an irc message");
						free(line);
						continue;
					}

					free(line);
				} while (irc_socket_pending(&irc_sockets[i]) > 0);
			}
		}

		if (pollfds[IDX_TMR].revents & POLLIN)
			plumb_out(pollfds[IDX_TMR].fd, irc_sockets, config.entries_size);

		if (pollfds[IDX_CTL].revents & POLLIN)
			plumb_out(pollfds[IDX_CTL].fd, irc_sockets, config.entries_size);

		if (pollfds[IDX_RMD].revents & POLLIN)
			plumb_out(pollfds[IDX_RMD].fd, irc_sockets, config.entries_size);
	}

	return EXIT_SUCCESS;
}
