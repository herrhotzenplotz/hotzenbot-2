/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <hotzenbot/db.h>
#include <hotzenbot/irc_commands.h>
#include <hotzenbot/logging.h>
#include <hotzenbot/stalk.h>

int
stalk(const stalk_buf *sb)
{
	if (!sb) {
		log_error("stalk: request to null pointer");
		return -1;
	}
	if (channel_null(sb->request_channel)) {
		log_error("stalk: no network");
		return -1;
	}
	if (sv_null(sb->target_user)) {
		log_error("stalk: no target user");
		return -1;
	}
	if (sv_null(sb->requested_by)) {
		log_error("stalk: no request user");
		return -1;
	}

	return db_add_stalk(sb->request_channel,
						sb->requested_by,
						sb->target_user,
						sb->message);
}

/* TODO: Cache the stalk entries in memory */
static int
stalk_query(arena *arena, sv network, sv nick, stalk_buf **out)
{
	return db_query_stalk(arena, network, nick, out);
}

void
dostalk(arena *arena, channel channel, sv nick, irc_socket *socket)
{
	int stalks_size;
	stalk_buf *stalks;

	stalks_size = stalk_query(arena, channel.network, nick, &stalks);
	if (stalks_size <= 0)
		return;

	for (int i = 0; i < stalks_size; ++i) {
		char buffer[512] = {0};

		/* FIXME: Maybe use strftime instead. ctime is ... beep */
		char *when = ctime(&stalks[i].when);
		size_t when_len = strlen(when);

		if (stalks[i].message.length > 0) {
			snprintf(buffer, sizeof buffer,
					 SV_FMT": "SV_FMT
					 " wanted to let you know this when you returned: "SV_FMT" (%.*s)",
					 SV_ARGS(nick), SV_ARGS(stalks[i].requested_by),
					 SV_ARGS(stalks[i].message),
					 (int)(when_len - 1), when);
		} else {
			snprintf(buffer, sizeof buffer,
					 SV_FMT": "SV_FMT
					 " wanted to be reminded when you are back. (%.*s)",
					 SV_ARGS(nick), SV_ARGS(stalks[i].requested_by),
					 (int)(when_len - 1), when);

		}

		irc_privmsg(socket, stalks[i].request_channel.name, buffer);

		if (db_delete_stalk(stalks[i].id)) {
			log_error("Could not delete stalk request");
			break;
		}
	}

	free(stalks);
}
