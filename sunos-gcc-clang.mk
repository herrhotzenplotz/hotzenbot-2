SUNOS_CFLAGS		=	-std=c11 -g -O0 -pedantic -I${PWD}/include \
				-fPIC -Wall -Wextra -Werror -Wno-pedantic
SUNOS_CPPFLAGS		=	-D_XOPEN_SOURCE=600
SUNOS_DAEMON_LDFLAGS	=	-L/usr/local/lib -lsqlite3 -lpthread -ldl -lsocket -lnsl -lrt
SUNOS_CTL_LDFLAGS	=	-lnsl -lsocket
SUNOS_LDFLAGS		=	-Xlinker -Bdynamic
