#!/bin/sh

info() {
    echo "$*" 1>&2
}

die() {
    info "$*"
    exit 42
}

CC=${CC-`which cc`}
[ $CC ] || die "error: I have no C compiler"

# Checking for Studio Compiler
# Both GCC and Clang exit with 1 here
$CC -V > /dev/null 2>&1

if [ $? -eq 0 ]; then
    [ `uname -p` = "sparc" ] || die "Developer Studio is not yet adapted to x86 platforms"

    info " ==> NOTE : This is probably Oracle Solaris with the Developer Studio Compiler"
    info "          : on SPARC. If it is not, please report a bug."
    cat sunstudio.mk

    exit 0
else
    info " ==> NOTE : This is probably Oracle Solaris, Illumos or OpenSolaris with GCC or Clang."
    info "          : if it is not, please report a bug."

    cat sunos-gcc-clang.mk
    exit 0
fi
