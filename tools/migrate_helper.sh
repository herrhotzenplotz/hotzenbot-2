#!/bin/sh

if [ "$(which sqlite3)" = "" ]; then
    echo "ERROR: Need sqlite3"
    exit 1;
fi

if [ "$1" = "" ]; then
    echo "No database specified"
    exit 1
fi

DB_FILE="$1"

# Prepare Tables
TARGET_DB="${2-migration.out.db}"

echo "INFO : Output is ${TARGET_DB}"

echo "INFO : Preparing tables...."

PREP_QUERIES="CREATE TABLE IF NOT EXISTS Command (id INTEGER PRIMARY KEY, code TEXT NOT NULL,\
                                                  times INT NOT NULL DEFAULT 0);\
              CREATE TABLE IF NOT EXISTS CommandName (name TEXT NOT NULL, \
                                                      commandId INTEGER NOT NULL REFERENCES Command(id) ON DELETE CASCADE, \
                                                      UNIQUE(name) ON CONFLICT REPLACE); \
              CREATE TABLE IF NOT EXISTS Groups (gid INTEGER PRIMARY KEY, \
                                           name TEXT NOT NULL); \
              CREATE TABLE IF NOT EXISTS IrcUserGroup (account TEXT NOT NULL, \
                                                       network TEXT NOT NULL DEFAULT 'libera', \
                                                       gid INTEGER NOT NULL REFERENCES Groups(gid) ON DELETE CASCADE);"
sqlite3 ${TARGET_DB} "${PREP_QUERIES}"

echo "INFO : Migrating command tables"
echo "NOTE : If you see a couple of UNIQUE constraints failing, that is because the old bot database"
echo "     : contained inconsistent command definitions. Don't be worried, they are going to be skipped."

COMMAND_QUERY="select cmd.id, cn.name, cmd.code from CommandName cn inner join Command cmd on cmd.id = cn.commandId;"
echo "`sqlite3 \"${DB_FILE}\" \"${COMMAND_QUERY}\" | awk -F '|' -f gen_sql.awk`" | sqlite3 ${TARGET_DB}

echo "INFO : Migrating Groups and Roles"
GROUP_QUERY="select * from TwitchRoles;"
sqlite3 "${DB_FILE}" "${GROUP_QUERY}" | awk -F '|' '{ printf "INSERT INTO Groups (gid, name) VALUES (%d, \"%s\");\n", $1, $2; }' \
    | sqlite3 ${TARGET_DB}

echo "NOTE : Users will not be migrated, as account fields may have"
echo "     : changed and no distinction of networks is being made atm"

echo "INFO : Done."
echo "     : You may wanna check the resulting database at ${TARGET_DB}"
echo "     : for consistency and correctness. No promises made."
