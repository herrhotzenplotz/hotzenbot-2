#!/bin/sh
# -*- mode:shell-script -*-

die()
{
    rm ${TMPFILE}
    echo "maintainerctl: $*" 1>&2
    exit 1
}

list_maintainers()
{
    (echo "NETWORK|ACCOUNT";
     sqlite3 "${TARGET_DB}" "select usr.network, usr.account from Groups gp inner join IrcUserGroup usr on usr.gid = gp.gid where gp.name = 'maintainer';") \
        | column -t -s '|'
}

add_maintainer()
{
    local GID=$(sqlite3 "${TARGET_DB}" \
                        "select gid from Groups where name = 'maintainer';" | \
                    sed 1q)

    [ $GID ] || die "Maintainer group is undefined. See hotzenbot(8)."

    sqlite3 "${TARGET_DB}" "INSERT INTO IrcUserGroup (gid, account, network) VALUES (${GID}, '${ACCT}', '${NETWORK}');"
}

remove_maintainer()
{
    local GID=$(sqlite3 "${TARGET_DB}" \
                        "select gid from Groups where name = 'maintainer';" | \
                    sed 1q)

    [ $GID ] || die "Maintainer group is undefined. See hotzenbot(8)."

    sqlite3 "${TARGET_DB}" "delete from IrcUserGroup where gid = ${GID} and account = '${ACCT}' and network = '${NETWORK}';"
}

#############################
### ENTRY ###################
#############################

WHATTODO=nothing
TMPFILE=$(mktemp)

args=`getopt u:n:d:lar $*`

if [ $? -ne 0 ]; then
    echo 'Usage: ...'
    exit 2
fi

set -- $args

while :; do
    case "$1" in
        -l)
            [ $WHATTODO = "nothing" ] || die "Multiple operations specified"
            WHATTODO=list
            shift
            ;;
        -d)
            TARGET_DB=$2
            shift; shift
            ;;
        -n)
            NETWORK=$2
            shift; shift
            ;;
        -u)
            ACCT=$2
            shift; shift
            ;;
        -a)
            [ $WHATTODO = "nothing" ] || die "Multiple operations specified"
            WHATTODO=add
            shift
            ;;
        -r)
            [ $WHATTODO = "nothing" ] || die "Multiple operations specified"
            WHATTODO=remove
            shift
            ;;
        --)
            shift; break
            ;;
    esac
done

[ $TARGET_DB ] || die "Database is unspecified (use -d)"

case "$WHATTODO" in
    nothing)
        die "No operation specified (use -a for adding, -l for listing or -r for removing)"
        ;;
    list)
        list_maintainers
        ;;
    add)
        [ $NETWORK ] || die "Network is unspecified (use -n)"
        [ $ACCT ]    || die "Account is unspecified (use -u)"
        add_maintainer
        ;;
    remove)
        [ $NETWORK ] || die "Network is unspecified (use -n)"
        [ $ACCT ]    || die "Account is unspecified (use -u)"
        remove_maintainer
        ;;
esac
