#!/bin/sh

#
# I have no idea to use e.g. Watcom, PCC etc...  If you feel like
# extending this, please do so! Also, please send me a patch! I do
# like to support whatever you got!
#

# This outputs GCC / LLVM-Clang style flags and configuration. For
# now.

# Use the info command to output to the Make output. Use echo for
# printing into the guessed.mk.

info() {
    echo "$*" 1>&2
}

die() {
    info $*
    exit 1
}

info ""
info "=== START OF AUTOMATIC CONFIGURATION ==="

# General things

# C Compiler

HOSTCC="${CC-$(which cc)}"

# C flags
info "Guessing C Compiler flags"

WANTEDCFLAGS="-std=iso9899:2011 -g -O0 -I`pwd`/include -fPIC -Wall -Wextra"

# C preprocessing flags

# We don't enforce _XOPEN_SOURCE to be 700 as this would drag in
# POSIX.1 implicitly, which doesn't give us UNIX domain sockets. As
# much as I wanna do it, I just can't.
WANTEDCPPFLAGS=""

# Linker flags
WANTEDLDFLAGS="-rdynamic -lpthread -lrt"

# Sqlite 3
[ -x `which pkg-config` ] || die "error: I need pkg-config in the PATH."
[ "$(pkg-config --list-all | grep sqlite3)" != "" ] || die "error: missing dependency: sqlite3. Please check that you have it installed."

# Quirks
case "`uname`" in
    Haiku)
        info "Haiku detected. Applying a workaround for gcc not having the -rdynamic option"
        info "Please make sure that the /var/tmp directory exists, such that the daemon can create the IPC socket."
        WANTEDLDFLAGS="-Xlinker --export-dynamic -lpthread -lnetwork"
        ;;
    Minix)
        info "MINIX detected. Please make sure you have all needed packages installed."
        WANTEDCFLAGS="${WANTEDCFLAGS} -I/usr/pkg/include/"
        HOSTCC="${CC-$(which clang)}"
        WANTEDLDFLAGS="-rdynamic -L/usr/pkg/lib -lpthread"
        ;;
    GNU)
        info "The Hurd has been detected. Explicitely linking -ldl and forcing _XOPEN_SOURCE=700"
        WANTEDCPPFLAGS="${WANTEDCPPFLAGS} -D_XOPEN_SOURCE=700"
        WANTEDLDFLAGS="${WANTEDLDFLAGS} -ldl"
        ;;
esac

[ $HOSTCC ] || die "I have no C compiler. Please set CC."

HOSTCFLAGS="${CFLAGS-${WANTEDCFLAGS}}"
HOSTCPPFLAGS="${CPPFLAGS-${WANTEDCPPFLAGS}}"
HOSTLDFLAGS="${LDFLAGS-${WANTEDLDFLAGS}}"
SQLITECFLAGS="$(pkg-config --cflags sqlite3)"
SQLITELDFLAGS="$(pkg-config --libs sqlite3)"

info "Using ${HOSTCC} as C compiler"
info "Using the following general CFLAGS      : ${HOSTCFLAGS}"
info "Using the following general CPPFLAGS    : ${HOSTCPPFLAGS}"
info "Using the following general LDFLAGS     : ${HOSTLDFLAGS}"
info "Using the following CFLAGS for sqlite3  : ${SQLITECFLAGS}"
info "Using the following LDFLAGS for sqlite3 : ${SQLITELDFLAGS}"

info "Note: The network module will go looking for cURL or libfetch later during the build."

info "Writing guessed.mk"
echo "CC=${HOSTCC}"
echo "GUESSED_CFLAGS=${HOSTCFLAGS}"
echo "GUESSED_CPPFLAGS=${HOSTCPPFLAGS}"
echo "GUESSED_LDFLAGS=${HOSTLDFLAGS}"
echo "GUESSED_DAEMON_CFLAGS=${SQLITECFLAGS}"
echo "GUESSED_DAEMON_LDFLAGS=${SQLITELDFLAGS}"

info "=== END OF AUTOMATIC CONFIGURATION ==="
info ""
