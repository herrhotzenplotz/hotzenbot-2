#!/bin/sh

if [ "$1" = "" ]; then
    echo "ERR  : Need database"
    exit 1
fi

sqlite3 $1 "select * from Quotes where quoteChannel = '#lastmiles';" | \
    awk -F \| '{ gsub("\"", "\"\"", $4); printf "INSERT INTO Quotes (channel, date, message) VALUES (\"#blastwave\", \"%s\", \"%s\");\n", $3, $4 }'
