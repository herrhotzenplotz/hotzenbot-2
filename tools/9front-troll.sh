#!/bin/sh

# http://git.9front.org/plan9front/plan9front/2d56837b2fabf6a7cc11f02b51f20a1e35128c57/lib/troll/raw

echo "static char *${1}_cases[] = {"
curl -L -4 --url "http://git.9front.org/plan9front/plan9front/2d56837b2fabf6a7cc11f02b51f20a1e35128c57/lib/${1}/raw" \
    | sed 's/\"/\\\"/g' \
    | awk "{ printf \"    \\\"\%s\\\",\n\", \$0}"
echo "};"

cat <<EOF
#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
${1}_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(${1}_cases);
    sb_append_sv(out, arena, SV(${1}_cases[idx]));
    return 0;
}
EOF
