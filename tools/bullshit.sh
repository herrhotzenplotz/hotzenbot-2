#!/bin/sh

#
# This little script is only needed to regenerate the stubs for the
# bullshit module. You normally shouldn't need it.
#

if [ "$1" = "" ]; then
    echo "Need input URL" 1>&2
    exit 1
fi
INFILE=$1

if [ "$2" = "" ]; then
    echo "Need output directory" 1>&2
    exit 1
fi
OUTDIR=$2

TMPFILE=$(mktemp)
fetch -o - ${INFILE} 2>/dev/null > $TMPFILE

for key in $(cat $TMPFILE | jq "keys[]" | tr -d "\""); do
    OUTFILE="${OUTDIR}/${key}.c"

    echo "static char *${key}_cases[] = {" > $OUTFILE
    cat $TMPFILE | jq .${key}[] | awk '{ printf "%s, \n", $0}' >> $OUTFILE
    echo "};" >> $OUTFILE

    cat <<EOF >> $OUTFILE
#include <hotzenbot/arena.h>
#include <hotzenbot/eval.h>
#include <hotzenbot/helper.h>
#include <string.h>
#include <stdlib.h>

int
${key}_function_impl(expr_list *list, arena *arena, eval_ctx *ctx, sb *out)
{
    (void) list;
    (void) ctx;
    int idx = rand() % ARRAY_SIZE(${key}_cases);
    sb_append_sv(out, arena, SV(${key}_cases[idx]));
    return 0;
}
EOF
done

rm $TMPFILE
