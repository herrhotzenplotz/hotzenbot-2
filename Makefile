DESTDIR		?=	/
PREFIX			?=	/opt/sn/
BINDIR			?=	${DESTDIR}/${PREFIX}/bin/
LIBDIR			?=	${DESTDIR}/${PREFIX}/lib/hotzenbot
MANDIR			?=	${DESTDIR}/${PREFIX}/man/

.SILENT:

PROGS			=	hotzenbotd		\
				hotzenbotctl		\
				debugrepl		\
				markovtool		\
				beepd

MODULES		=	bullshit		\
				debug			\
				default		\
				network		\
				roles			\
				strutil		\
				twitch		\
				beep

hotzenbotd_SRCS	=	src/arena.c		\
				src/channel_state.c	\
				src/channel.c		\
				src/config.c		\
				src/db.c		\
				src/eval.c		\
				src/hook.c		\
				src/hotzenbot.c	\
				src/ipcsocket.c	\
				src/irc_commands.c	\
				src/irc_message.c	\
				src/logging.c		\
				src/modules.c		\
				src/pipe.c		\
				src/plumb.c		\
				src/reminder.c		\
				src/sb.c		\
				src/socket.c		\
				src/stalk.c		\
				src/sv.c		\
				src/timer.c		\
				src/markov.c


hotzenbotctl_SRCS	=	src/hotzenbotctl.c

markovtool_SRCS	=	src/markovtool.c	\
				src/db.c		\
				src/logging.c		\
				src/markov.c		\
				src/sv.c		\
				src/arena.c		\
				src/sb.c

debugrepl_SRCS		=	src/arena.c		\
				src/channel_state.c	\
				src/channel.c		\
				src/config.c		\
				src/db.c		\
				src/eval.c		\
				src/hook.c		\
				src/debug.c		\
				src/ipcsocket.c	\
				src/irc_commands.c	\
				src/irc_message.c	\
				src/logging.c		\
				src/modules.c		\
				src/pipe.c		\
				src/plumb.c		\
				src/reminder.c		\
				src/sb.c		\
				src/socket.c		\
				src/stalk.c		\
				src/sv.c		\
				src/timer.c		\
				src/markov.c

beepd_SRCS		=	src/beepd.c

hotzenbotd_OBJS	=	${hotzenbotd_SRCS:.c=.o}
hotzenbotctl_OBJS	=	${hotzenbotctl_SRCS:.c=.o}
markovtool_OBJS	=	${markovtool_SRCS:.c=.o}
debugrepl_OBJS		=	${debugrepl_SRCS:.c=.o}
beepd_OBJS	=	${beepd_SRCS:.c=.o}

OBJS			=	${hotzenbotd_OBJS}	\
				${hotzenbotctl_OBJS}	\
				${markovtool_OBJS}	\
				${debugrepl_OBJS}	\
				${beepd_OBJS}

DEPS			=	${OBJS:.o=.d}

CC			?=	/usr/bin/cc

CFLAGS			+=	-Iinclude
gcc_CFLAGS		=	-Wno-address	\
				-Wno-nonnull

LIBADD			=	sqlite3

#######################################################################
# all must be the very first target                                   #
#######################################################################
.PHONY: all depend build ${MODULES}
all: Makefile config.mk
	${MAKE} -f Makefile depend

-include config.mk
-include ${DEPS}

include freebsd.mk
include linux.mk
include sunos.mk

#######################################################################
# Begin build rules                                                   #
#######################################################################

config.mk: tools/autodetect.sh
	rm -f config.mk
	@echo " ==> Performing autodetection of system variables"
	tools/autodetect.sh "${MAKE}"

depend: include/hotzenbot/version.h ${DEPS}
	@echo " ==> Starting build"
	${MAKE} -f Makefile build

build: ${PROGS} ${MODULES}

src/hotzenbot.o: include/hotzenbot/version.h

${PROGS}: ${OBJS}
	@echo " ==> Linking ${@}"
	${CC} -o ${@} ${${@}_OBJS} ${LINK_FLAGS}

include/hotzenbot/version.h: genversion.sh
	@echo " ==> Generating version.h"
	./genversion.sh > include/hotzenbot/version.h


${MODULES}:
	${MAKE} CC="${CC}" TOPDIR="${PWD}" MODNAME="$@" COMPILE_FLAGS="${COMPILE_FLAGS}" \
		LINK_FLAGS="${LINK_FLAGS}" MKDEPS_FLAGS="${MKDEPS_FLAGS}" -s -f modules/$@/Makefile all

.PHONY: clean
clean:
	@echo " ==> Cleaning"
	rm -f ${PROGS} ${OBJS} ${DEPS} include/hotzenbot/version.h config.mk
	for module in ${MODULES}; do								\
		${MAKE} TOPDIR="${PWD}" MODNAME="$$module" -f modules/$$module/Makefile clean;	\
	done

.PHONY: install
install: all
# Binaries
	@echo " ==> Creating directory ${BINDIR}"
	install -d ${BINDIR}
	@echo " ==> Installing hotzenbotd to ${BINDIR}"
	install hotzenbotd ${BINDIR}
	@echo " ==> Installing hotzenbotctl to ${BINDIR}"
	install hotzenbotctl ${BINDIR}
	@echo " ==> Installing markovtool to ${BINDIR}"
	install markovtool ${BINDIR}
	@echo " ==> Installing maintainerctl to ${BINDIR}"
	install tools/maintainerctl ${BINDIR}
	@echo " ==> Installing hookctl to ${BINDIR}"
	install tools/hookctl ${BINDIR}
	@echo " ==> Creating directory ${LIBDIR}"
	install -d ${LIBDIR}
	@echo " ==> Installing modules to ${LIBDIR}"
	install lib*.so ${LIBDIR}
# Manpages
	@echo " ==> Creating directory ${MANDIR}"
	install -d ${MANDIR}/man8/
	@echo " ==> Compressing and installing hotzenbotd.8 into ${MANDIR}"
	gzip -c docs/man8/hotzenbotd.8 > ${MANDIR}/man8/hotzenbotd.8.gz
	@echo " ==> Compressing and installing hotzenbotctl.8 into ${MANDIR}"
	gzip -c docs/man8/hotzenbotctl.8 > ${MANDIR}/man8/hotzenbotctl.8.gz
	@echo " ==> Compressing and installing hotzenbot.8 into ${MANDIR}"
	gzip -c docs/man8/hotzenbot.8 > ${MANDIR}/man8/hotzenbot.8.gz
	@echo " ==> Compressing and installing maintainerctl.8 into ${MANDIR}"
	gzip -c docs/man8/maintainerctl.8 > ${MANDIR}/man8/maintainerctl.8.gz
	@echo " ==> Compressing and installing hookctl.8 into ${MANDIR}"
	gzip -c docs/man8/hookctl.8 > ${MANDIR}/man8/hookctl.8.gz
# Last message
	@echo " The hotzenbot has been installed."
	@echo " Quick start is at hotzenbot(8). (e.g. \`man -M ${MANDIR} hotzenbot\`)"

include rules.mk
