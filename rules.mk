.SUFFIXES: .c .o
.c.o:
# This is kinda slow but ... blame noomake.
	case ${CC} in *c99) printf -- '\n\n ERROR : You are probably using GNU make. Please set CC in your environment to something that is not c99 and see linux.mk for more information.\n\n'; exit 69;; esac
	@echo " ==> Compiling "`basename $@`
	${CC} -c -o $@ ${COMPILE_FLAGS} $<

.SUFFIXES: .c .d
.c.d:
	@echo " ==> Generating dependencies for $<"
	${CC} ${MKDEPS_FLAGS} $< > $@

# Print out the needed libraries for the autodetect script
snmk-libdeps:
	@echo ${LIBADD}
